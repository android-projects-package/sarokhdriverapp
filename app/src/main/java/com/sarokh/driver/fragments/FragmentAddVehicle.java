package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.BuildConfig;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackDateAndTime;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentAddVehicleBinding;
import com.sarokh.driver.databinding.FragmentVehicleMaintenanceBinding;
import com.sarokh.driver.dialogs.DatePickerDialogUtils;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResUploadFile;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.FileCompressor;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

import static android.app.Activity.RESULT_OK;

/**
 * Created by becody.com on 15,04,2020
 */
public class FragmentAddVehicle extends Fragment implements CallbackResponse {
    private FragmentAddVehicleBinding binding;
    private File mPhotoFile;
    private FileCompressor mCompressor;
    static final int REQUEST_TAKE_PHOTO = 1;
    static final int REQUEST_GALLERY_PHOTO = 2;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_vehicle, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCompressor = new FileCompressor(getActivity());
        binding.edtUploadInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPrettyDialog();
            }
        });
    /*    binding.edtCreatedDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDateDialog();
            }
        });*/
        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtVehicleName.getText().toString().trim().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Vehicle Name");
                    return;
                }

                if (binding.edtCurMillage.getText().toString().trim().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Current Millage");
                    return;
                }
                if (binding.edtVehicleMake.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Vehicle Make");
                    return;
                }
                if (binding.edtModel.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Vehicle Model");
                    return;
                }
                if (binding.edtCreatedDate.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Production Year");
                    return;
                }
                if (binding.edtVehicleType.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Vehicle Type");
                    return;
                }
                if (binding.edtCargoCapacity.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Select Cargo Capacity");
                    return;
                }
                if (binding.edtUploadInvoice.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Upload Registration File");
                    return;
                }
                if (binding.edtRegistrationNumber.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Registration Number");
                    return;
                }
                if (binding.edtRegistrationYear.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Registration Year");
                    return;
                }

                submitExpense();
            }
        });


    }

    private void showPrettyDialog() {
        final PrettyDialog prettyDialog = new PrettyDialog(getContext());
        prettyDialog.setMessage("Upload Invoice")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .addButton(
                        "Camera",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                requestStoragePermission(true);
                                prettyDialog.dismiss();
                            }
                        }
                ).addButton(
                "Gallery",                    // button text
                R.color.pdlg_color_white,        // button text color
                R.color.colorPrimary,        // button background color
                new PrettyDialogCallback() {        // button OnClick listener
                    @Override
                    public void onClick() {
                        requestStoragePermission(false);
                        prettyDialog.dismiss();
                    }
                }
        ).addButton(
                "Cancel",                    // button text
                R.color.pdlg_color_white,        // button text color
                R.color.colorPrimary,        // button background color
                new PrettyDialogCallback() {        // button OnClick listener
                    @Override
                    public void onClick() {
                        prettyDialog.dismiss();
                    }
                }
        ).show();
    }

    private void submitExpense() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            //    jsonObject.put("amount", binding.edtAmount.getText().toString().trim());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            jsonObject.put("cargoCapacity", binding.edtCargoCapacity.getText().toString().trim());
            jsonObject.put("currentMileage", binding.edtCurMillage.getText().toString().trim());
            jsonObject.put("make", binding.edtVehicleMake.getText().toString().trim());
            jsonObject.put("productionYear", binding.edtCreatedDate.getText().toString().trim());
            jsonObject.put("registrationFile", binding.edtUploadInvoice.getText().toString().trim());
            jsonObject.put("registrationNumber", binding.edtRegistrationNumber.getText().toString().trim());
            jsonObject.put("registrationYear", binding.edtRegistrationYear.getText().toString().trim());
            jsonObject.put("type", Integer.parseInt(binding.edtVehicleType.getText().toString().trim()));
            jsonObject.put("vehicleModel", binding.edtModel.getText().toString().trim());
            jsonObject.put("vehicleName", binding.edtVehicleName.getText().toString().trim());
            NetworkLoader networkLoader = new NetworkLoader(this);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_POST_ADD_MY_VEHICLE, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            if (type == 1) {
                AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
                binding.edtUploadInvoice.setText("");
                binding.edtCargoCapacity.setText("");
                binding.edtCurMillage.setText("");
                binding.edtVehicleMake.setText("");
                binding.edtCreatedDate.setText("");
                binding.edtUploadInvoice.setText("");
                binding.edtRegistrationNumber.setText("");
                binding.edtRegistrationYear.setText("");
                binding.edtVehicleType.setText("");
                binding.edtModel.setText("");
                binding.edtVehicleName.setText("");
            } else if (type == 2) {
                ResUploadFile resUploadFile = new Gson().fromJson(jsonObject.toString(), ResUploadFile.class);
                AppConstants.showDialogWithoutTitle(getActivity(), resUploadFile.getMessage());
                binding.edtUploadInvoice.setText(resUploadFile.getData());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), response);

    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    private void requestStoragePermission(final boolean isCamera) {
        Dexter.withActivity(getActivity())
                .withPermissions(Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            if (isCamera) {
                                dispatchTakePictureIntent();
                            } else {
                                dispatchGalleryIntent();
                            }
                        }
                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions,
                                                                   PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                })
                .withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        AppConstants.showErrorDialog(getContext(), "Permission error");
                    }
                })
                .onSameThread()
                .check();
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        final PrettyDialog prettyDialog = new PrettyDialog(getContext());
        prettyDialog.setTitle("Need Permissions")
                .setMessage("This app needs permission to use this feature. You can grant them in app settings.")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .addButton(
                        "GOTO SETTINGS",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                                openSettings();
                            }
                        }
                ).show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getContext().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                ex.printStackTrace();
                // Error occurred while creating the File
            }
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(getActivity(),
                        BuildConfig.APPLICATION_ID + ".provider",
                        photoFile);
                mPhotoFile = photoFile;
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    /**
     * Select image fro gallery
     */
    private void dispatchGalleryIntent() {
        Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        pickPhoto.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        startActivityForResult(pickPhoto, REQUEST_GALLERY_PHOTO);
    }

    private void uploadImageToServer(File file) {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.uploadFileToServer(AppConstants.END_UPLOAD_FILE, file, 2);
    }

    /**
     * Get real file path from URI
     */
    public String getRealPathFromUri(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Create file with current timestamp name
     *
     * @throws IOException
     */
    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String mFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File mFile = File.createTempFile(mFileName, ".jpg", storageDir);
        return mFile;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_TAKE_PHOTO) {
                try {
                    mPhotoFile = mCompressor.compressToFile(mPhotoFile);
                    uploadImageToServer(mPhotoFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == REQUEST_GALLERY_PHOTO) {
                Uri selectedImage = data.getData();
                try {
                    mPhotoFile = mCompressor.compressToFile(new File(getRealPathFromUri(selectedImage)));
                    uploadImageToServer(mPhotoFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*private void showDateDialog() {
        DatePickerDialogUtils datePickerDialogUtils = new DatePickerDialogUtils();
        datePickerDialogUtils.setCallbackDateAndTime(this);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        datePickerDialogUtils.show(ft, "Date Dialog");
    }*/
}
