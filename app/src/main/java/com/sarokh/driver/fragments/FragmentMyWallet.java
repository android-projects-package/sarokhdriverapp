package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentMyWalletBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResWallet;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 15,04,2020
 */
public class FragmentMyWallet extends Fragment implements CallbackResponse {
    private FragmentMyWalletBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_wallet, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
    }

    private void loadData() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_DRIVER_WALLET + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        ResWallet resWallet = new Gson().fromJson(jsonObject.toString(), ResWallet.class);
        binding.txtAmount.setText(getString(R.string.currency) + " " + resWallet.data.getCodAmount() + "");
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}
