package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody on 12,04,2020
 */
public class ResCodLedger {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        private int ledgerId;
        private String deliveryDate;
        private float amountPaid;
        private float pendingAmount;
        private int driverId;
        private String driverName;

        public int getLedgerId() {
            return ledgerId;
        }

        public void setLedgerId(int ledgerId) {
            this.ledgerId = ledgerId;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }

        public float getAmountPaid() {
            return amountPaid;
        }

        public void setAmountPaid(float amountPaid) {
            this.amountPaid = amountPaid;
        }

        public float getPendingAmount() {
            return pendingAmount;
        }

        public void setPendingAmount(float pendingAmount) {
            this.pendingAmount = pendingAmount;
        }

        public int getDriverId() {
            return driverId;
        }

        public void setDriverId(int driverId) {
            this.driverId = driverId;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }
    }
}