package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.adapters.AdapterCodDetail;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentGiveShipmentDetailBinding;
import com.sarokh.driver.databinding.FragmentPayCodDetailBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResCodDetail;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentPayCodDetail extends Fragment implements CallbackResponse {
    private FragmentPayCodDetailBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_pay_cod_detail, container, false);
        return binding.getRoot();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.listing.setLayoutManager(linearLayoutManager);
    }

    private void loadData() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_PAY_COD_DETAIL + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        ResCodDetail resReceiveShipmentDetail = new Gson().fromJson(jsonObject.toString(), ResCodDetail.class);
        AdapterCodDetail receiveShipmentDetail = new AdapterCodDetail(resReceiveShipmentDetail.getData());
        binding.listing.setAdapter(receiveShipmentDetail);
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}
