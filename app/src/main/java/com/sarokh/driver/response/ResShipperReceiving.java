package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 13,04,2020
 */
public class ResShipperReceiving {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private int shipperId;
        private String shipperName;
        public int getShipperId() {
            return shipperId;
        }

        public void setShipperId(int shipperId) {
            this.shipperId = shipperId;
        }

        public String getShipperName() {
            return shipperName;
        }

        public void setShipperName(String shipperName) {
            this.shipperName = shipperName;
        }
    }

}
