package com.sarokh.driver.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.ActivityCashHandOverSignatureBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.AppProgressBar;
import com.sarokh.driver.utils.CustomToast;
import com.github.gcacace.signaturepad.views.SignaturePad;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by becody on 29,02,2020
 */
public class CashHandOverSignatureActivity extends AppCompatActivity implements SignaturePad.OnSignedListener, CallbackResponse {
    private ActivityCashHandOverSignatureBinding binding;
    private boolean isFromCashHandOver = false;
    private NetworkLoader networkLoader;
    private AppProgressBar appProgressBar;
    private Bitmap bitmapSignature;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_cash_hand_over_signature);
        isFromCashHandOver = getIntent().getBooleanExtra(AppConstants.KEY_FOR_SIGNATURE, false);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        appProgressBar = new AppProgressBar(this);
        networkLoader = new NetworkLoader(this);
        binding.signaturePad.setOnSignedListener(this);
        binding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.signaturePad.clear();
            }
        });
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isFromCashHandOver) {
                    submitCashHandOverData();
                }else{
                    submitReceiving();
                }
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void submitCashHandOverData() {
        try {
            if (bitmapSignature == null) {
                CustomToast.showToast(this, "Signature required");
                return;
            }
            String signature = toBase64(bitmapSignature);
            JSONObject jsonObject = AppController.getInstance().cashHandOverObj;
            jsonObject.put("signature", signature);
            appProgressBar.showProgressDialog(null);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_CASH_HAND_OVER_SUBMISSION, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            appProgressBar.dismissProgressDialog();
        }
    }
    private void submitReceiving(){
        try {
            if (bitmapSignature == null) {
                CustomToast.showToast(this, "Signature required");
                return;
            }
            String signature = toBase64(bitmapSignature);
            JSONObject jsonObject = AppController.getInstance().cashHandOverObj;
            jsonObject.put("signature", signature);
            appProgressBar.showProgressDialog(null);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_SHIPMENT_RECEIVED, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
            appProgressBar.dismissProgressDialog();
        }
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        bitmapSignature = binding.signaturePad.getSignatureBitmap();
    }

    @Override
    public void onClear() {
        bitmapSignature = null;
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onResponse(String response, int type) {
        appProgressBar.dismissProgressDialog();
        CustomToast.showToast(this,response);
        this.finish();
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onAppError(String msg, int type) {
        appProgressBar.dismissProgressDialog();
        CustomToast.showToast(this, msg);

    }

    public String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }
}
