package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResCodDetail;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterCodDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResCodDetail.Data> dataList;
    private Context _context;

    public AdapterCodDetail(List<ResCodDetail.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTrackingNo;
        private TextView txtDeliveryDate;
        private TextView txtBillAmount;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtTrackingNo = itemView.findViewById(R.id.txtTrackingNo);
            txtDeliveryDate = itemView.findViewById(R.id.txtDeliveryDate);
            txtBillAmount = itemView.findViewById(R.id.txtBillAmount);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cod_detail, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResCodDetail.Data data = dataList.get(position);
                cellViewHolder.txtTrackingNo.setText(data.getTrackingNo());
                cellViewHolder.txtDeliveryDate.setText(data.getDeliveryDate());
                cellViewHolder.txtBillAmount.setText(_context.getResources().getString(R.string.currency) + "" + data.getBilledAmount() + "");
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
