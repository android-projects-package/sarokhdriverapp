package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentReportIssueBinding;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentReportIssue extends Fragment implements CallbackResponse {
    private FragmentReportIssueBinding fragmentReportIssueBinding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentReportIssueBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_issue, container, false);
        return fragmentReportIssueBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setProblemTypeArr();
        if (fragmentReportIssueBinding.spnProblemType.getText().toString().equals("Application")) {
            fragmentReportIssueBinding.edtApplicationId.setEnabled(false);
        } else {
            fragmentReportIssueBinding.edtApplicationId.setEnabled(true);
        }
        fragmentReportIssueBinding.spnProblemType.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                if (item.toString().equals("Application")) {
                    fragmentReportIssueBinding.edtApplicationId.setEnabled(false);
                } else {
                    fragmentReportIssueBinding.edtApplicationId.setEnabled(true);
                }
            }
        });
        fragmentReportIssueBinding.btnSubmitIssue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!fragmentReportIssueBinding.spnProblemType.getText().toString().equals("Application")) {
                    if (fragmentReportIssueBinding.edtApplicationId.getText().toString().trim().isEmpty()) {
                        AppConstants.showDialogWithoutTitle(getActivity(), "Enter Name or ID");
                        return;
                    }
                }

                if (fragmentReportIssueBinding.edtSubject.getText().toString().trim().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Subject");
                    return;
                }

                if (fragmentReportIssueBinding.edtProblemDescription.getText().toString().trim().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Problem Description");
                    return;
                }
            }
        });

    }

    private void setProblemTypeArr() {
        String[] problemArr = getResources().getStringArray(R.array.report_problem_arr);
        fragmentReportIssueBinding.spnProblemType.setItems(problemArr);
    }

  /*  private void submitComplain() {
        try {
            fragmentReportIssueBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("complaintAgainst", AppController.getInstance().resLoginObj.getData().getUser().getFullName());
            jsonObject.put("complaintComments", fragmentReportIssueBinding.edtComplain.getText().toString().trim());
            jsonObject.put("trackingNumber", fragmentReportIssueBinding.edtTrackingNumber.getText().toString().trim());
            NetworkLoader networkLoader = new NetworkLoader(this);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_REPORT_ISSUE_SHIPMENT, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentReportIssueBinding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentReportIssueBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentReportIssueBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentReportIssueBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }
}
