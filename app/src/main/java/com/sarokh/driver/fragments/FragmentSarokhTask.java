package com.sarokh.driver.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSarokhTaskBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTask;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Text;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentSarokhTask extends Fragment implements CallbackResponse{
    private FragmentSarokhTaskBinding sarokhTaskBinding;



    String[] shipmentsList = null;

    TableLayout ll;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sarokhTaskBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sarokh_task, container, false);

        ll = (TableLayout)  sarokhTaskBinding.receiveShipmentsTable;

        sarokhTaskBinding.btnRecieveShipments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Bundle bundle = new Bundle();
                String[] shipmentList = AppController.getInstance().resSarokhTask.getData().getReceiveShipmentList();
                bundle.putStringArray("receiveShipmentList",shipmentList);
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionReceiveShipment,bundle);
            }
        });

        sarokhTaskBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                builder1.setMessage("You have received a total of "+ AppController.getInstance().resSarokhTask.getData().getReceivedShipmentsCount() +" shipments. Would you like to receive more shipments or would you like to finish the transaction? ");
                builder1.setCancelable(true);
                builder1.setIcon(R.drawable.alert);

                builder1.setPositiveButton(
                        "End Transaction",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                if(AppController.getInstance().resSarokhTask.getData().getGiveShipmentList() == null){
                                    Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionPointTransactionPayment);
                                }
                                else{
                                    Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionPointTransactionGiveShipment);
                                }

                            }
                        });

                builder1.setNegativeButton(
                        "Receive more",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });

        /*sarokhTaskBinding.imgReceiveShipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionShipmentDetail);
            }
        });*/
        /*sarokhTaskBinding.imgGiveShipmentDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionGiveShipmentDetail);
            }
        });*/

        // button to consider.
        /*sarokhTaskBinding.btnReceiveCod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionPayCodDetail);
            }
        });*/
        /*sarokhTaskBinding.btnReceivedShipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Bundle bundle = new Bundle();
                String[] shipmentList = AppController.getInstance().resSarokhTask.getData().getReceiveShipmentList();
                bundle.putStringArray("receiveShipmentList",shipmentList);
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionReceiveShipment,bundle);
            }
        });*/
        /*sarokhTaskBinding.btnConfirmationShipment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.actionSarokhTaskConfirm);
            }
        });*/

        return sarokhTaskBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainActivity.var = 1;

        /*if (AppController.getInstance().resSarokhTaskDriverData.getLocationName() == null) {
            sarokhTaskBinding.txtDealerName.setText(AppController.getInstance().resSarokhTaskDriverData.getStopType());
        } else if (AppController.getInstance().resSarokhTaskDriverData.getLocationName().isEmpty()) {
            sarokhTaskBinding.txtDealerName.setText(AppController.getInstance().resSarokhTaskDriverData.getStopType());
        } else {
            sarokhTaskBinding.txtDealerName.setText(AppController.getInstance().resSarokhTaskDriverData.getLocationName());
        }
        sarokhTaskBinding.txtDealerId.setText(AppController.getInstance().resSarokhTaskDriverData.getReceiverName());
    */
    }



    @Override
    public void onResume() {
        super.onResume();
        if (AppController.getInstance().resSarokhTask == null) {
            loadData();
            return;
        }
        /*if (AppController.getInstance().resSarokhTask.getData() == null) {
            loadData();
            return;
        }*/
        initUi();
    }

    private void loadData() {
        try {
            sarokhTaskBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
           //Toast.makeText(getActivity(), "TASK ID = "+AppController.getInstance().resSarokhTask.getData().getTaskId(), Toast.LENGTH_LONG).show();

            Log.d("DRIVERID", String.valueOf(AppController.getInstance().resSarokhTaskDriverData.getId()));
//            networkLoader.loadJsonRequest(AppConstants.END_POST_POINT_SUMMARY+""+AppController.getInstance().resSarokhTask.getData().getTaskId(),1);
            networkLoader.loadJsonRequest(AppConstants.END_POST_POINT_SUMMARY + "" + AppController.getInstance().resSarokhTaskDriverData.getId(), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        sarokhTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        Gson gson = new Gson();

        AppController.getInstance().resSarokhTask = gson.fromJson(jsonObject.toString(), ResSarokhTask.class);
        Log.d("JSONDATA",jsonObject.toString());
        Log.d("CASTEDDATA",AppController.getInstance().resSarokhTask.toString());

        if (AppController.getInstance().resSarokhTask.getData() == null) {
            try {
                sarokhTaskBinding.btnConfirm.setEnabled(false);
                sarokhTaskBinding.btnRecieveShipments.setEnabled(false);
                sarokhTaskBinding.btnRecieveShipments.animate().alpha(0.5f).setDuration(100);
                sarokhTaskBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else {
            if (AppController.getInstance().resSarokhTask.getData().getReceiveShipments() == 0) {


                Navigation.findNavController(sarokhTaskBinding.getRoot()).popBackStack();
                Navigation.findNavController(sarokhTaskBinding.getRoot()).navigate(R.id.fragmentPointTransactionGiveShipment);
                //Navigation.popBackStack(R.id.dest_id_of_B, true)
                // return;

                /*sarokhTaskBinding.btnConfirm.setEnabled(false);
                sarokhTaskBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                sarokhTaskBinding.btnRecieveShipments.setEnabled(false);
                sarokhTaskBinding.btnRecieveShipments.animate().alpha(0.5f).setDuration(100);*/
            }


            initUi();
        }
        /*if (AppController.getInstance().resSarokhTask.getData() == null) {
            try {
                sarokhTaskBinding.btnConfirmationShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.animate().alpha(0.5f).setDuration(100);
                sarokhTaskBinding.btnConfirmationShipment.animate().alpha(0.5f).setDuration(100);
                AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (AppController.getInstance().resSarokhTask.getData().getReceiveShipments() == 0) {
                sarokhTaskBinding.btnConfirmationShipment.setEnabled(false);
                sarokhTaskBinding.btnConfirmationShipment.animate().alpha(0.5f).setDuration(100);
                ;
                sarokhTaskBinding.btnReceivedShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.animate().alpha(0.5f).setDuration(100);
            }


            initUi();
        }*/
    }

    private void initUi() {

        if(AppController.getInstance().resSarokhTask.getData().getPointName()!=null){
            sarokhTaskBinding.textviewPointName.setText(AppController.getInstance().resSarokhTask.getData().getPointName());
        }

        if(AppController.getInstance().resSarokhTask.getData().getTaskId()!=null){
            sarokhTaskBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getTaskId());
        }



       // ll.removeViews(0, Math.max(0, ll.getChildCount() - 1));



        if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList()!=null){


            ll.removeAllViews();

            TableRow row1= new TableRow(getActivity());
            TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row1.setLayoutParams(lp1);
            TextView serialNumber1 = new TextView(getActivity());
            serialNumber1.setText("SR#");
            TextView trackingNumber1 = new TextView(getActivity());
            trackingNumber1.setText("Tracking No");
            TextView status1 = new TextView(getActivity());
            status1.setText("Status");
            row1.addView(serialNumber1);
            row1.addView(trackingNumber1);
            row1.addView(status1);
            row1.setBackgroundResource(R.drawable.table_row_border);
            ll.addView(row1,0);

            for (int i = 0; i <AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().size(); i++) {
                TableRow row= new TableRow(getActivity());
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                TextView serialNumber = new TextView(getActivity());
                serialNumber.setText(String.valueOf(i+1));
                TextView trackingNumber = new TextView(getActivity());
                trackingNumber.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getTrackingNumber().toString());
                TextView status = new TextView(getActivity());
                status.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getStatus());
                if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getStatus().equals("Assigned")){
                    status.setText("Waiting");
                }
                if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getStatus().equals("Completed")){
                    status.setText("Received From Point");
                }
                row.addView(serialNumber);
                row.addView(trackingNumber);
                row.addView(status);
                row.setBackgroundResource(R.drawable.table_row_border);
                ll.addView(row,i+1);
            }
        }


        /*if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.GONE);
        }

        if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.GONE);
        }
        sarokhTaskBinding.txtReceiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipments() + "");
        sarokhTaskBinding.txtGiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");
        sarokhTaskBinding.txtPayCodVal.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD() + "");
         */

    }

    @Override
    public void onResponse(String response, int type) {
        sarokhTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        sarokhTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        sarokhTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }


}
