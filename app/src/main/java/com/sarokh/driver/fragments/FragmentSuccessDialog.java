package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.print.PrintAttributes;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.databinding.FragmentSuccessLayoutBinding;


/**
 * Created by app.com on 01,08,2020
 */
public class FragmentSuccessDialog extends DialogFragment {
    private FragmentSuccessLayoutBinding binding;
    private CallbackForDialog callbackForDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_success_layout, container, false);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppController.getInstance().successM == null) {
            this.dismiss();
        } else {
            binding.txtTitle.setText(AppController.getInstance().successM.getTitle() == null ? "Success" : AppController.getInstance().successM.getTitle());
            binding.txtDescription.setText(AppController.getInstance().successM.getDescription());
            binding.btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    decideAboutAction();
                }
            });
          /*  if (AppController.getInstance().successM.getType() == 3) {
                binding.btnReceipt.setVisibility(View.VISIBLE);
            } else {
                binding.btnReceipt.setVisibility(View.GONE);
            }*/
        }
    }

    public void setCallbackForDialog(CallbackForDialog callbackForDialog) {
        this.callbackForDialog = callbackForDialog;
    }

    private void decideAboutAction() {
        if (AppController.getInstance().successM.getType() == 1 || AppController.getInstance().successM.getType() == 3) {
            callbackForDialog.onSuccessDialogDismiss(1);
            this.dismiss();
        } else if (AppController.getInstance().successM.getType() == 2) {
            createScreenshot();
        }
    }

    /*private void printReceipt() {
        String value = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "\n" +
                "<head>\n" +
                "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" +
                "    <title>Print Pdf</title>\n" +
                "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n" +
                "    <style>\n" +
                "        .print-order {\n" +
                "            width: 288px;\n" +
                "            margin: 0 auto;\n" +
                "        }\n" +
                "\n" +
                "        .form-row {\n" +
                "            display: -webkit-box;\n" +
                "            display: -ms-flexbox;\n" +
                "            display: flex;\n" +
                "            -ms-flex-wrap: wrap;\n" +
                "            flex-wrap: wrap;\n" +
                "            margin-right: -5px;\n" +
                "            margin-left: -5px;\n" +
                "        }\n" +
                "\n" +
                "        hr {\n" +
                "            margin-top: 1rem;\n" +
                "            margin-bottom: 1rem;\n" +
                "            border: 0;\n" +
                "            border-top: 1px solid rgba(0, 0, 0, .1);\n" +
                "            width: 100%;\n" +
                "        }\n" +
                "\n" +
                "        .mt-10 {\n" +
                "            margin-top: 10px;\n" +
                "        }\n" +
                "\n" +
                "        .print-heading {\n" +
                "            border-bottom: 1px solid rgba(0, 0, 0, .1);\n" +
                "        }\n" +
                "\n" +
                "        .print-heading img {\n" +
                "            width: 150px;\n" +
                "        }\n" +
                "    </style>\n" +
                "</head>\n" +
                "\n" +
                "<body>\n" +
                "    <div class=\"print-order\">\n" +
                "        <div class=\"print-heading\">\n" +
                "            <div class=\"form-row\">\n" +
                "                <div style=\" width: 63%; text-align: center; \">\n" +
                "                    <img class=\"mr-3\" src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEYAAAAdCAYAAAAXZxqwAAAABHNCSVQICAgIfAhkiAAABzxJREFUWIXN2X2M3EUZB/DP7Mtdu0dfKD0qtCwqB4Ig4AsiKPKqYDGmUBERkJKK4SWeosSIJKKCxmoQWaCgSNBAghGiIi8mhtICVZAXBVReazFHEbQHfaHd6/V2d/xj5nrXy10K/2zuSX7ZnfntzHznO8/zfWZmg0lq/fN6uvBz/Aa3z16zKrZz/EI7B3uz1j+vB87DQlyFnlzXNpuUxGAPfBG/Qj++gtBOAJOVmKMwF4+gDwsws50ASu0c7C3Y+zEVP5EWL2IvPNYuAJOOmKwls6TQKebqgF3aiWOyhlJ9TDlioJ0AJp3HoIyyKBoR3Jag0k4QbVX6HVn/vJ4i8RwVFxXnxmbz5TA1dMXBUNFsrQkbNcOps9eserEdWCZbKB2u07md8xvrSh9qDhamxZeK1bihY34jlg5sbRVd0T+vp9wOIJONmJKiog6NuC40Yz3s0uoPnYZsyUi70WoPkMllfZoGw/RYKsyNnaX9ShvDjBmloDm9+WxjkMEnZq9Z1WwHkMlGTL9oo2jfEHTrCKEwbZpQgPWDwuCj7QIy2ULpDQV/FwyhJCqgQCgo2Ii/tgvIZPOYFi4OnZbGaGEIFhJfIvyiMLP0gGh9u4Bsl67rteq275XevnZhGNfWzukJodMS0T2zX1q1ot3jB7YRshPeix40sBr/wIbxSMptZub3ccy7Ig61/cEvYhNW4b+V3r5xs0u9Vg2YGaMDWq+EQ0JX7C/M8Bj+hcHRWOq16lwclItDeKjS27epXquW8WG2bQpbeS57Ykauex2PDOOo16oHY/fh35fyBHvwM3zUyPmkgadwUb1WXTF28pI+XYXL8mRH2xRciQ+OqY9Yi1vqteqSSm/f/8aQUsEi9Iagp7h7HMYygPvxzXqt+sQoLMfgl9ICv4Yj6rXqczgXP0JnHnM5zpYOpR/IbR/ECUaOH1/DGfn71gI6UMPRebINNDNBB+Ek42vRYTgV54wOwR1YwK64EDfXa9VZo0jpwuUZ/N4xWhdbnoktq2M6HhyPu3DsDsY7AZdKpMCzEimvvVmQJCJ6JOZJ7F2Kr+NPuAOXVXr7hkY3yqHyjTz4aXj7DsZ5CL+XwrMlEXQMzsr9BXwO50uLckPz6cJnB64uP77l+vIydWfgScnVr8FEzBwoeerwSXwNzqr09r1lwSxgtuQ1MqhnsBSfxOJKb9/acdodiePy992xeAereIV02XQk/pzrSjgxk9wtkVLGDbhw692l9QbDp+KmMH/g2o4V0jVnn7SQZ48zRheWYO9cXosvYLy9T5QiolKvVbuyt24XFQW8gFdyeVoGditOwU5jJ5yF7atGXLUorfaeE5ACsdLbFyu9fWtw+6j6bmlR3oV9RK8N/aWwrL6kYxqm5767pEur1ZKeFHFcxjHapmQMAW/gIvxxguwasL900X5Xfo4eS8yruAQbjFwILZC85m4syKs6bB8d1cnwHUkVp+9Ia3LIvGNU1QZJ03bLE3u18VRxN8F+eJvkQVMxPU/w8dxujkTYRPZT3DpOwhhtM3CEdI16VO5zmxVy41twuqTemyRXK0sxu1RKveq1aod0MT1VSo+X58+SpOhzJwDx7nqtejQulnVFCtsHcvstiIIpU84c+h12xrcyhg6c07/7XpUYTc9tt0qETnTSPhxzdrBQrTz28LPd9qEw6kf3SxnoE1Kqez2/2w0n12vVgiSYw96yAlfj4VzuwSkTgLkEd+K70t4mSlpzXf0HHVO2Li/unMebGzeEj4l6sa/kwQHnKbhOy8Lc9llslsJqeJMa80PKmFeyjcjx7J95vvPzc992xNRr1U4pHm+SNnkr8e088WGbLXnJl6RNU8DL+HT+JK3eImNcMltJ0qThhXgOZ9Z/2FFXcG7jqcKBcchDqITueGFxn9ZKMb4gaoqGhPh8x/xmMxSdgMHYcFt9ScfYMTbiHsmTQp70d8bRomFbj+WV3r57K71990qSsh3gpVIYlHBAJmS6kaxD2sAdIenL8Ap9Hmfa/lixXwZ08xgQl0hZ4lqJ4D3jZh8RfVxwmi2hPnRfsdZxfPOgUHBwx4LGjNgfljVfDHfp0Fnsae0VunwG5TjkjoFry0dFThp6uPBY+dDWWkF3JuT7ktecKHnT+fh3vVa9agJyJrSStMc4WXLxfaUMMdpekMLge0YEby0Gx/Szq6QHZ+MPY/pYJe1j5uPk2LCy8WRxnpT5yoLpjScLp4VZ8cbyIa2zQvDO0B0XF7rjkER8GTE2/XbLTeU+g+GCEJSHHiy+rzA33lncIy7K46zDl6V91f4Zz+V4XtKzN20F3IhjpRS9zogHbMBt0v5hlvRfz2YptR+L94x6DsPfJOHukVS+nsub0Kz09jVwWeSBgWvKrwytLCwWNPL7zYQ5Q8uLp2/5den62LAET0uivAHLWv0uGKiVN8Z1YZFgKzZrhTlDjxa3SOFcl7RyteQp/8l9w4+l/c3AKExJ8EdscNS7Tf8H9qlplPmxvjUAAAAASUVORK5CYII=\">\n" +
                "                </div>\n" +
                "                <div class=\"text-left \" style=\"width: 30%; text-align: center; \">\n" +
                "                    <img src=\"http://app.sarokh.net/web/qrcode_files/00001000001.png\" alt=\"Logo\" style=\"width: 80px;\">\n" +
                "                </div>\n" +
                "            </div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Receiver Name</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">ameen</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Receiver Contact</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">0310-5021450</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Receiver Address</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">dfadfdf</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div class=\"col-sm-6 \">&nbsp;</div>\n" +
                "            <div class=\"col-sm-6 \">&nbsp;</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Receiver City</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">dfadfdf</div>\n" +
                "        </div>\n" +
                "        <hr>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Shipper Name</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">dfadfdf</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Phone Number</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">0310-5021450</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Address</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \"></div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div class=\"col-sm-6 \">&nbsp;</div>\n" +
                "            <div class=\"col-sm-6 \">&nbsp;</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">City</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">Jeddah</div>\n" +
                "        </div>\n" +
                "        <hr>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Pick up Date</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">Sat Aug 29 2020</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Delivery Date</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">dfadfdf</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Piece</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">dfadfdf</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Service</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">COD</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Weight</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">Upto 5 kg</div>\n" +
                "        </div>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 50%; text-align: center; \">Note</div>\n" +
                "            <div style=\"width: 50%; text-align: center; \">Gift Packaging,</div>\n" +
                "        </div>\n" +
                "        <hr>\n" +
                "        <div class=\"form-row mt-10 \" style=\"width: 100%; \">\n" +
                "            <div style=\"width: 70%; text-align: center; \">\n" +
                "                <img src=\"http://app.sarokh.net/web/barcode_files/00001000001.png \" alt=\"Logo \" style=\"width: 100%; \">\n" +
                "                <p>00001000001</p>\n" +
                "            </div>\n" +
                "            <div class=\"text-left font40 \" style=\"width: 30%; text-align: center; \">JED</div>\n" +
                "        </div>\n" +
                "    </div>\n" +
                "</body>\n" +
                "\n" +
                "</html>";
        new CreatePdf(getActivity())
                .setPdfName(AppConstants.getCurDateAndTime() + "_receipt")
                .openPrintDialog(true)
                .setContentBaseUrl(null)
                .setContent(value)
                .setPageSize(PrintAttributes.MediaSize.ISO_A4)
                //   .setFilePath(Environment.getExternalStorageDirectory().getAbsolutePath() + "/MyPdf")
                .setCallbackListener(new CreatePdf.PdfCallbackListener() {
                    @Override
                    public void onFailure(@NonNull String s) {
                        // handle error
                        Log.d("code", s);
                    }

                    @Override
                    public void onSuccess(@NonNull String s) {
                        // do your stuff here
                        Log.d("code", s);
                    }
                })
                .create();
    }*/

    private void createScreenshot() {

    }
}
