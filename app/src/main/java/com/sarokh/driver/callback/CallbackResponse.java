package com.sarokh.driver.callback;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 22,02,2020
 */
public interface CallbackResponse {
    void onResponse(JSONObject jsonObject, int type);
    void onResponse(String response, int type);

    void onResponse(JSONArray jsonArray, int type);

    void onAppError(String msg, int type);
}
