package com.sarokh.driver.dialogs;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TimePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.sarokh.driver.callback.CallbackDateAndTime;

import java.util.Calendar;

/**
 * Created by becody.com on 01,03,2020
 */
public class TimePickerDialogUtils extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private CallbackDateAndTime callbackDateAndTime;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int hrs = calendar.get(Calendar.HOUR_OF_DAY);
        int mint = calendar.get(Calendar.MINUTE);
        return new TimePickerDialog(getActivity(), this, hrs, mint,false);
    }

    public void setCallbackDateAndTime(CallbackDateAndTime dateAndTime) {
        this.callbackDateAndTime = dateAndTime;
    }


    @Override
    public void onTimeSet(TimePicker timePicker, int hrs, int mint) {
        callbackDateAndTime.onDataAndTimeSelected(false, hrs + ":" + mint);
    }
}
