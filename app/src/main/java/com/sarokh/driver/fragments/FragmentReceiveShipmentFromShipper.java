package com.sarokh.driver.fragments;

import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentReceiveShipmentFromShipperBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTask;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


// TODO
public class FragmentReceiveShipmentFromShipper extends Fragment implements CallbackResponse {

    private FragmentReceiveShipmentFromShipperBinding fragmentReceiveShipmentFromShipperBinding;

    TableLayout ll;


    public FragmentReceiveShipmentFromShipper() {
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentReceiveShipmentFromShipperBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_receive_shipment_from_shipper,container,false);


        fragmentReceiveShipmentFromShipperBinding.btnReceiveShipments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resSarokhTask.getData() == null)
                    return;
                Bundle bundle = new Bundle();
                String[] shipmentList = AppController.getInstance().resSarokhTask.getData().getReceiveShipmentList();
                bundle.putStringArray("receiveShipmentList",shipmentList);
                Navigation.findNavController(fragmentReceiveShipmentFromShipperBinding.getRoot()).navigate(R.id.actionReceiveShipment,bundle);
            }
        });

        fragmentReceiveShipmentFromShipperBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               // Navigation.findNavController(fragmentReceiveShipmentFromShipperBinding.getRoot()).navigate(R.id.actionSarokhTaskDriver);
               // getActivity().getFragmentManager().popBackStack();
                try {
                    fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
                    NetworkLoader networkLoader = new NetworkLoader(FragmentReceiveShipmentFromShipper.this);
                    JSONObject jsonObject = new JSONObject();
                    networkLoader.loadJsonRequest(AppConstants.SHIPPER_WAREHOUSE_TASK_COMPLETION + "" + AppController.getInstance().resSarokhTaskDriverData.getId(), 2);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        return fragmentReceiveShipmentFromShipperBinding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        loadData();
        if (AppController.getInstance().resSarokhTask == null) {

            return;
        }
        /*if (AppController.getInstance().resSarokhTask.getData() == null) {
            loadData();
            return;
        }*/
        initUi();
    }

    private void loadData() {
        try {
            fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            networkLoader.loadJsonRequest(AppConstants.END_POST_POINT_SUMMARY + "" + AppController.getInstance().resSarokhTaskDriverData.getId(), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {


        if(type == 1){

            fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.GONE);
            Gson gson = new Gson();

            AppController.getInstance().resSarokhTask = gson.fromJson(jsonObject.toString(), ResSarokhTask.class);
            Log.d("JSONDATA",jsonObject.toString());
            Log.d("CASTEDDATA",AppController.getInstance().resSarokhTask.toString());

            if (AppController.getInstance().resSarokhTask.getData() == null) {
                try {
                    fragmentReceiveShipmentFromShipperBinding.btnConfirm.setEnabled(false);
                    fragmentReceiveShipmentFromShipperBinding.btnReceiveShipments.setEnabled(false);
                    fragmentReceiveShipmentFromShipperBinding.btnReceiveShipments.animate().alpha(0.5f).setDuration(100);
                    fragmentReceiveShipmentFromShipperBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                    AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {

                if (AppController.getInstance().resSarokhTask.getData().getReceiveShipments() == 0) {
                    fragmentReceiveShipmentFromShipperBinding.btnConfirm.setEnabled(false);
                    fragmentReceiveShipmentFromShipperBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);

                    fragmentReceiveShipmentFromShipperBinding.btnReceiveShipments.setEnabled(false);
                    fragmentReceiveShipmentFromShipperBinding.btnReceiveShipments.animate().alpha(0.5f).setDuration(100);
                }


                initUi();
            }

        }
        else{

            try {
                AppConstants.showDialogWithoutTitle(getContext(), (String) jsonObject.get("message"));
                getActivity().onBackPressed();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



        /*if (AppController.getInstance().resSarokhTask.getData() == null) {
            try {
                sarokhTaskBinding.btnConfirmationShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.animate().alpha(0.5f).setDuration(100);
                sarokhTaskBinding.btnConfirmationShipment.animate().alpha(0.5f).setDuration(100);
                AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (AppController.getInstance().resSarokhTask.getData().getReceiveShipments() == 0) {
                sarokhTaskBinding.btnConfirmationShipment.setEnabled(false);
                sarokhTaskBinding.btnConfirmationShipment.animate().alpha(0.5f).setDuration(100);
                ;
                sarokhTaskBinding.btnReceivedShipment.setEnabled(false);
                sarokhTaskBinding.btnReceivedShipment.animate().alpha(0.5f).setDuration(100);
            }


            initUi();
        }*/
    }

    private void initUi() {

        if(AppController.getInstance().resSarokhTask.getData().getLocationName()!=null){
            fragmentReceiveShipmentFromShipperBinding.textviewPointName.setText(AppController.getInstance().resSarokhTask.getData().getLocationName());
        }

        if(AppController.getInstance().resSarokhTask.getData().getTaskId()!=null){
            fragmentReceiveShipmentFromShipperBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getTaskId());
        }
        if(AppController.getInstance().resSarokhTask.getData().getManagerName()!=null){
            fragmentReceiveShipmentFromShipperBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getManagerName());
        }




        if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList()!=null){
            ll = (TableLayout)  fragmentReceiveShipmentFromShipperBinding.receiveShipmentsTable;

            ll.removeAllViews();
            TableRow row1= new TableRow(getActivity());
            TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row1.setLayoutParams(lp1);
            TextView serialNumber1 = new TextView(getActivity());
            serialNumber1.setText("SR#");
            TextView trackingNumber1 = new TextView(getActivity());
            trackingNumber1.setText("Tracking No");
            TextView status1 = new TextView(getActivity());
            status1.setText("Status");
            row1.addView(serialNumber1);
            row1.addView(trackingNumber1);
            row1.addView(status1);
            row1.setBackgroundResource(R.drawable.table_row_border);
            ll.addView(row1,0);

            for (int i = 0; i <AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().size(); i++) {
                TableRow row= new TableRow(getActivity());
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                TextView serialNumber = new TextView(getActivity());
                serialNumber.setText(String.valueOf(i+1));
                TextView trackingNumber = new TextView(getActivity());
                trackingNumber.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getTrackingNumber().toString());
                TextView status = new TextView(getActivity());
                if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getStatus().equals("Assigned")){
                    status.setText("Waiting");
                }
                if(AppController.getInstance().resSarokhTask.getData().getReceiveShipmentsList().get(i).getStatus().equals("Completed")){
                    status.setText("Received From Point");
                }
                row.addView(serialNumber);
                row.addView(trackingNumber);
                row.addView(status);
                row.setBackgroundResource(R.drawable.table_row_border);
                ll.addView(row,i+1);
            }
        }


        /*if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.GONE);
        }

        if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.GONE);
        }
        sarokhTaskBinding.txtReceiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipments() + "");
        sarokhTaskBinding.txtGiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");
        sarokhTaskBinding.txtPayCodVal.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD() + "");
         */
    }
    @Override
    public void onResponse(String response, int type) {
        fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.GONE);
    }
    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.GONE);
    }
    @Override
    public void onAppError(String msg, int type) {
        fragmentReceiveShipmentFromShipperBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

}