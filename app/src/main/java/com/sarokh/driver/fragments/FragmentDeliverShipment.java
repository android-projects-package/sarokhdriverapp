package com.sarokh.driver.fragments;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.adapters.AdapterCodLedger;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDeliverShipmentBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResCodLedger;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
public class FragmentDeliverShipment extends Fragment implements CallbackResponse {

    FragmentDeliverShipmentBinding fragmentDeliverShipmentBinding;

    Integer taskId;
    String tripId;

    public FragmentDeliverShipment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onResume() {

        super.onResume();
        
        loadData();
    
    }

    private void loadData() {

        if (taskId == 0) {
            return;
        }

        fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.VISIBLE);

        NetworkLoader networkLoader = new NetworkLoader(this);

        networkLoader.loadJsonRequest(AppConstants.GET_LAST_MILE_TASK+taskId,1);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentDeliverShipmentBinding = DataBindingUtil.inflate(inflater,R.layout.fragment_deliver_shipment,container,false);
        taskId = getArguments().getInt("taskId");
        tripId = getArguments().getString("tripId");


        fragmentDeliverShipmentBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(fragmentDeliverShipmentBinding.editTextPinCode.getText().toString().equals("")){
                    AppConstants.showDialogWithoutTitle(getContext(),"Please Provide Pin Code");
                    return;
                }

                NetworkLoader networkLoader = new NetworkLoader(FragmentDeliverShipment.this);

                JSONObject jsonObject = new JSONObject();

                try {

                    jsonObject.put("tripId",tripId);

                    jsonObject.put("otpCode",fragmentDeliverShipmentBinding.editTextPinCode.getText().toString());

                    jsonObject.put("driverId",AppController.getInstance().resLoginObj.getData().getId());

                    jsonObject.put("trackingNo",fragmentDeliverShipmentBinding.textviewTrackingNumber.getText());

                    jsonObject.put("taskId",taskId);

                    fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
                    networkLoader.sendRequest(Request.Method.POST,AppConstants.DELIVER_LAST_MILE_SHIPMENT,2,jsonObject);


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

        return fragmentDeliverShipmentBinding.getRoot();

    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {

        fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

        if(jsonObject!=null){

            if(type == 1){

                try {

                    JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                    String trackingNo =   jsonObject1.getString("trackingNo");

                    String receiverName = jsonObject1.getString("receiverName");

                    String contact = jsonObject1.getString("contact");

                    String codAmount = "SR ";


                    if(jsonObject1.getString("codAmount").toString().equals("null")){
                        codAmount +="0";
                    }
                    else{
                        codAmount += jsonObject1.getString("codAmount");

                    }


                    codAmount += " /-";

                    fragmentDeliverShipmentBinding.textviewTrackingNumber.setText(trackingNo);

                    fragmentDeliverShipmentBinding.textviewManagerName.setText(receiverName);

                    fragmentDeliverShipmentBinding.textViewCodAmount.setText(codAmount);

                    fragmentDeliverShipmentBinding.textViewReceiverContact.setText(contact);


                } catch (JSONException e) {

                    e.printStackTrace();

                }
            }
            else{
                try {
                    AppConstants.showDialogWithoutTitle(getContext(),jsonObject.getString("message"));
                    Navigation.findNavController(fragmentDeliverShipmentBinding.getRoot()).navigate(R.id.actionSarokhTaskDriver);

                    /*fragmentDeliverShipmentBinding.textViewCodAmount.setText("");
                    fragmentDeliverShipmentBinding.editTextPinCode.setText("");
                    fragmentDeliverShipmentBinding.textviewTrackingNumber.setText("");
                    fragmentDeliverShipmentBinding.textviewManagerName.setText("");
                    fragmentDeliverShipmentBinding.btnConfirm.setEnabled(false);*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        //AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.toString());
    }

    @Override
    public void onResponse(String response, int type) {

        fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onAppError(String msg, int type) {

        fragmentDeliverShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);
        if(msg.equals("Shipment Delivered!")){
            AppConstants.showDialogWithoutTitle(getActivity(), msg);
            Navigation.findNavController(fragmentDeliverShipmentBinding.getRoot()).navigate(R.id.actionSarokhTaskDriver);
        }
        else{
            AppConstants.showDialogWithoutTitle(getActivity(), msg);
        }

    }
}