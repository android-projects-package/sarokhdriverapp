package com.sarokh.driver.ui.deliver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.activities.SearchShipmentResultDisplay;
import com.sarokh.driver.callback.CallbackRecyclerViewClick;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentChildDeliverBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResDeliverOrderStatus;
import com.sarokh.driver.response.ResSearchShipment;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;
import com.sarokh.driver.utils.LocalFile;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class ChildFragment extends Fragment implements CallbackResponse, CallbackRecyclerViewClick<ResSearchShipment.Data> {
    private FragmentChildDeliverBinding binding;
    private Context _context;
    private List<ResDeliverOrderStatus.Data> dataList;
    private LocalFile localFile;
    private NetworkLoader networkLoader;
    private AdapterChildDeliver adapterChildDeliver;
    private boolean isActive = false;

    public static ChildFragment newInstance(boolean isActive) {

        Bundle args = new Bundle();
        args.putBoolean("isActive", isActive);
        ChildFragment fragment = new ChildFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_child_deliver, container, false);
        _context = getContext();
        isActive = getArguments().getBoolean("isActive");
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();

    }

    private void init() {
        dataList = new ArrayList<>();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(_context);
        localFile = new LocalFile(_context);
        networkLoader = new NetworkLoader(this);
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.listing.setLayoutManager(linearLayoutManager);

    }

    private void loadData() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            if (isActive) {
            } else {
                jsonObject.put("orderStatus", "Completed");
                networkLoader.sendRequest(Request.Method.POST, AppConstants.END_DELIVER_ORDER_STATUS, 2, jsonObject);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        try {
            binding.progressBar.progressBar.setVisibility(View.GONE);
            if (type == 2) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                Gson gson = new Gson();
                Type founderListType = new TypeToken<ArrayList<ResDeliverOrderStatus.Data>>() {
                }.getType();
                dataList = gson.fromJson(jsonArray.toString(), founderListType);
                adapterChildDeliver = new AdapterChildDeliver(dataList);
                binding.listing.setAdapter(adapterChildDeliver);
            } else if (type == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                Gson gson = new Gson();
                Type founderListType = new TypeToken<ArrayList<ResSearchShipment.Data>>() {
                }.getType();
                List<ResSearchShipment.Data> resultForDisplay = gson.fromJson(jsonArray.toString(), founderListType);
                AdapterChildDeliverActive adapterChildDeliver = new AdapterChildDeliverActive(resultForDisplay, this);
                binding.listing.setAdapter(adapterChildDeliver);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {

    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(_context,msg);
    }

    @Override
    public void onClickRow(ResSearchShipment.Data modal) {
        Intent intent = new Intent(_context, SearchShipmentResultDisplay.class);
        AppController.getInstance().resShipmentData = modal;
        startActivity(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }
}
