package com.sarokh.driver.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.activities.CashHandOverSignatureActivity;
import com.sarokh.driver.callback.CallbackDateAndTime;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentCashHandOverBinding;
import com.sarokh.driver.dialogs.DatePickerDialogUtils;
import com.sarokh.driver.dialogs.TimePickerDialogUtils;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResCashHandOver;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;
import com.sarokh.driver.utils.LocalFile;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by becody.com on 29,02,2020
 */
public class CashHandOverFragment extends Fragment implements CallbackResponse, RadioGroup.OnCheckedChangeListener, CallbackDateAndTime {
    private FragmentCashHandOverBinding binding;
    private Context _context;
    private LocalFile localFile;
    private NetworkLoader networkLoader;
    private ResCashHandOver resCashHandOver;
    private ListAdapter listAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cash_hand_over, container, false);
        _context = getContext();
        return binding.getRoot();
    }

    private void loadData() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnProceedToNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmitVal();
            }
        });
        binding.txtInputDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDataPickerDialog();
            }
        });
        binding.txtInputTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showTimePickerDialog();
            }
        });
        binding.edtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                binding.txtDisplayAmount.setText(_context.getString(R.string.currency) + " " + editable.toString());
            }
        });
        localFile = new LocalFile(_context);
        networkLoader = new NetworkLoader(this);
        loadData();
    }

    private void showDataPickerDialog() {
        DatePickerDialogUtils datePickerDialogUtils = new DatePickerDialogUtils();
        datePickerDialogUtils.setCallbackDateAndTime(this);
        datePickerDialogUtils.show(getChildFragmentManager(), "DateUtils");
    }

    private void showTimePickerDialog() {
        TimePickerDialogUtils datePickerDialogUtils = new TimePickerDialogUtils();
        datePickerDialogUtils.setCallbackDateAndTime(this);
        datePickerDialogUtils.show(getChildFragmentManager(), "TimeUtils");
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        Gson gson = new Gson();
        resCashHandOver = gson.fromJson(jsonObject.toString(), ResCashHandOver.class);
        binding.rdoGrp.setOnCheckedChangeListener(this);
        initVal();
    }

    private void initVal() {
        if (resCashHandOver != null) {
            if (binding.rdoBtnAgentCollection.isChecked()) {
                binding.spinner.setItems(getCollectionAgentList(resCashHandOver.getData().getCollectionAgentsList()));
            } else if (binding.rdoBtnDriver.isChecked()) {
                binding.spinner.setItems(getDriversList(resCashHandOver.getData().getDriversList()));
            }
            binding.btnWalledVal.setText(_context.getResources().getString(R.string.currency) + "" + resCashHandOver.getData().getCashInWallet());
        }
    }

    private String[] getCollectionAgentList(List<ResCashHandOver.CollectionAgentsList> collectionAgentsLists) {
        String[] items = new String[collectionAgentsLists.size()];
        for (int i = 0; i < collectionAgentsLists.size(); i++) {
            items[i] = collectionAgentsLists.get(i).getName();
        }

        return items;
    }

    private String[] getDriversList(List<ResCashHandOver.DriversList> driversLists) {
        String[] items = new String[driversLists.size()];
        for (int i = 0; i < driversLists.size(); i++) {
            items[i] = driversLists.get(i).getFirstName() + " " + driversLists.get(i).getLastName();
        }

        return items;
    }


    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(_context, msg);
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.rdoBtnAgentCollection:
            case R.id.rdoBtnDriver:
                initVal();
                break;
        }
    }

    @Override
    public void onDataAndTimeSelected(boolean isDate, String val) {
        if (isDate) {
            binding.txtInputDate.setText(val);
        } else {
            binding.txtInputTime.setText(val);
        }
    }

    private void onSubmitVal() {
        try {
            if (binding.edtAmount.getText().toString().isEmpty()) {
                binding.inputAmount.setError("Enter amount");
                binding.inputAmount.setErrorEnabled(true);
                return;
            }

            binding.inputAmount.setErrorEnabled(false);

            if (binding.txtInputDate.getText().toString().isEmpty()) {
                binding.txtInputDate.setError("Enter Date");
                return;
            }
            binding.txtInputDate.setError(null);

            if (binding.txtInputTime.getText().toString().isEmpty()) {
                binding.txtInputTime.setError("Enter Time");
            }

            binding.txtInputTime.setError(null);

            JSONObject jsonObject = new JSONObject();
            if (binding.rdoBtnDriver.isChecked()) {
                ResCashHandOver.DriversList driversList = resCashHandOver.getData().getDriversList().get(binding.spinner.getSelectedIndex());
                jsonObject.put("driverId", driversList.getId());
                jsonObject.put("receiverName", driversList.getFirstName() + " " + driversList.getLastName());
                jsonObject.put("agentId", 0);
            } else if (binding.rdoBtnAgentCollection.isChecked()) {
                ResCashHandOver.CollectionAgentsList collectionAgentsList = resCashHandOver.getData().getCollectionAgentsList().get(binding.spinner.getSelectedIndex());
                jsonObject.put("driverId", 0);
                jsonObject.put("agentId", collectionAgentsList.getId());
                jsonObject.put("receiverName", collectionAgentsList.getName());
            }
            jsonObject.put("paymentDate", binding.txtInputDate.getText() + " " + binding.txtInputTime.getText());
            jsonObject.put("amount", Float.parseFloat(binding.edtAmount.getText().toString().trim()));
            AppController.getInstance().cashHandOverObj = jsonObject;
            Intent intent = new Intent(_context, CashHandOverSignatureActivity.class);
            intent.putExtra(AppConstants.KEY_FOR_SIGNATURE, true);
            startActivity(intent);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
