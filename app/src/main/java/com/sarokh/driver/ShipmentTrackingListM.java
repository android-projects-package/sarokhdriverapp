package com.sarokh.driver;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by becody.com on 22,04,2020
 */
public class ShipmentTrackingListM {
    private int shipperId;
    private String shipperName;
    private List<String> trackingList;

    public int getShipperId() {
        return shipperId;
    }

    public void setShipperId(int shipperId) {
        this.shipperId = shipperId;
    }

    public String getShipperName() {
        return shipperName;
    }

    public void setShipperName(String shipperName) {
        this.shipperName = shipperName;
    }

    public List<String> getTrackingList() {
        if (trackingList == null) {
            return new ArrayList<>();
        }
        return trackingList;
    }

    public void setTrackingList(List<String> trackingList) {
        this.trackingList = trackingList;
    }
}
