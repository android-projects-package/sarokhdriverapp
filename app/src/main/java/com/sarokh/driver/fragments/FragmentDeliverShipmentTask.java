package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDeliverShipmentTaskBinding;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSearchTrackingNo;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by app.com on 12,04,2020
 */
public class FragmentDeliverShipmentTask extends BaseFragment implements CallbackResponse, CallbackForDialog {

    private FragmentDeliverShipmentTaskBinding fragmentDeliverShipmentTaskBinding;
    private ResSearchTrackingNo resSearchTrackingNo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentDeliverShipmentTaskBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_deliver_shipment_task, container, false);
        return fragmentDeliverShipmentTaskBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentDeliverShipmentTaskBinding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentDeliverShipmentTaskBinding.edtTrackingNumber.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.edtTrackingNumber.setError("Enter Tracking No");
                    return;
                }
                fragmentDeliverShipmentTaskBinding.edtTrackingNumber.setError(null);
                searchTrackingNo(fragmentDeliverShipmentTaskBinding.edtTrackingNumber.getText().toString().trim());
            }
        });
        fragmentDeliverShipmentTaskBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resSearchTrackingNo == null) {
                    printMessage("Enter Valid Tracking Info!");
                    return;
                }

                if (fragmentDeliverShipmentTaskBinding.edtOtp.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.edtOtp.setError("Enter OTP");
                    return;
                }

                if (!fragmentDeliverShipmentTaskBinding.edtOtp.getText().toString().trim().equals("1111")) {
                    fragmentDeliverShipmentTaskBinding.edtOtp.setError("OTP is Invalid");
                    return;
                }

                fragmentDeliverShipmentTaskBinding.edtOtp.setError(null);

                if (fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.edtPaidAmount.setError("Enter Paid Amount!");
                    return;
                }
                if (Float.parseFloat(fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString()) < resSearchTrackingNo.getData().getBilledAmount()) {
                    fragmentDeliverShipmentTaskBinding.edtPaidAmount.setError("Paid amount should be equal to billed amount.");
                    return;
                }

                fragmentDeliverShipmentTaskBinding.edtPaidAmount.setError(null);
                resSearchTrackingNo.getData().setOTP(fragmentDeliverShipmentTaskBinding.edtOtp.getText().toString());
                resSearchTrackingNo.getData().setPaidAmount(fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString());

                AppController.getInstance().resSearchTrackingNo = resSearchTrackingNo;

                Navigation.findNavController(fragmentDeliverShipmentTaskBinding.getRoot()).navigate(R.id.actionDeliverShipmentTaskConfirm);
            }
        });
        fragmentDeliverShipmentTaskBinding.btnConfirm.setEnabled(false);
        fragmentDeliverShipmentTaskBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
    }

    private void printMessage(String msg) {
        SuccessM successM = new SuccessM();
        successM.setDescription(msg);
        showErrorDialog(successM, this);
    }

    private void searchTrackingNo(String trackingNo) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_SEARCH_SHIPMENT_TRACKING_NO + trackingNo, 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        resSearchTrackingNo = new Gson().fromJson(jsonObject.toString(), ResSearchTrackingNo.class);
        if (resSearchTrackingNo == null) {
            fragmentDeliverShipmentTaskBinding.btnConfirm.setEnabled(false);
            fragmentDeliverShipmentTaskBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
        } else if (resSearchTrackingNo.getData() == null) {
            fragmentDeliverShipmentTaskBinding.btnConfirm.setEnabled(false);
            fragmentDeliverShipmentTaskBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
        } else {
            fragmentDeliverShipmentTaskBinding.btnConfirm.setEnabled(true);
            fragmentDeliverShipmentTaskBinding.btnConfirm.animate().alpha(1f).setDuration(100);
            initData();
        }

    }

    private void initData() {
        fragmentDeliverShipmentTaskBinding.txtBillAmount.setText(getString(R.string.currency) + "" + resSearchTrackingNo.getData().getBilledAmount());
        fragmentDeliverShipmentTaskBinding.txtReceiverName.setText(resSearchTrackingNo.getData().getReceiverName());
        fragmentDeliverShipmentTaskBinding.txtReceiverContact.setText(resSearchTrackingNo.getData().getMobile());
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    @Override
    public void onSuccessDialogDismiss(int type) {

    }

    @Override
    public void onErrorDialogDismiss(int type) {

    }
}
