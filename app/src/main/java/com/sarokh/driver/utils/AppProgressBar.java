package com.sarokh.driver.utils;

import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by bocody.com on 22,02,2020
 */
public class AppProgressBar {
    private Context context;
    private ProgressDialog progressHUD;


    public AppProgressBar(Context ctx) {
        this.context = ctx;
        progressHUD = new ProgressDialog(context);
        progressHUD.setCancelable(false);
    }

    public synchronized void showProgressDialog(String message) {

        try {
            if (!progressHUD.isShowing()) {
                progressHUD.setCancelable(false);
                String msg = message == null ? "Please wait..." : message;
                progressHUD.setMessage(msg);
                progressHUD.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized void updateProgressDialog(String message) {

        try {
            if (!progressHUD.isShowing()) {
                progressHUD.setCancelable(false);
                String msg = message == null ? "Please wait..." : message;
                progressHUD.setMessage(msg);
            } else {
                String msg = message == null ? "Please wait..." : message;
                progressHUD.setMessage(msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public synchronized void dismissProgressDialog() {
        try {
            if (progressHUD != null && progressHUD.isShowing()) {

                progressHUD.dismiss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
