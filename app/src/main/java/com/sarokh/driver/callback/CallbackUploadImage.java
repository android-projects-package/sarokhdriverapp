package com.sarokh.driver.callback;

import org.json.JSONObject;

/**
 * Created by becody.com on 6/5/18.
 */

public interface CallbackUploadImage {
    void uploadImageSuccess(JSONObject jsonObject);
    void uploadImageFailed(String msg);
    void uploadImageProgress(int progress);
}
