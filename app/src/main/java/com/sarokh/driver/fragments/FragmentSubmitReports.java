package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSumbitReportBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentSubmitReports extends Fragment implements CallbackResponse {
    private FragmentSumbitReportBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sumbit_report, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        String[] reports = getResources().getStringArray(R.array.reports_array);
        binding.spinner.setItems(reports);
        binding.btnSubmitReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().trim().isEmpty()) {
                    binding.inputTrackingNo.setErrorEnabled(true);
                    binding.inputTrackingNo.setError("Enter Tracking No");
                    return;
                }
                binding.inputTrackingNo.setErrorEnabled(false);

                if (binding.edtComplain.getText().toString().trim().isEmpty()) {
                    binding.inputComplain.setErrorEnabled(true);
                    binding.inputComplain.setError("Write Complain");
                    return;
                }

                binding.inputComplain.setErrorEnabled(false);

                submitReport();

            }
        });
    }

    private void submitReport() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("complaintAgainst", AppController.getInstance().resLoginObj.getData().getUser().getFullName());
            jsonObject.put("complaintAgainstName", binding.spinner.getText());
            jsonObject.put("complaintComments", binding.edtComplain.getText().toString().trim());
            jsonObject.put("trackingNumber", binding.edtTrackingNumber.getText().toString().trim());
            NetworkLoader networkLoader = new NetworkLoader(this);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_REPORT_ISSUE_SHIPMENT, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            CustomToast.showToast(getActivity(), jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}
