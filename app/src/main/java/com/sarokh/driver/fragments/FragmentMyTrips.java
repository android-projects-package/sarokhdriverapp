package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.adapters.AdapterCodLedger;
import com.sarokh.driver.adapters.AdapterMyTrips;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentCodLedgerDetailBinding;
import com.sarokh.driver.databinding.FragmentMyTripsBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResCodLedger;
import com.sarokh.driver.response.ResMyTrips;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentMyTrips extends Fragment implements CallbackResponse {
    private FragmentMyTripsBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_trips, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.listing.setLayoutManager(layoutManager);
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
    }

    private void loadData(boolean refresh) {
        if (refresh) {
            binding.swipeContainer.setRefreshing(true);
            binding.progressBar.progressBar.setVisibility(View.GONE);
        } else {
            binding.swipeContainer.setRefreshing(false);
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        }
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_MY_TRIPS + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData(false);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
        ResMyTrips resCodLedger = new Gson().fromJson(jsonObject.toString(), ResMyTrips.class);
        AdapterMyTrips codLedger = new AdapterMyTrips(resCodLedger.getData());
        if (resCodLedger.getData() == null) {
            binding.txtTotalTrips.setText("0");
        } else {
            binding.txtTotalTrips.setText(resCodLedger.getData().size() + "");
        }

        binding.listing.setAdapter(codLedger);
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
        AppConstants.showDialogWithoutTitle(getContext(), response);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.swipeContainer.setRefreshing(false);
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getContext(), msg);
    }
}
