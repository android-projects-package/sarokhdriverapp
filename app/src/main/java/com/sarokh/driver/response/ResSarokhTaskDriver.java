package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 14,04,2020
 */
public class ResSarokhTaskDriver {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        private int id;
        private String stopType;
        private String receiverType;
        private String receiverName;
        private String latitude;
        private String longitude;
        private String locationName;
        private boolean delivered;
        private int stopTypeId;
        public double distance;

        public String getLocationName() {
            return locationName;
        }

        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        public int getStopTypeId() {
            return stopTypeId;
        }

        public void setStopTypeId(int stopTypeId) {
            this.stopTypeId = stopTypeId;
        }

        public String getStopType() {
            return stopType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public void setStopType(String stopType) {
            this.stopType = stopType;
        }

        public String getReceiverType() {
            return receiverType;
        }

        public void setReceiverType(String receiverType) {
            this.receiverType = receiverType;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public boolean isDelivered() {
            return delivered;
        }

        public void setDelivered(boolean delivered) {
            this.delivered = delivered;
        }


        @Override
        public String toString() {
            return "Data{" +
                    "id=" + id +
                    ", stopType='" + stopType + '\'' +
                    ", receiverType='" + receiverType + '\'' +
                    ", receiverName='" + receiverName + '\'' +
                    ", latitude='" + latitude + '\'' +
                    ", longitude='" + longitude + '\'' +
                    ", locationName='" + locationName + '\'' +
                    ", delivered=" + delivered +
                    ", stopTypeId=" + stopTypeId +
                    ", distance=" + distance +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ResSarokhTaskDriver{" +
                "data=" + data +
                '}';
    }
}