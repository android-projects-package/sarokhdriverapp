package com.sarokh.driver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.databinding.ActivitySearchShipmentResultDisplayBinding;
import com.sarokh.driver.response.ResSearchShipment;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.LocalFile;

import org.json.JSONObject;

import java.util.Calendar;

/**
 * Created by becody.com on 29,02,2020
 */
public class SearchShipmentResultDisplay extends AppCompatActivity {
    private ActivitySearchShipmentResultDisplayBinding binding;
    private LocalFile localFile;
    private ResSearchShipment.Data result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_shipment_result_display);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        localFile = new LocalFile(this);
        result = AppController.getInstance().resShipmentData;
        binding.txtReceiverName.setText(result.getReceiverName());
        binding.txtMobile.setText(result.getMobile());
        binding.txtShipmentNumber.setText(result.getShipmentNo());
        binding.txtPaymentType.setText(result.getOrderType());
        if (result.getShipmentReceived()) {
            binding.edtPayment.setEnabled(false);
            binding.btnAddPayment.setEnabled(false);
        }
        binding.txtReceiverPickupTime.setText(result.getPickupDatetime());
        binding.txtDeliveryCharges.setText(getString(R.string.currency) + "" + result.getDeliveryCharges());
        binding.btnAddPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtPayment.getText().toString().isEmpty()) {
                    binding.inputPayment.setError("Enter payment");
                    binding.inputPayment.setErrorEnabled(true);
                    return;
                }
                binding.inputPayment.setErrorEnabled(false);
                addPayment();
            }
        });

    }

    private void addPayment() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("codcharges", binding.edtPayment.getText().toString().trim());
            jsonObject.put("receivedDateTime", getCurDateAndTime());
            jsonObject.put("receiverName", result.getReceiverName());
            jsonObject.put("shipmentNo", result.getShipmentNo());
            Intent intent = new Intent(SearchShipmentResultDisplay.this, CashHandOverSignatureActivity.class);
            AppController.getInstance().cashHandOverObj = jsonObject;
            intent.putExtra(AppConstants.KEY_FOR_SIGNATURE, true);
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private String getCurDateAndTime() {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        int hrs = calendar.get(Calendar.HOUR_OF_DAY);
        int mint = calendar.get(Calendar.MINUTE);
        return yy + "-" + mm + "-" + dd + " " + hrs + ":" + mint;
    }
}
