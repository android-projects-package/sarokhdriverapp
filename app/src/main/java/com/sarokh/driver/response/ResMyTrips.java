package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 15,04,2020
 */
public class ResMyTrips {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data{
        private int tripId;
        private String date;
        private String startTime;
        private String endTime;
        private int totalPickups;
        private int totalDeliveries;
        private int totalStops;
        private float paymentCollection;

        public int getTripId() {
            return tripId;
        }

        public void setTripId(int tripId) {
            this.tripId = tripId;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getStartTime() {
            return startTime;
        }

        public void setStartTime(String startTime) {
            this.startTime = startTime;
        }

        public String getEndTime() {
            return endTime;
        }

        public void setEndTime(String endTime) {
            this.endTime = endTime;
        }

        public int getTotalPickups() {
            return totalPickups;
        }

        public void setTotalPickups(int totalPickups) {
            this.totalPickups = totalPickups;
        }

        public int getTotalDeliveries() {
            return totalDeliveries;
        }

        public void setTotalDeliveries(int totalDeliveries) {
            this.totalDeliveries = totalDeliveries;
        }

        public int getTotalStops() {
            return totalStops;
        }

        public void setTotalStops(int totalStops) {
            this.totalStops = totalStops;
        }

        public float getPaymentCollection() {
            return paymentCollection;
        }

        public void setPaymentCollection(float paymentCollection) {
            this.paymentCollection = paymentCollection;
        }
    }

}
