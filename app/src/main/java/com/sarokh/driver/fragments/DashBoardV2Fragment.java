package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Scroller;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDashboardV2Binding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResDashboard;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Becody on 05,04,2020
 */
public class DashBoardV2Fragment extends Fragment implements CallbackResponse {
    private FragmentDashboardV2Binding binding;
    private ResDashboard resDashboard;



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard_v2, container, false);
        return binding.getRoot();
    }



    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);










        binding.txtDealerName.setText(AppController.getInstance().resLoginObj.getData().getUser().getFullName());
        if (AppController.getInstance().resLoginObj.getData().getUser().getUserType().equalsIgnoreCase("Driver")) {
            binding.lnrVehicleType.setVisibility(View.GONE);
        } else {
            binding.lnrVehicleType.setVisibility(View.VISIBLE);
            binding.txtOperator.setText(AppController.getInstance().resLoginObj.getData().getVehicle().getName());
        }
        binding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRecord(true);
            }
        });
        String profile = AppController.getInstance().resLoginObj.getData().getUser().getProfilePicture();
        Log.d("profile",profile);
        Picasso.get().load(AppController.getInstance().resLoginObj.getData().getUser().getProfilePicture()).resize(1000,1000).centerInside().into(binding.imgProfile, new Callback() {
            @Override
            public void onSuccess() {
                Log.d("profile","success");
            }

            @Override
            public void onError(Exception e) {
                Log.d("profile",e.getMessage());
            }
        });

        getPermission();    // to get permission to access current location

        String trackingNos = "00001000002\n00001000003\n00001000004\n00001000005\n00001000006\n00001000007\n" +
                "00001000002\n00001000002\n00001000002\n00001000002\n00001000002\n00001000002\n00001000002\n";

//        binding.textviewShipmentsScanned.setText(trackingNos);
   /*     binding.textviewShipmentsScanned.setScroller(new Scroller(getContext()));
        binding.textviewShipmentsScanned.setMaxLines(1);
        binding.textviewShipmentsScanned.setVerticalScrollBarEnabled(true);
        binding.textviewShipmentsScanned.setMovementMethod(new ScrollingMovementMethod());*/

    }

    private void getPermission() {
        Dexter.withActivity(this.getActivity())
                .withPermission(Manifest.permission.ACCESS_FINE_LOCATION)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse permissionGrantedResponse) {
//                        Toast.makeText(getContext(), "Granted", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse permissionDeniedResponse) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getContext().getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permissionRequest, PermissionToken permissionToken) {
                        permissionToken.continuePermissionRequest();
                    }
                }).check();
    }

    private void loadRecord(boolean refresh) {
        if (refresh) {
            binding.swipeContainer.setRefreshing(true);
            binding.progressBar.progressBar.setVisibility(View.GONE);
        } else {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        }
        NetworkLoader loader = new NetworkLoader(this);
        loader.loadJsonRequest(AppConstants.END_DASHBOARD + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
        resDashboard = new Gson().fromJson(jsonObject.toString(), ResDashboard.class);
        initRecord();
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRecord(false);
    }

    private void initRecord() {
        binding.availableShipmentVal.setText(resDashboard.getData().getAvailableShipments() + "");
        binding.txtPickedUp.setText(resDashboard.getData().getPickedUp() + "");
        binding.txtDroppedOff.setText(resDashboard.getData().getDroppedOff() + "");
        binding.txtCompletedStops.setText(resDashboard.getData().getCompletedStops() + "");
        binding.txtRemainingStops.setText(resDashboard.getData().getRemainingStops() + "");
        binding.txtTrips.setText(resDashboard.getData().getTrips() + "");
        binding.txtCargoCapacity.setText(resDashboard.getData().getCargoCapacity());
        binding.txtVehicle.setText(resDashboard.getData().getVehicle());
        binding.txtNxtStopDistance.setText(resDashboard.getData().getNextStopDistance()+"");
        binding.walletAvailableBalance.setText(AppConstants.priceFormat(getContext(), resDashboard.getData().getCollectionWalletBalance(), false));


    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
        AppConstants.showDialogWithoutTitle(getContext(),response);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.swipeContainer.setRefreshing(false);
       // CustomToast.showToast(getActivity(), msg);
       // AppConstants.showDialogWithoutTitle(getContext(),msg);
    }
}
