package com.sarokh.driver.activities;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.ActivityEdtProfileBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.AppProgressBar;
import com.sarokh.driver.utils.CustomToast;
import com.sarokh.driver.utils.LocalFile;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 17,02,2020
 */
public class EdtProfileActivity extends AppCompatActivity implements CallbackResponse {
    private ActivityEdtProfileBinding binding;
    private NetworkLoader networkLoader;
    private LocalFile localFile;
    private AppProgressBar appProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_edt_profile);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        networkLoader = new NetworkLoader(this);
        appProgressBar = new AppProgressBar(this);
        localFile = new LocalFile(this);
        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtOldPwd.getText().toString().isEmpty()) {
                    binding.inputOldPwd.setError("Old password required");
                    binding.inputOldPwd.setErrorEnabled(true);
                    return;
                }

                if (binding.edtOldPwd.getText().toString().trim().length() < 5) {
                    binding.inputOldPwd.setError("Minimum character should be 5");
                    binding.inputOldPwd.setErrorEnabled(true);
                    return;
                }

                binding.inputOldPwd.setErrorEnabled(false);

                if (binding.edtNewPwd.getText().toString().isEmpty()) {
                    binding.inputNewPwd.setError("New password required");
                    binding.inputNewPwd.setErrorEnabled(true);
                    return;
                }

                if (binding.edtNewPwd.getText().toString().trim().length() < 5) {
                    binding.inputNewPwd.setError("Minimum character should be 5");
                    binding.inputNewPwd.setErrorEnabled(true);
                    return;
                }

                binding.inputNewPwd.setErrorEnabled(false);


                if (binding.edtConfirmPwd.getText().toString().isEmpty()) {
                    binding.inputConfirmPwd.setError("New password required");
                    binding.inputConfirmPwd.setErrorEnabled(true);
                    return;
                }

                if (binding.edtConfirmPwd.getText().toString().trim().length() < 5) {
                    binding.inputConfirmPwd.setError("Minimum character should be 5");
                    binding.inputConfirmPwd.setErrorEnabled(true);
                    return;
                }

                binding.inputConfirmPwd.setErrorEnabled(false);

                if (!binding.edtNewPwd.getText().toString().trim().equals(binding.edtConfirmPwd.getText().toString().trim())) {
                    binding.inputConfirmPwd.setError("Confirm password does not match");
                    binding.inputConfirmPwd.setErrorEnabled(true);
                    return;
                }

                binding.inputConfirmPwd.setErrorEnabled(false);

                doChangePwd();

            }
        });
        setUpUi();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setUpUi() {
        /*binding.edtDealerName.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getDealerName());
        binding.edtBusinessName.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getBusinessName());
        binding.edtContactOwner.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getContactNoOwner());
        binding.edtContactBusiness.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getContactNoBusiness());
        binding.edtBusinessEmail.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getBusinessEmail());
        binding.edtBusinessTiming.setText(AppController.getInstance().resDashboard.getData().getDealerProfile().getBusinessTiming());*/
    }

    private void doChangePwd() {
        try {
            appProgressBar.showProgressDialog(null);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("currentPassword", binding.edtOldPwd.getText().toString().trim());
            jsonObject.put("newPassword", binding.edtNewPwd.getText().toString().trim());
            networkLoader.sendRequest(Request.Method.PATCH, AppConstants.END_USER_CHANGED_PWD, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onResponse(String response, int type) {
        appProgressBar.dismissProgressDialog();
        CustomToast.showToast(this, response);
        binding.edtNewPwd.setText("");
        binding.edtOldPwd.setText("");
        binding.edtConfirmPwd.setText("");
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onAppError(String msg, int type) {
        appProgressBar.dismissProgressDialog();
        CustomToast.showToast(this, msg);

    }
}
