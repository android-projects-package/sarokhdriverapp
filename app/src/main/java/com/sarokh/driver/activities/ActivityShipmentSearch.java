package com.sarokh.driver.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;

import com.sarokh.driver.R;
import com.sarokh.driver.databinding.ActivitySearchShipmentBinding;
import com.sarokh.driver.fragments.SearchShipmentFragment;

/**
 * Created by becody.com on 28,02,2020
 */
public class ActivityShipmentSearch extends AppCompatActivity {
    private ActivitySearchShipmentBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search_shipment);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, SearchShipmentFragment.newInstance()).commit();

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
