package com.sarokh.driver.fragments;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.sarokh.driver.AppController;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.modals.SuccessM;


/**
 * Created by app.com on 02,08,2020
 */
public abstract class BaseFragment extends Fragment {


    void showErrorDialog(SuccessM successM, CallbackForDialog callbackForDialog) {
        AppController.getInstance().successM = successM;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        FragmentErrorDialog fragmentErrorDialog = new FragmentErrorDialog();
        fragmentErrorDialog.setCallbackForDialog(callbackForDialog);
        fragmentErrorDialog.show(ft,"error_dialog");
    }

    void showSuccessDialog(SuccessM successM, CallbackForDialog callbackForDialog) {
        AppController.getInstance().successM = successM;
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        FragmentSuccessDialog fragmentSuccessDialog = new FragmentSuccessDialog();
        fragmentSuccessDialog.setCallbackForDialog(callbackForDialog);
        fragmentSuccessDialog.show(ft,"success_dialog");
    }
}
