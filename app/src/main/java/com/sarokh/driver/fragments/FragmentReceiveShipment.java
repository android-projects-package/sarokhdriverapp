package com.sarokh.driver.fragments;
import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentReceiveShipmentBinding;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResReceiveShipmentDetail;
import com.sarokh.driver.response.ResSarokhTask;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
/**
 * Created by app.com on 12,04,2020
*/
public class FragmentReceiveShipment extends BaseFragment implements CallbackResponse, CallbackForDialog {
    private FragmentReceiveShipmentBinding binding;
    private CodeScanner mCodeScanner;
    ListView list;
    private List<String> List_file;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_receive_shipment, container, false);
        return binding.getRoot();
    }


    private void updateRecord(List<ResReceiveShipmentDetail.Data> dataList) {
        Log.d("DATALIST",dataList.toString());
        int shipment = 0;
        int remainingShipment = dataList.size();
        for (ResReceiveShipmentDetail.Data obj : dataList) {
            if (obj.isConfirmed()) {
                shipment++;
                remainingShipment--;
            }
        }
        if (remainingShipment <= 0) {
            binding.btnNext.setEnabled(false);
            binding.btnNext.animate().alpha(0.5f).setDuration(100);
        }
        binding.txtConfirmShipment.setText(shipment + "");
        binding.txtRemaining.setText(remainingShipment + "");
        ResSarokhTask resSarokhTask = AppController.getInstance().resSarokhTask;
        ResSarokhTask.Data data = resSarokhTask.getData();
        data.setRemainingShipments(remainingShipment);
        resSarokhTask.setData(data);
        AppController.getInstance().resSarokhTask= resSarokhTask;


//        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,List_file);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1,AppController.getInstance().resSarokhTask.getData().getScannedShipments());
        /*for(String orderId : AppController.getInstance().resSarokhTask.getData().getScannedShipments()){
           // msg+=orderId+"\n";
            List_file.add(orderId);
        }*/
        list.setAdapter(arrayAdapter);

       /* List_file.add("Coderzheaven");
        List_file.add("Google");
        List_file.add("Android");
        List_file.add("iPhone");
        List_file.add("Apple");
        //Create an adapter for the listView and add the ArrayList to the adapter.
        list.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,List_file));
        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,long arg3)
            {
                //args2 is the listViews Selected index
            }
        });*/


        //binding.textviewShipmentsScanned.setText(msg);
        AppController.getInstance().resSarokhTask.getData().setReceivedShipmentsCount(Integer.parseInt(binding.txtConfirmShipment.getText().toString()));

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        List_file =new ArrayList<String>();
        list =  view.findViewById(R.id.listview);




        MainActivity.var = 0;
        mCodeScanner = new CodeScanner(getActivity(), binding.scannerView);
        mCodeScanner.setScanMode(ScanMode.CONTINUOUS);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(binding.edtTrackingNumber.getText().toString().equals("") || !binding.edtTrackingNumber.getText().toString().equals(result.getText())){
                            binding.edtTrackingNumber.setText(result.getText());
                            AppConstants.showDialogWithoutTitle(getActivity(), "Tracking Number Scanned");
                        }
/*                        binding.edtTrackingNumber.setText(result.getText());
                        AppConstants.showDialogWithoutTitle(getActivity(), "Tracking Number Scanned");*/
                    }
                });
            }
        });
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.inputTrackingNo.setErrorEnabled(true);
                    binding.inputTrackingNo.setError("Enter Or Scan QR Code");
                    return;
                }
                binding.inputTrackingNo.setErrorEnabled(false);

                if(isTrackingNoExists(getArguments().getStringArray("receiveShipmentList")))
                {
                    doSearch(binding.edtTrackingNumber.getText().toString());
                }
                else{
                    // do nothing
                }
            }
        });





    }

    private boolean isTrackingNoExists(String[] receiveShipmentList) {

        for(String s : receiveShipmentList){
            if(s.equals(binding.edtTrackingNumber.getText().toString())){
                return true;
            }
        }

        return false;
    }

    private void doSearch(String trackingNo) {
        ResReceiveShipmentDetail resReceiveShipmentDetail = AppController.getInstance().receiveShipmentDetail;
        if (resReceiveShipmentDetail.getData().size() > 0) {
            int defaultStatus = 0;
            int counter = 0;
            ResReceiveShipmentDetail.Data defaultObj = null;
            for (ResReceiveShipmentDetail.Data obj : resReceiveShipmentDetail.getData()) {
                if (obj.getTrackingNo().equals(trackingNo)) {
                    if (obj.isConfirmed()) {
                        defaultStatus = 2;
                    } else {
                        obj.setConfirmed(true);
                        defaultStatus = 1;
                    }
                    defaultObj = obj;

                    break;
                }
                counter++;
            }
            if (defaultStatus == 0) {
                SuccessM successM = new SuccessM();
                successM.setDescription("Tracking Number Not Found");
                showErrorDialog(successM, this);
            } else if (defaultStatus == 2) {
                SuccessM successM = new SuccessM();
                successM.setDescription("Tracking Number Already shipped");
                showErrorDialog(successM, this);
            } else {
                resReceiveShipmentDetail.getData().set(counter, defaultObj);
                AppController.getInstance().resSarokhTask.getData().getScannedShipments().add(binding.edtTrackingNumber.getText().toString());

            }
            AppController.getInstance().receiveShipmentDetail = resReceiveShipmentDetail;

            if(! AppController.getInstance().resSarokhTask.getData().getScannedShipments().contains(trackingNo)){
                AppController.getInstance().resSarokhTask.getData().getScannedShipments().add(trackingNo);
            }

//            if(!List_file.contains(trackingNo)){
//                List_file.add(trackingNo);
//            }

            updateRecord(resReceiveShipmentDetail.getData());





            binding.edtTrackingNumber.setText("");

            try {
                NetworkLoader networkLoader = new NetworkLoader(this);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("trackingNo",trackingNo);
                Log.d("RESSAROKHTASKDATA",AppController.getInstance().resSarokhTask.getData().toString());
                Log.d("GIVENTRIPID",AppController.getInstance().resSarokhTask.getData().getTripId());

                jsonObject.put("tripId",AppController.getInstance().resSarokhTask.getData().getTripId());

                networkLoader.sendRequest(Request.Method.POST,AppConstants.DRIVER_RECEIVE_SHIPMENT,2,jsonObject);
               // networkLoader.loadJsonRequest(AppConstants.DRIVER_RECEIVE_SHIPMENT + "" + trackingNo, 2);
            }
            catch (Exception e){
                e.printStackTrace();
            }

        }
    }


    private void askForPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHavePermission()) {
                showPermissionDialog();
            } else {
                mCodeScanner.startPreview();
            }

        }
    }

    private void showPermissionDialog() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        AppConstants.showDialogWithoutTitle(getActivity(), "You cannot scan without permission");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();

        loadData();
        if (AppController.getInstance().receiveShipmentDetail == null) {
        } else {
            updateRecord(AppController.getInstance().receiveShipmentDetail.getData());
        }
        askForPermission();

    }

    private void loadData() {

        //Create an adapter for the listView and add the ArrayList to the adapter.
        list.setAdapter(new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,List_file));
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);





        try {
            NetworkLoader networkLoader = new NetworkLoader(this);
            networkLoader.loadJsonRequest(AppConstants.END_GET_RECEIVE_SHIPMENT_DETAIL_V2 + "" + AppController.getInstance().resSarokhTaskDriverData.getId(), 1);
        }
        catch (Exception e){
            e.printStackTrace();
        }


        /*try {
        NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("dealerId", AppController.getInstance().resSarokhTask.getData().getDealerId());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            Log.d("RESPONSED",AppController.getInstance().resSarokhTask.getData().getDealerId());
            Log.d("RESPONSED", String.valueOf(AppController.getInstance().resLoginObj.getData().getId()));

            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_GET_RECEIVE_SHIPMENT_DETAIL, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public void onPause() {
        super.onPause();
        mCodeScanner.releaseResources();
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {

        if(type == 2){
            try {
                AppConstants.showDialogWithoutTitle(getContext(), (String) jsonObject.get("message"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{
            binding.progressBar.progressBar.setVisibility(View.GONE);
            AppController.getInstance().receiveShipmentDetail = new Gson().fromJson(jsonObject.toString(), ResReceiveShipmentDetail.class);
            updateRecord(AppController.getInstance().receiveShipmentDetail.getData());
        }


    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    @Override
    public void onSuccessDialogDismiss(int type) {

    }

    @Override
    public void onErrorDialogDismiss(int type) {

    }

    private void CreateListView()
    {

    }
}
