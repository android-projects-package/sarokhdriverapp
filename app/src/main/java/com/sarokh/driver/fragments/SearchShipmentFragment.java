package com.sarokh.driver.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.activities.SearchShipmentResultDisplay;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSearchShipmentBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSearchShipment;
import com.sarokh.driver.utils.AppConstants;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 29,02,2020
 */
public class SearchShipmentFragment extends Fragment implements CallbackResponse {
    private FragmentSearchShipmentBinding binding;
    private NetworkLoader networkLoader;
    private Context _context;

    public static SearchShipmentFragment newInstance() {

        Bundle args = new Bundle();

        SearchShipmentFragment fragment = new SearchShipmentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_shipment, container, false);
        _context = getContext();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        networkLoader = new NetworkLoader(this);
        binding.progressBar.progressBar.setVisibility(View.GONE);
        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtMobile.getText().toString().isEmpty()) {
                    binding.inputContact.setError("Mobile No required");
                    binding.inputContact.setErrorEnabled(true);
                    return;
                }
                binding.inputContact.setErrorEnabled(false);
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.inputTrackingNo.setError("Tracking No required");
                    binding.inputTrackingNo.setErrorEnabled(true);
                    return;
                }

                binding.inputTrackingNo.setErrorEnabled(false);
                doSearch();
            }
        });
    }

    private void doSearch() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        try {
            JSONObject params = new JSONObject();
            params.put("mobileNumber", binding.edtMobile.getText().toString().trim());
            params.put("trackingNumber", binding.edtTrackingNumber.getText().toString().trim());
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_SEARCH_BY_TRACKING_MOBILE, 1, params);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        Gson gson = new Gson();
        ResSearchShipment resSearchShipment = gson.fromJson(jsonObject.toString(), ResSearchShipment.class);
        AppController.getInstance().resShipmentData = resSearchShipment.getData();
        Intent intent = new Intent(_context, SearchShipmentResultDisplay.class);
        startActivity(intent);
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }
}
