package com.sarokh.driver.activities;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.databinding.ActivityShipmentReceivingBinding;
import com.sarokh.driver.response.ResReceivedShipment;

/**
 * Created by becody.com on 18,02,2020
 */
public class ShipmentReceivingActivity extends AppCompatActivity {
    private ActivityShipmentReceivingBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_shipment_receiving);
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        setUpUi();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void setUpUi() {
        ResReceivedShipment resReceivedShipment = AppController.getInstance().resReceivedShipment;
        if (resReceivedShipment.getData().getOrderType().toLowerCase().equals("cod")) {
            binding.chkCod.setChecked(true);
        } else if (resReceivedShipment.getData().getOrderType().toLowerCase().equals("prepaid")) {
            binding.chkPrepaid.setChecked(true);
        } else if (resReceivedShipment.getData().getOrderType().toLowerCase().equals("full prepaid")) {
            binding.chkFullPrepaid.setChecked(true);
        }
        binding.txtShipmentNumber.setText(resReceivedShipment.getData().getShipmentNo());
        binding.txtReceiverPickupTime.setText(resReceivedShipment.getData().getPickupDatetime());
        binding.txtDeliveryTimeStamp.setText(resReceivedShipment.getData().getDriverArrivalTime());
        binding.txtShipmentBill.setText(resReceivedShipment.getData().getShipmentPrice() + "");
        binding.txtDeliveryCharges.setText(resReceivedShipment.getData().getDeliveryCharges() + "");
    }
}
