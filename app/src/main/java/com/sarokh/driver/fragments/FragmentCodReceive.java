package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentCodReceiveBinding;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResCodReceive;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

/**
 * Created by becody.com on 15,04,2020
 */
public class FragmentCodReceive extends BaseFragment implements CallbackResponse, CallbackForDialog {
    private FragmentCodReceiveBinding binding;
    private ResCodReceive resCodReceive;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cod_receive, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        MainActivity.var  = 0;
        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtAmount.getText().toString().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Enter Receive Amount");
                    return;
                }

                submitData();

            }
        });
    }

    private void submitData() {


        final PrettyDialog prettyDialog = new PrettyDialog(getContext());
        prettyDialog.setMessage("The amount will be transfered to driver collection wallet regardless of sarokh task.")
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .setAnimationEnabled(false)
                .addButton(
                        "OK",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                                binding.progressBar.progressBar.setVisibility(View.VISIBLE);
                                try {
                                    JSONObject jsonObject = new JSONObject();
                                    jsonObject.put("amount", binding.edtAmount.getText().toString().trim());
                                    jsonObject.put("taskId", AppController.getInstance().resSarokhTaskDriverData.getId());
                                    jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
                                    NetworkLoader networkLoader = new NetworkLoader(FragmentCodReceive.this);
                                    networkLoader.sendRequest(Request.Method.POST, AppConstants.END_GET_COD_SUBMIT, 2, jsonObject);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        }
                ).show();




    }

    /*private void loadData() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        try {
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("dealerId", AppController.getInstance().resSarokhTaskDriverData.getId());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_GET_COD_RECEIVE, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            if (type == 1) {
                resCodReceive = new Gson().fromJson(jsonObject.toString(), ResCodReceive.class);
            } else if (type == 2) {
                SuccessM successM = new SuccessM();
                successM.setDescription(jsonObject.getString("message"));
                showSuccessDialog(successM, this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        //loadData();
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    @Override
    public void onSuccessDialogDismiss(int type) {
        Navigation.findNavController(binding.getRoot()).popBackStack();
    }

    @Override
    public void onErrorDialogDismiss(int type) {

    }
}
