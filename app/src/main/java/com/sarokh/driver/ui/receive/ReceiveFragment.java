package com.sarokh.driver.ui.receive;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.activities.ActivityScanner;
import com.sarokh.driver.activities.ShipmentReceivingActivity;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentReceiveBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResReceivedShipment;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.AppProgressBar;
import com.sarokh.driver.utils.CustomToast;
import com.sarokh.driver.utils.LocalFile;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

public class ReceiveFragment extends Fragment implements CallbackResponse {
    private FragmentReceiveBinding binding;
    private AppProgressBar appProgressBar;
    private NetworkLoader networkLoader;
    private LocalFile localFile;
    private Context _context;
    private String[] arrReceivedShipper = {"Driver", "Shipper", "Dealer"};

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_receive, container, false);
        _context = getContext();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        appProgressBar = new AppProgressBar(_context);
        networkLoader = new NetworkLoader(this);
        localFile = new LocalFile(_context);
        String[] searchArr = getResources().getStringArray(R.array.receive_array);
        binding.spinner.setItems(searchArr);
        binding.spinner.setBackgroundResource(R.drawable.spinner_selector);
        binding.btnQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), ActivityScanner.class);
                startActivity(intent);
            }
        });
        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.inputTrackingNo.setError("Tracking number filed should not be empty");
                    binding.inputTrackingNo.setErrorEnabled(true);
                    return;
                }
                binding.inputTrackingNo.setErrorEnabled(false);
                int index = binding.spinner.getSelectedIndex();
                doSearch(binding.edtTrackingNumber.getText().toString().trim(), arrReceivedShipper[index], index == 2);

            }
        });
    }

    private void doSearch(String trackingNumber, String from, boolean returnFrom) {
        try {
            appProgressBar.showProgressDialog(null);
            JSONObject jsonObject = new JSONObject();
            if (returnFrom) {
                jsonObject.put("returnFrom", from);
                jsonObject.put("receivedFrom", "");
            } else {
                jsonObject.put("returnFrom", "");
                jsonObject.put("receivedFrom", from);
            }
            jsonObject.put("trackingNumber", trackingNumber);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_RECEIVE_SHIPMENT, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        appProgressBar.dismissProgressDialog();
        Gson gson = new Gson();
        AppController.getInstance().resReceivedShipment = gson.fromJson(jsonObject.toString(), ResReceivedShipment.class);
        Intent intent = new Intent(getActivity(), ShipmentReceivingActivity.class);
        startActivity(intent);

    }

    @Override
    public void onResponse(String response, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onAppError(String msg, int type) {
        appProgressBar.dismissProgressDialog();
        CustomToast.showToast(_context, msg);
    }
}