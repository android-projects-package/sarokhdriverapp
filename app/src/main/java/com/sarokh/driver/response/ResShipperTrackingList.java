package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 22,04,2020
 */
public class ResShipperTrackingList {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private int shipperId;
        private String shipperName;
        private List<String> shipmentList;

        public int getShipperId() {
            return shipperId;
        }

        public void setShipperId(int shipperId) {
            this.shipperId = shipperId;
        }

        public String getShipperName() {
            return shipperName;
        }

        public void setShipperName(String shipperName) {
            this.shipperName = shipperName;
        }

        public List<String> getShipmentList() {
            return shipmentList;
        }

        public void setShipmentList(List<String> shipmentList) {
            this.shipmentList = shipmentList;
        }
    }
}