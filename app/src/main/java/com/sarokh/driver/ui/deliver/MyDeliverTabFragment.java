package com.sarokh.driver.ui.deliver;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.sarokh.driver.R;
import com.sarokh.driver.databinding.FragmentMyDeliverTabBinding;

/**
 * Created by becody.com on 24,02,2020
 */
public class MyDeliverTabFragment extends Fragment {
    private FragmentMyDeliverTabBinding binding;
    private Context _context;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_deliver_tab, container, false);
        _context = getContext();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initFragments();
    }

    private void initFragments() {
        binding.tabLayout.setupWithViewPager(binding.viewPager);
        AdapterTabs adapter = new AdapterTabs(getChildFragmentManager());
        adapter.addFrag(ChildFragment.newInstance(true), "Active Orders");
        adapter.addFrag(ChildFragment.newInstance(false), "Completed Orders");
        binding.viewPager.setAdapter(adapter);
    }
}
