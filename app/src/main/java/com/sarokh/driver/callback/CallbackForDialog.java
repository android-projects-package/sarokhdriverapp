package com.sarokh.driver.callback;

/**
 * Created by app.com on 01,08,2020
 */
public interface CallbackForDialog {
    void onSuccessDialogDismiss(int type);

    void onErrorDialogDismiss(int type);
}
