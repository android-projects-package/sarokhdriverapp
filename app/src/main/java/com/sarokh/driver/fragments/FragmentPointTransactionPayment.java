package com.sarokh.driver.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentPointTransactionPaymentBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.net.ssl.SSLEngineResult;

public class FragmentPointTransactionPayment extends Fragment implements CallbackResponse {

    private FragmentPointTransactionPaymentBinding fragmentPointTransactionPaymentBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        fragmentPointTransactionPaymentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_point_transaction_payment, container, false);
        // Inflate the layout for this fragment
        fragmentPointTransactionPaymentBinding.btnReceiveAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(fragmentPointTransactionPaymentBinding.editTextAmount.getText().toString().equals("")){
                    AppConstants.showDialogWithoutTitle(getContext(),"Please Enter Amount! ");
                    return;
                }

                if(Double.parseDouble(fragmentPointTransactionPaymentBinding.editTextAmount.getText().toString()) != Double.parseDouble(fragmentPointTransactionPaymentBinding.textviewAmountToReceive.getText().toString())){

                    AppConstants.showDialogWithoutTitle(getContext(),"Please Enter Valid Amount! ");

                    return;
                }

                try {

                    fragmentPointTransactionPaymentBinding.progressBar.progressBar.setVisibility(View.VISIBLE);

                    NetworkLoader networkLoader = new NetworkLoader(FragmentPointTransactionPayment.this);

                    JSONObject jsonObject = new JSONObject();

                    jsonObject.put("amount",fragmentPointTransactionPaymentBinding.editTextAmount.getText().toString());

                    jsonObject.put("driverId",AppController.getInstance().resSarokhTask.getData().getDriverId());

                    jsonObject.put("taskId",AppController.getInstance().resSarokhTask.getData().getTaskId());

                   // AppConstants.showDialogWithoutTitle(getContext(),jsonObject.toString());

                    networkLoader.sendRequest(Request.Method.POST,AppConstants.DRIVER_RECEIVE_AMOUNT,2,jsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // TODO SEND AMOUNT, TASKID AND DRIVERID TO mobile-driver-v2/receive-amount endpoint and navigate to the sarokh task screen.
        return fragmentPointTransactionPaymentBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MainActivity.var = 0;


    }

    @Override
    public void onResume() {
        super.onResume();
        initUi();
        loadData();
    }

    private void initUi() {

       // Toast.makeText(getContext(), "Fragment Point Transaction Payment Screen...!", Toast.LENGTH_SHORT).show();

        fragmentPointTransactionPaymentBinding.textviewPointName.setText(AppController.getInstance().resSarokhTask.getData().getPointName());

        fragmentPointTransactionPaymentBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getTaskId());

    }


    private void loadData() {
        try{
            NetworkLoader networkLoader = new NetworkLoader(FragmentPointTransactionPayment.this);
            networkLoader.loadJsonRequest(AppConstants.GET_DEALER_WALLET+AppController.getInstance().resSarokhTask.getData().getDealerId(), 1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    private void updateUi() {

        /*if(AppController.getInstance().resSarokhTask.getData() != null){

            fragmentPointTransactionPaymentBinding.textviewPointName.setText(AppController.getInstance().resSarokhTask.getData().getPointName());

            fragmentPointTransactionPaymentBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getTaskId());

            fragmentPointTransactionPaymentBinding.textviewAmountToReceive.setText(String.valueOf(AppController.getInstance().resSarokhTask.getData().getDealerAmount()));
        }*/


    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentPointTransactionPaymentBinding.progressBar.progressBar.setVisibility(View.GONE);

        Log.d("JSONO",jsonObject.toString());

        if(type == 1){
            try {
                JSONObject jsonObject1 = jsonObject.getJSONObject("data");

                String amount = jsonObject1.getString("amount");

                fragmentPointTransactionPaymentBinding.textviewAmountToReceive.setText(amount);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else{


            try {
                if(Integer.parseInt(String.valueOf(jsonObject.get("status"))) == 200){
                    AppConstants.showDialogWithoutTitle(getContext(), String.valueOf(jsonObject.get("message")));
                    Navigation.findNavController(fragmentPointTransactionPaymentBinding.getRoot()).navigate(R.id.actionMySarokh);
                }
            }
            catch (JSONException e) {
                e.printStackTrace();
            }


        }
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentPointTransactionPaymentBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentPointTransactionPaymentBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentPointTransactionPaymentBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }
}