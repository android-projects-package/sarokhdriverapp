package com.sarokh.driver;

import android.app.Application;
import android.location.Location;
import android.os.Build;
import android.os.StrictMode;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.LatLng;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.response.ResDashboard;
import com.sarokh.driver.response.ResLogin;
import com.sarokh.driver.response.ResReceiveShipmentDetail;
import com.sarokh.driver.response.ResReceivedShipment;
import com.sarokh.driver.response.ResSarokhTask;
import com.sarokh.driver.response.ResSarokhTaskDriver;
import com.sarokh.driver.response.ResSearchShipment;
import com.sarokh.driver.response.ResSearchTrackingNo;
import com.sarokh.driver.response.ResShipperReceiving;
import com.sarokh.driver.response.ResShipperTrackingList;
import com.sarokh.driver.response.ResWareHouse;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by becody.com on 22,02,2020
 */
public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();

    private RequestQueue mRequestQueue;
    private static AppController mInstance;
    public ResDashboard resDashboard;
    public ResReceivedShipment resReceivedShipment;
    public ResSearchShipment.Data resShipmentData;
    public JSONObject cashHandOverObj;
    public ResLogin resLoginObj;
    public ResSarokhTask resSarokhTask;
    public ResSearchTrackingNo resSearchTrackingNo;
    public ResReceiveShipmentDetail receiveShipmentDetail;
    public ResShipperReceiving resShipperReceiving;
    public ResShipperTrackingList resShipperTrackingList;
    public ShipmentTrackingListM resShipperTrackingModal;
    public ResSarokhTaskDriver resSarokhTaskDriver;
    public ResSarokhTaskDriver.Data resSarokhTaskDriverData;
    public ResWareHouse resWareHouse;
    public SuccessM successM;
    public static int dealerId;
    public static double lat,lng, totalDistance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // Kitkat and lower has a bug that can cause in correct strict mode
            // warnings about expected activity counts
            // enableStrictMode();
        }
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }


    public void enableStrictMode() {
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build());
    }
}
