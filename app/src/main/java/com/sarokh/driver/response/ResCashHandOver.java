package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 01,03,2020
 */
public class ResCashHandOver {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        private float cashInWallet;
        private List<CollectionAgentsList> collectionAgentsList;
        private List<DriversList> driversList;

        public float getCashInWallet() {
            return cashInWallet;
        }

        public void setCashInWallet(float cashInWallet) {
            this.cashInWallet = cashInWallet;
        }

        public List<CollectionAgentsList> getCollectionAgentsList() {
            return collectionAgentsList;
        }

        public void setCollectionAgentsList(List<CollectionAgentsList> collectionAgentsList) {
            this.collectionAgentsList = collectionAgentsList;
        }

        public List<DriversList> getDriversList() {
            return driversList;
        }

        public void setDriversList(List<DriversList> driversList) {
            this.driversList = driversList;
        }
    }
    public static class CollectionAgentsList{
        private int id;
        private String name;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
    public static class DriversList{
        private int id;
        private String firstName;
        private String lastName;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }
    }
}
