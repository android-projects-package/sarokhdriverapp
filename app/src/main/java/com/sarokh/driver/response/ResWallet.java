package com.sarokh.driver.response;

/**
 * Created by becody.com on 15,04,2020
 */
public class ResWallet {
    public Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private float codAmount;

        public float getCodAmount() {
            return codAmount;
        }

        public void setCodAmount(float codAmount) {
            this.codAmount = codAmount;
        }
    }
}