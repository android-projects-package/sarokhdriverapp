package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.ShipmentTrackingListM;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentShipperReceivingBinding;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResShipperTrackingList;
import com.sarokh.driver.response.ResVerifyTrackingNumber;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by app.com on 12,04,2020
 */
public class FragmentShipperReceiving extends BaseFragment implements CallbackResponse, CallbackForDialog {
    private FragmentShipperReceivingBinding binding;
    private CodeScanner mCodeScanner;
    private ShipmentTrackingListM shipmentTrackingListM;
    private ResVerifyTrackingNumber resSearchTrackingNo;
    private String[] receivedShipments;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shipper_receiving, container, false);
        binding.btnProceedToNxt.setEnabled(false);
        return binding.getRoot();
    }

    private void updateRecord() {

        if (shipmentTrackingListM == null) {
            binding.txtShipmentReceived.setText("0");
        }
        if (resSearchTrackingNo.getData().getPickUpLocation().equals("Sarokh Point") &&
                !resSearchTrackingNo.getData().getShipFromCity().equals(resSearchTrackingNo
                        .getData().getDealerCity())) {
            AppConstants.showDialogWithoutTitle(getContext(), "Invalid Shipment");
        } else if (resSearchTrackingNo.getData().getPickUpLocation().equals("Sarokh Point") &&
                resSearchTrackingNo.getData().getAssignToDetails() != null) {

            if (resSearchTrackingNo.getData().getAssignToDetails().equals(
                    resSearchTrackingNo.getData().getDealerName()
            )) {
                AppConstants.showDialogWithoutTitle(getContext(), "Shipment already Receive at Point");
            } else {
                AppConstants.showDialogWithoutTitle(getContext(), "Shipment received at another point.");
            }
        }
        else {

            if (shipmentTrackingListM.getTrackingList().size() > 0) {
                binding.btnProceedToNxt.setEnabled(true);
            }
            binding.txtShipmentReceived.setText(shipmentTrackingListM.getTrackingList().size() + "");
        }


    }
   /* private void addTrackingIntoList(String tracking) {
        List<String> trackingList = shipmentTrackingListM.getTrackingList();
        trackingList.add(tracking);
        shipmentTrackingListM.setTrackingList(trackingList);
    }*/

    private boolean isTrackingAddedIntoList(String tracking) {
        if (shipmentTrackingListM == null)
            return false;

        if (shipmentTrackingListM.getTrackingList().size() == 0)
            return false;

        for (String trackingListM : shipmentTrackingListM.getTrackingList()) {
            if (trackingListM.equals(tracking)) {
                return true;
            }
        }
        return false;
    }

   /* private void setUpSpinnerData(List<ResShipperReceiving.Data> dataList) {
        String[] shipperName = new String[dataList.size()];
        int counter = 0;
        for (ResShipperReceiving.Data data : dataList) {
            shipperName[counter] = data.getShipperName();
            counter++;
        }
        binding.spinner.setItems(shipperName);
        if (dataList.size() > 0) {
            getShipmentList(0);
        }
    }*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        receivedShipments = getArguments().getStringArray("receiveShipmentList");

        shipmentTrackingListM = new ShipmentTrackingListM();
        mCodeScanner = new CodeScanner(getActivity(), binding.scannerView);
        mCodeScanner.setScanMode(ScanMode.CONTINUOUS);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(binding.edtTrackingNumber.getText().toString().equals("") || !binding.edtTrackingNumber.getText().toString().equals(result.getText())){
                            binding.edtTrackingNumber.setText(result.getText());
                            AppConstants.showDialogWithoutTitle(getActivity(), "Tracking Number Scanned");
                        }
                    }
                });
            }
        });

        binding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.edtTrackingNumber.setError("Enter Or Scan QR Code");
                }
                binding.edtTrackingNumber.setError(null);

                if(isValidTrackingNo(binding.edtTrackingNumber.getText().toString())){
                    if(isTrackingAddedIntoList(binding.edtTrackingNumber.getText().toString())){

                        AppConstants.showDialogWithoutTitle(getActivity(),"This shipment is already scanned! ");
                    }
                    else{
                        if (shipmentTrackingListM == null)
                            shipmentTrackingListM = new ShipmentTrackingListM();
                        shipmentTrackingListM.setShipperId(Integer.parseInt(AppController.getInstance().resWareHouse.getData().getShipperId()));
                        shipmentTrackingListM.setShipperName(AppController.getInstance().resWareHouse.getData().getShipperName());
                        List<String> arrayList = shipmentTrackingListM.getTrackingList();
                        arrayList.add(binding.edtTrackingNumber.getText().toString().trim());
                        shipmentTrackingListM.setTrackingList(arrayList);
                        binding.edtTrackingNumber.setText("");
                        binding.txtShipmentReceived.setText(shipmentTrackingListM.getTrackingList().size()+"");
                        if(shipmentTrackingListM.getTrackingList().size() > 0){
                            binding.btnProceedToNxt.setEnabled(true);
                        }
                        //updateRecord();
                    }
                }
                else{
                    AppConstants.showDialogWithoutTitle(getActivity(),"Invalid Shipment! ");
                }
              //  doSearch(binding.edtTrackingNumber.getText().toString());
            }
        });

       /* binding.spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                shipmentTrackingListM.setTrackingList(new ArrayList<String>());
                binding.txtShipmentReceived.setText("0");
                getShipmentList(position);
            }
        });*/
        binding.btnProceedToNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shipmentTrackingListM == null) {
                    AppConstants.showDialogWithoutTitle(getContext(), "Please receive shipment first");
                    return;
                }
                if (shipmentTrackingListM.getTrackingList().size() == 0) {
                    AppConstants.showDialogWithoutTitle(getContext(), "Please receive shipment first");
                    return;
                }
                AppController.getInstance().resShipperTrackingModal = shipmentTrackingListM;
                Navigation.findNavController(binding.getRoot()).navigate(R.id.actionShipperReceivingConfirm);
            }
        });
    }

    private boolean isValidTrackingNo(String trackingNo) {
        for(String no : receivedShipments){
            if(no.equals(trackingNo)){
                return true;
            }
        }
        return false;
    }

  /*  private void getShipmentList(int position) {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        ResShipperReceiving.Data resShipperReceiving = AppController.getInstance().resShipperReceiving.getData().get(position);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_SHIPPER_RECEIVING + resShipperReceiving.getShipperId(), 2);
    }*/

    private void doSearch(String trackingNo) {
        if (isTrackingAddedIntoList(trackingNo)) {
            AppConstants.showDialogWithoutTitle(getActivity(), "Tracking Number Already Scanned");
        } else {
            scanTrackingNumber(trackingNo);
        }
    }

    private void askForPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHavePermission()) {
                showPermissionDialog();
            } else {
                mCodeScanner.startPreview();
            }

        }
    }

    private void showPermissionDialog() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        AppConstants.showDialogWithoutTitle(getActivity(), "You cannot scan without permission");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();
        askForPermission();
    }

    private void scanTrackingNumber(String number) {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_VERIFY_TRACKING_NUMBER + number, 1);
    }

    @Override
    public void onPause() {
        super.onPause();
        mCodeScanner.releaseResources();
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        if (type == 1) {
            resSearchTrackingNo = new Gson().fromJson(jsonObject.toString(), ResVerifyTrackingNumber.class);
            if (resSearchTrackingNo.getData() == null) {
                SuccessM successM = new SuccessM();
                successM.setDescription(resSearchTrackingNo.getMessage());
                showErrorDialog(successM, this);
            } else {
                if (shipmentTrackingListM == null)
                    shipmentTrackingListM = new ShipmentTrackingListM();
                shipmentTrackingListM.setShipperId(resSearchTrackingNo.getData().getShipperId());
                shipmentTrackingListM.setShipperName(resSearchTrackingNo.getData().getName());
                List<String> arrayList = shipmentTrackingListM.getTrackingList();
                arrayList.add(binding.edtTrackingNumber.getText().toString().trim());
                shipmentTrackingListM.setTrackingList(arrayList);
                binding.edtTrackingNumber.setText("");
                updateRecord();
            }
        } else if (type == 2) {
            AppController.getInstance().resShipperTrackingList = new Gson().fromJson(jsonObject.toString(), ResShipperTrackingList.class);
            shipmentTrackingListM.setShipperId(AppController.getInstance().resShipperTrackingList.getData().getShipperId());
            shipmentTrackingListM.setShipperName(AppController.getInstance().resShipperTrackingList.getData().getShipperName());
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), response);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        SuccessM successM = new SuccessM();
        successM.setDescription(msg);
        showErrorDialog(successM, this);
    }

    @Override
    public void onSuccessDialogDismiss(int type) {

    }

    @Override
    public void onErrorDialogDismiss(int type) {

    }
}
