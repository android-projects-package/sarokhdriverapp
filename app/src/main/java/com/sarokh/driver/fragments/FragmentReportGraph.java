package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackDateAndTime;
import com.sarokh.driver.databinding.FragmentReportGraphBinding;
import com.sarokh.driver.dialogs.DatePickerDialogUtils;

/**
 * Created by becody.com on 16,08,2020
 */
public class FragmentReportGraph extends Fragment implements CallbackDateAndTime {
    private FragmentReportGraphBinding binding;
    private boolean isForFromDate = false;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_graph, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.edtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isForFromDate = true;
                showDateDialog();
            }
        });
        binding.edtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isForFromDate = false;
                showDateDialog();
            }
        });
    }

    @Override
    public void onDataAndTimeSelected(boolean isDate, String val) {
        if(isForFromDate){
            binding.edtFromDate.setText(val);
        }else{
            binding.edtToDate.setText(val);
        }
    }

    private void showDateDialog() {
        DatePickerDialogUtils datePickerDialogUtils = new DatePickerDialogUtils();
        datePickerDialogUtils.setCallbackDateAndTime(this);
        FragmentTransaction ft = getChildFragmentManager().beginTransaction();
        datePickerDialogUtils.show(ft, "Date Dialog");
    }
}
