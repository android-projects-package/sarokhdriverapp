package com.sarokh.driver.ui.deliver;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackRecyclerViewClick;
import com.sarokh.driver.response.ResSearchShipment;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterChildDeliverActive extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResSearchShipment.Data> dataList;
    private CallbackRecyclerViewClick<ResSearchShipment.Data> recyclerViewClick;

    public AdapterChildDeliverActive(List<ResSearchShipment.Data> dataList, CallbackRecyclerViewClick<ResSearchShipment.Data> callbackRecyclerViewClick) {
        this.dataList = dataList;
        this.recyclerViewClick = callbackRecyclerViewClick;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtReceiverName;
        private TextView txtShipmentNumber;
        private RelativeLayout rltIconPlus;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtReceiverName = itemView.findViewById(R.id.txtReceiverName);
            txtShipmentNumber = itemView.findViewById(R.id.txtShipmentNumber);
            rltIconPlus = itemView.findViewById(R.id.rltIconPlus);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_child_deliver, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
               final ResSearchShipment.Data data = dataList.get(position);
                cellViewHolder.rltIconPlus.setVisibility(View.VISIBLE);
                cellViewHolder.rltIconPlus.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        recyclerViewClick.onClickRow(data);
                    }
                });
                cellViewHolder.txtReceiverName.setText(data.getReceiverName());
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
