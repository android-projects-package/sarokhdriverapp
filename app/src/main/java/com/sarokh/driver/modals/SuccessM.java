package com.sarokh.driver.modals;

/**
 * Created by app.com on 01,08,2020
 */
public class SuccessM {
    private String title;
    private String description;
    private String orderId;
    private int type = 1; // 1 for close, 2 for send task confirmation screenshot.

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
