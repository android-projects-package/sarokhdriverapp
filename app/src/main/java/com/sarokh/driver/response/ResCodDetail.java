package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 11,04,2020
 */
public class ResCodDetail {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private String trackingNo;
        private float billedAmount;
        private String deliveryDate;

        public String getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(String trackingNo) {
            this.trackingNo = trackingNo;
        }

        public float getBilledAmount() {
            return billedAmount;
        }

        public void setBilledAmount(float billedAmount) {
            this.billedAmount = billedAmount;
        }

        public String getDeliveryDate() {
            return deliveryDate;
        }

        public void setDeliveryDate(String deliveryDate) {
            this.deliveryDate = deliveryDate;
        }
    }
}
