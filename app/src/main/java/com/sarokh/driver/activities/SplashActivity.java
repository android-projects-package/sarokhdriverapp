package com.sarokh.driver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.utils.LocalFile;

/**
 * Created by becody.com on 22,02,2020
 */
public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                openNextActivity();
            }
        }, 3000);

    }

    private void openNextActivity() {
        LocalFile localFile = new LocalFile(this);
        if (localFile.isUserLoggedIn()) {
            AppController.getInstance().resLoginObj = localFile.getLoginObj();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }
}
