package com.sarokh.driver.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.sarokh.driver.R;
import com.sarokh.driver.activities.LoginActivity;
import com.sarokh.driver.databinding.FragmentLogoutBinding;
import com.sarokh.driver.utils.LocalFile;

/**
 * Created by becody.com on 25,04,2020
 */
public class FragmentLogout extends Fragment {
    private FragmentLogoutBinding binding;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        /*binding = DataBindingUtil.inflate(inflater, R.layout.fragment_logout,container,false);

        LocalFile localFile = new LocalFile(getActivity());
        localFile.setLogout();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();*/
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
