package com.sarokh.driver.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.widget.DatePicker;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.sarokh.driver.callback.CallbackDateAndTime;

import java.util.Calendar;

/**
 * Created by becody.com on 01,03,2020
 */
public class DatePickerDialogUtils extends DialogFragment implements android.app.DatePickerDialog.OnDateSetListener {

    private CallbackDateAndTime callbackDateAndTime;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, yy, mm, dd);
    }

    public void setCallbackDateAndTime(CallbackDateAndTime dateAndTime) {
        this.callbackDateAndTime = dateAndTime;
    }

    @Override
    public void onDateSet(DatePicker datePicker, int y, int m, int d) {
        callbackDateAndTime.onDataAndTimeSelected(true, y + "-" + m + 1 + "-" + d);
    }
}
