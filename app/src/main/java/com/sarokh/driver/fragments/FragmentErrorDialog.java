package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.DialogFragment;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.databinding.FragmentErrorDialogBinding;

/**
 * Created by app.com on 01,08,2020
 */
public class FragmentErrorDialog extends DialogFragment {
    private FragmentErrorDialogBinding binding;
    private CallbackForDialog callbackForDialog;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_error_dialog, container, false);
        return binding.getRoot();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppController.getInstance().successM == null) {
            this.dismiss();
        } else {
            binding.txtTitle.setText(AppController.getInstance().successM.getTitle() == null ? "An Error Has Occurred" : AppController.getInstance().successM.getTitle());
            binding.txtDescription.setText(AppController.getInstance().successM.getDescription());
            binding.btnTryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    decideAboutAction();
                }
            });
        }
    }

    public void setCallbackForDialog(CallbackForDialog callbackForDialog) {
        this.callbackForDialog = callbackForDialog;
    }

    private void decideAboutAction() {
        callbackForDialog.onSuccessDialogDismiss(1);
        this.dismiss();
    }

}
