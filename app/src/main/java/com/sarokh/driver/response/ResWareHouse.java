package com.sarokh.driver.response;

import java.util.Arrays;

/**
 * Created by becody.com on 15,06,2020
 */
public class ResWareHouse {
    private int status;
    private String message;
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class Data {
        private int receiveShipments;
        private int returnShipments;
        private String[] receiveShipmentList;
        private String shipperBusinessName;
        private String shipperId;
        private String shipperName;

        public String[] getReceiveShipmentList() {
            return receiveShipmentList;
        }

        public void setReceiveShipmentList(String[] receiveShipmentList) {
            this.receiveShipmentList = receiveShipmentList;
        }

        public String getShipperBusinessName() {
            return shipperBusinessName;
        }

        public void setShipperBusinessName(String shipperBusinessName) {
            this.shipperBusinessName = shipperBusinessName;
        }

        public String getShipperId() {
            return shipperId;
        }

        public void setShipperId(String shipperId) {
            this.shipperId = shipperId;
        }

        public String getShipperName() {
            return shipperName;
        }

        public void setShipperName(String shipperName) {
            this.shipperName = shipperName;
        }

        public int getReturnShipments() {
            return returnShipments;
        }

        public void setReturnShipments(int returnShipments) {
            this.returnShipments = returnShipments;
        }

        private String driverName;
        private int driverId;

        public int getReceiveShipments() {
            return receiveShipments;
        }

        public void setReceiveShipments(int receiveShipments) {
            this.receiveShipments = receiveShipments;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public int getDriverId() {
            return driverId;
        }

        public void setDriverId(int driverId) {
            this.driverId = driverId;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "receiveShipments=" + receiveShipments +
                    ", returnShipments=" + returnShipments +
                    ", receiveShipmentList=" + Arrays.toString(receiveShipmentList) +
                    ", shipperBusinessName='" + shipperBusinessName + '\'' +
                    ", shipperId='" + shipperId + '\'' +
                    ", shipperName='" + shipperName + '\'' +
                    ", driverName='" + driverName + '\'' +
                    ", driverId=" + driverId +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ResWareHouse{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
