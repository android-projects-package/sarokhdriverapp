package com.sarokh.driver.callback;

/**
 * Created by becody.com on 24,02,2020
 */
public interface CallbackRecyclerViewClick<T> {
    void onClickRow(T modal);
}
