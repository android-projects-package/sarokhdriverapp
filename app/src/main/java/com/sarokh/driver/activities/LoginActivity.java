package com.sarokh.driver.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.ActivityLoginBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.AppProgressBar;
import com.sarokh.driver.utils.CustomToast;
import com.sarokh.driver.utils.LocalFile;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 16,02,2020
 */
public class LoginActivity extends AppCompatActivity implements CallbackResponse {
    private ActivityLoginBinding binding;
    private NetworkLoader networkLoader;
    private AppProgressBar appProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        networkLoader = new NetworkLoader(this);
        appProgressBar = new AppProgressBar(this);
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtEmail.getText().toString().isEmpty()) {
                    binding.inputEmail.setError("Username required");
                    binding.inputEmail.setErrorEnabled(true);
                    return;
                }
                if (binding.edtEmail.getText().toString().trim().length() < 5) {
                    binding.inputEmail.setError("Minimum 5 character required");
                    binding.inputEmail.setErrorEnabled(true);
                    return;
                }

                binding.inputEmail.setErrorEnabled(false);

                if (binding.edtPwd.getText().toString().isEmpty()) {
                    binding.inputPwd.setError("Password required");
                    binding.inputPwd.setErrorEnabled(true);
                    return;
                }
                /*if (binding.edtPwd.getText().toString().trim().length() < 5) {
                    binding.inputPwd.setError("Minimum 5 character required");
                    binding.inputPwd.setErrorEnabled(true);
                    return;
                }*/

                binding.inputPwd.setErrorEnabled(false);
                doLogin(binding.edtEmail.getText().toString().trim(), binding.edtPwd.getText().toString().trim());
            }
        });
        binding.txtForgetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ActivityForgetPassword.class);
                startActivity(intent);
            }
        });
    }

    private void openHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void doLogin(String username, String password) {
        try {
            appProgressBar.showProgressDialog(null);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("username", username);
            jsonObject.put("password", password);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_POINT_LOGIN, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        appProgressBar.dismissProgressDialog();
        AppConstants.showErrorDialog(this,response);

    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        appProgressBar.dismissProgressDialog();
        LocalFile localFile = new LocalFile(this);
        localFile.doLogin(jsonObject.toString());
        AppController.getInstance().resLoginObj = localFile.getLoginObj();
        if(AppController.getInstance().resLoginObj.getData()==null){
            AppConstants.showErrorDialog(this,"Unable to login.");
        }else{
            openHomeActivity();
        }
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        appProgressBar.dismissProgressDialog();
    }

    @Override
    public void onAppError(String msg, int type) {
        appProgressBar.dismissProgressDialog();
        AppConstants.showErrorDialog(this,msg);
    }

}
