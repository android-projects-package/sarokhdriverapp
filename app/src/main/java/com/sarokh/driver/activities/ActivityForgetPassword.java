package com.sarokh.driver.activities;

import android.net.Network;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.android.volley.Request;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.ActivityForgotPasswordBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 16,08,2020
 */
public class ActivityForgetPassword extends AppCompatActivity implements CallbackResponse {

    private ActivityForgotPasswordBinding binding;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        binding.btnGetPwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtNumber.getText().toString().trim().isEmpty()) {
                    AppConstants.showDialogWithoutTitle(ActivityForgetPassword.this, "Enter Contact Number");
                    return;
                }

                sendAPIsCall();
            }
        });
    }

    private void sendAPIsCall() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("contactNumber", binding.edtNumber.getText().toString().trim());
            NetworkLoader networkLoader = new NetworkLoader(this);
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_FORGET_PASSWORD, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            AppConstants.showDialogWithoutTitle(this, jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(this, msg);
    }
}
