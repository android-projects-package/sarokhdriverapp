package com.sarokh.driver.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSarokhTaskConfirmBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTaskConfirmation;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentSarokhTaskConfirmBackup extends Fragment implements CallbackResponse, SignaturePad.OnSignedListener {
    private FragmentSarokhTaskConfirmBinding fragmentSarokhTaskConfirmBinding;
    private ResSarokhTaskConfirmation resSarokhTaskConfirmation;
    private Bitmap bitmapSignature = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSarokhTaskConfirmBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sarokh_task_confirm, container, false);
        return fragmentSarokhTaskConfirmBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentSarokhTaskConfirmBinding.signaturePad.setOnSignedListener(this);
        loadBasicUi();
        fragmentSarokhTaskConfirmBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentSarokhTaskConfirmBinding.signaturePad.clear();
            }
        });
        fragmentSarokhTaskConfirmBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmapSignature == null) {
                    CustomToast.showToast(getActivity(), "Signature is required!");
                    return;
                }
                submitConfirmationTask();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRequest();
    }

    private void loadRequest() {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_SAROKH_TASK_CONFIRMATION + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        if (type == 1) {
            resSarokhTaskConfirmation = new Gson().fromJson(jsonObject.toString(), ResSarokhTaskConfirmation.class);
            setUpConfirmationDialog();
        } else if (type == 2) {
            try {
                CustomToast.showToast(getActivity(), jsonObject.getString("message"));
                Navigation.findNavController(fragmentSarokhTaskConfirmBinding.getRoot()).popBackStack();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadBasicUi() {
        fragmentSarokhTaskConfirmBinding.txtGiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");
       // fragmentSarokhTaskConfirmBinding.txtCodPaid.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD());
        fragmentSarokhTaskConfirmBinding.txtReceiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipments() + "");
        fragmentSarokhTaskConfirmBinding.txtDriverName.setText(AppController.getInstance().resSarokhTask.getData().getDriverName());
    }

    private void submitConfirmationTask() {
        try {
            fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("confirmationId", resSarokhTaskConfirmation.getData().getConfirmationId());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            jsonObject.put("driverName", AppController.getInstance().resLoginObj.getData().getUser().getFullName());
            jsonObject.put("giveShipments", true);
            jsonObject.put("payCOD", true);
            jsonObject.put("pendingCOD", true);
            jsonObject.put("receiveShipments", true);
            jsonObject.put("reportIssues", true);
            jsonObject.put("signature", toBase64(bitmapSignature));
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_POST_SUBMIT_SAROKH_TASK, 2, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void setUpConfirmationDialog() {
        if (resSarokhTaskConfirmation.getData().isGiveShipments()) {
            fragmentSarokhTaskConfirmBinding.ImgGiveShipmentVal.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.ImgGiveShipmentVal.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isReceiveShipments()) {
            fragmentSarokhTaskConfirmBinding.imgReceiveShipmentVal.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgReceiveShipmentVal.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isPayCOD()) {
            fragmentSarokhTaskConfirmBinding.imgCodPaid.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgCodPaid.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isPendingCOD()) {
            fragmentSarokhTaskConfirmBinding.imgPendingCod.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgPendingCod.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        bitmapSignature = fragmentSarokhTaskConfirmBinding.signaturePad.getSignatureBitmap();
    }

    @Override
    public void onClear() {
        bitmapSignature = null;
    }
}
