package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDeliverShipmentTaskBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSearchTrackingNo;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentDeliverShipmentTaskBackUp extends Fragment implements CallbackResponse {

    private FragmentDeliverShipmentTaskBinding fragmentDeliverShipmentTaskBinding;
    private ResSearchTrackingNo resSearchTrackingNo;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentDeliverShipmentTaskBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_deliver_shipment_task, container, false);
        return fragmentDeliverShipmentTaskBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
     /*   fragmentDeliverShipmentTaskBinding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (fragmentDeliverShipmentTaskBinding.edtTrackingNumber.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.inputTrackingNo.setErrorEnabled(true);
                    fragmentDeliverShipmentTaskBinding.inputTrackingNo.setError("Enter Tracking No");
                    return;
                }
                fragmentDeliverShipmentTaskBinding.inputTrackingNo.setErrorEnabled(false);
                searchTrackingNo(fragmentDeliverShipmentTaskBinding.edtTrackingNumber.getText().toString().trim());
            }
        });
        fragmentDeliverShipmentTaskBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (resSearchTrackingNo == null) {
                    CustomToast.showToast(getActivity(), "Enter Valid Tracking Info!");
                    return;
                }

                if (fragmentDeliverShipmentTaskBinding.edtOtp.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.inputOtp.setErrorEnabled(true);
                    fragmentDeliverShipmentTaskBinding.inputOtp.setError("Enter OTP");
                    return;
                }

                fragmentDeliverShipmentTaskBinding.inputOtp.setErrorEnabled(false);

                if (fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString().isEmpty()) {
                    fragmentDeliverShipmentTaskBinding.inputPaidAmount.setErrorEnabled(true);
                    fragmentDeliverShipmentTaskBinding.inputPaidAmount.setError("Enter Paid Amount!");
                    return;
                }

                if(Integer.parseInt(fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString())<resSearchTrackingNo.getData().getBilledAmount()){
                    fragmentDeliverShipmentTaskBinding.inputPaidAmount.setErrorEnabled(true);
                    fragmentDeliverShipmentTaskBinding.inputPaidAmount.setError("Paid amount should be equal to billed amount.");
                    return;
                }

                fragmentDeliverShipmentTaskBinding.inputPaidAmount.setErrorEnabled(false);
                resSearchTrackingNo.getData().setOTP(fragmentDeliverShipmentTaskBinding.edtOtp.getText().toString());
                resSearchTrackingNo.getData().setPaidAmount(fragmentDeliverShipmentTaskBinding.edtPaidAmount.getText().toString());

                AppController.getInstance().resSearchTrackingNo = resSearchTrackingNo;

                Navigation.findNavController(fragmentDeliverShipmentTaskBinding.getRoot()).navigate(R.id.actionDeliverShipmentTaskConfirm);
            }
        });*/
    }

    private void searchTrackingNo(String trackingNo) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_SEARCH_SHIPMENT_TRACKING_NO + trackingNo, 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        resSearchTrackingNo = new Gson().fromJson(jsonObject.toString(), ResSearchTrackingNo.class);
        initData();

    }

    private void initData() {
        fragmentDeliverShipmentTaskBinding.txtBillAmount.setText(getString(R.string.currency) + "" + resSearchTrackingNo.getData().getBilledAmount());
        fragmentDeliverShipmentTaskBinding.txtReceiverName.setText(resSearchTrackingNo.getData().getReceiverName());
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentDeliverShipmentTaskBinding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}
