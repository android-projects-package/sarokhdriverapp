package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class ResDeliverOrderStatusCompleted {
    private List<ResSearchShipment.Data> dataList;

    public List<ResSearchShipment.Data> getDataList() {
        return dataList;
    }

    public void setDataList(List<ResSearchShipment.Data> dataList) {
        this.dataList = dataList;
    }

}
