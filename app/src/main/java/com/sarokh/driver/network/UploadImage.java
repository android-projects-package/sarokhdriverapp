package com.sarokh.driver.network;

import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.sarokh.driver.callback.CallbackUploadImage;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

/**
 * Created by ranaaamersohail on 6/5/18.
 */

public class UploadImage {
    private CallbackUploadImage uploadImageCallBack;

    public UploadImage(CallbackUploadImage uploadImageCallBack) {
        this.uploadImageCallBack = uploadImageCallBack;
    }

    public void uploadProfilePicToServer(String url, File file, HashMap<String, String> param) {
        String baseUrl = AppConstants.BASE_URL + url;
        AndroidNetworking.upload(baseUrl)
                .addMultipartFile("userfile", file)
                .addMultipartParameter(param)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        Log.d("uploading", bytesUploaded + "value");
                        int progress = (int) ((bytesUploaded / (float) totalBytes) * 100);
                        uploadImageCallBack.uploadImageProgress(progress);
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // do anything with response
                        uploadImageCallBack.uploadImageSuccess(response);
                    }

                    @Override
                    public void onError(ANError error) {
                        uploadImageCallBack.uploadImageFailed(error.getMessage());
                        // handle error
                    }
                });
    }
}
