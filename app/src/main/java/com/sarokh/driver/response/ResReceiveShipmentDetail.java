package com.sarokh.driver.response;

import java.util.List;

/**
 * Created by becody.com on 11,04,2020
 */
public class ResReceiveShipmentDetail {
    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data {
        private String trackingNo;
        private String receiverName;
        private String receivedStatus;
        private float billedAmount;
        private boolean isConfirmed = false;

        public boolean isConfirmed() {
            return isConfirmed;
        }

        public void setConfirmed(boolean confirmed) {
            isConfirmed = confirmed;
        }

        public String getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(String trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public float getBilledAmount() {
            return billedAmount;
        }

        public void setBilledAmount(float billedAmount) {
            this.billedAmount = billedAmount;
        }

        public String getReceivedStatus() {
            return receivedStatus;
        }

        public void setReceivedStatus(String receivedStatus) {
            this.receivedStatus = receivedStatus;
        }


        @Override
        public String toString() {
            return "Data{" +
                    "trackingNo='" + trackingNo + '\'' +
                    ", receiverName='" + receiverName + '\'' +
                    ", receivedStatus='" + receivedStatus + '\'' +
                    ", billedAmount=" + billedAmount +
                    ", isConfirmed=" + isConfirmed +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "ResReceiveShipmentDetail{" +
                "data=" + data +
                '}';
    }
}