package com.sarokh.driver.response;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by becody.com on 12,04,2020
 */
public class ResSarokhTask {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        private String taskId;
        private int receiveShipments;
        private boolean allShipmentsReceived;
        private int remainingShipments;
        private int giveShipments;
        private boolean allShipmentsGiven;
        private int returnShipments;
        private float payCOD;
        private boolean isCODPaid;
        private float pendingCOD;
        private int receivedShipmentsCount;

        private String pointName;




        private String[] giveShipmentList;
        private String[] receiveShipmentList;

        private String driverName;
        private String driverId;
        private String dealerId;
        private String report;
        private String tripId;

        private List<DriverTransactionShipmentsDTO> giveShipmentsList = new ArrayList<>();
        private List<DriverTransactionShipmentsDTO> receiveShipmentsList = new ArrayList<>();
        private List<String> scannedShipments = new ArrayList<>();
        private Double dealerAmount;

        private String locationName;
        private String managerName;



        public String getLocationName() {
            return locationName;
        }

        public void setLocationName(String locationName) {
            this.locationName = locationName;
        }

        public String getManagerName() {
            return managerName;
        }

        public void setManagerName(String managerName) {
            this.managerName = managerName;
        }

        public Double getDealerAmount() {
            return dealerAmount;
        }

        public void setDealerAmount(Double dealerAmount) {
            this.dealerAmount = dealerAmount;
        }

        public List<String> getScannedShipments() {
            return scannedShipments;
        }

        public void setScannedShipments(List<String> scannedShipments) {
            this.scannedShipments = scannedShipments;
        }



        public int getReceivedShipmentsCount() {
            return receivedShipmentsCount;
        }

        public void setReceivedShipmentsCount(int receivedShipmentsCount) {
            this.receivedShipmentsCount = receivedShipmentsCount;
        }

        public String getPointName() {
            return pointName;
        }

        public void setPointName(String pointName) {
            this.pointName = pointName;
        }

        public List<DriverTransactionShipmentsDTO> getGiveShipmentsList() {
            return giveShipmentsList;
        }

        public void setGiveShipmentsList(List<DriverTransactionShipmentsDTO> giveShipmentsList) {
            this.giveShipmentsList = giveShipmentsList;
        }

        public List<DriverTransactionShipmentsDTO> getReceiveShipmentsList() {
            return receiveShipmentsList;
        }

        public void setReceiveShipmentsList(List<DriverTransactionShipmentsDTO> receiveShipmentsList) {
            this.receiveShipmentsList = receiveShipmentsList;
        }

        public String[] getGiveShipmentList() {
            return giveShipmentList;
        }

        public void setGiveShipmentList(String[] giveShipmentList) {
            this.giveShipmentList = giveShipmentList;
        }

        public String[] getReceiveShipmentList() {
            return receiveShipmentList;
        }

        public void setReceiveShipmentList(String[] receiveShipmentList) {
            this.receiveShipmentList = receiveShipmentList;
        }

        public int getRemainingShipments() {
            return remainingShipments;
        }

        public void setRemainingShipments(int remainingShipments) {
            this.remainingShipments = remainingShipments;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public int getReceiveShipments() {
            return receiveShipments;
        }

        public void setReceiveShipments(int receiveShipments) {
            this.receiveShipments = receiveShipments;
        }

        public boolean isAllShipmentsReceived() {
            return allShipmentsReceived;
        }

        public void setAllShipmentsReceived(boolean allShipmentsReceived) {
            this.allShipmentsReceived = allShipmentsReceived;
        }

        public int getGiveShipments() {
            return giveShipments;
        }

        public void setGiveShipments(int giveShipments) {
            this.giveShipments = giveShipments;
        }

        public boolean isAllShipmentsGiven() {
            return allShipmentsGiven;
        }

        public void setAllShipmentsGiven(boolean allShipmentsGiven) {
            this.allShipmentsGiven = allShipmentsGiven;
        }

        public int getReturnShipments() {
            return returnShipments;
        }

        public void setReturnShipments(int returnShipments) {
            this.returnShipments = returnShipments;
        }

        public float getPayCOD() {
            return payCOD;
        }

        public void setPayCOD(float payCOD) {
            this.payCOD = payCOD;
        }

        public boolean isCODPaid() {
            return isCODPaid;
        }

        public void setCODPaid(boolean CODPaid) {
            isCODPaid = CODPaid;
        }

        public float getPendingCOD() {
            return pendingCOD;
        }

        public void setPendingCOD(float pendingCOD) {
            this.pendingCOD = pendingCOD;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getDealerId() {
            return dealerId;
        }

        public void setDealerId(String dealerId) {
            this.dealerId = dealerId;
        }

        public String getReport() {
            return report;
        }

        public void setReport(String report) {
            this.report = report;
        }

        public String getTripId() {
            return tripId;
        }

        public void setTripId(String tripId) {
            this.tripId = tripId;
        }


        @Override
        public String toString() {
            return "Data{" +
                    "taskId='" + taskId + '\'' +
                    ", receiveShipments=" + receiveShipments +
                    ", allShipmentsReceived=" + allShipmentsReceived +
                    ", remainingShipments=" + remainingShipments +
                    ", giveShipments=" + giveShipments +
                    ", allShipmentsGiven=" + allShipmentsGiven +
                    ", returnShipments=" + returnShipments +
                    ", payCOD=" + payCOD +
                    ", isCODPaid=" + isCODPaid +
                    ", pendingCOD=" + pendingCOD +
                    ", pointName='" + pointName + '\'' +
                    ", giveShipmentList=" + Arrays.toString(giveShipmentList) +
                    ", receiveShipmentList=" + Arrays.toString(receiveShipmentList) +
                    ", driverName='" + driverName + '\'' +
                    ", driverId='" + driverId + '\'' +
                    ", dealerId='" + dealerId + '\'' +
                    ", report='" + report + '\'' +
                    ", tripId='" + tripId + '\'' +
                    ", giveShipmentsList=" + giveShipmentsList +
                    ", receiveShipmentsList=" + receiveShipmentsList +
                    '}';
        }
    }


    public static class DriverTransactionShipmentsDTO{
        private String trackingNumber;
        private String status;

        public String getTrackingNumber() {
            return trackingNumber;
        }


        public void setTrackingNumber(String trackingNumber) {
            this.trackingNumber = trackingNumber;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        @Override
        public String toString() {
            return "DriverTransactionShipmentsDTO{" +
                    "trackingNumber='" + trackingNumber + '\'' +
                    ", status='" + status + '\'' +
                    '}';
        }
    }


    @Override
    public String toString() {
        return "ResSarokhTask{" +
                "data=" + data +
                '}';
    }
}

