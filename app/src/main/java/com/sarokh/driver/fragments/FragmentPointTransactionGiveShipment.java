package com.sarokh.driver.fragments;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MainActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentPointTransactionGiveShipmentBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTask;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FragmentPointTransactionGiveShipment extends Fragment implements CallbackResponse {

    private FragmentPointTransactionGiveShipmentBinding fragmentPointTransactionGiveShipmentBinding;

    TableLayout ll;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        fragmentPointTransactionGiveShipmentBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_point_transaction_give_shipment, container, false);
        fragmentPointTransactionGiveShipmentBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(fragmentPointTransactionGiveShipmentBinding.getRoot()).navigate(R.id.actionPointTransactionPayment);
            }
        });

        fragmentPointTransactionGiveShipmentBinding.btnRefresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loadData();
            }
        });


        //btnConfirm
        return fragmentPointTransactionGiveShipmentBinding.getRoot();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (AppController.getInstance().resSarokhTask == null) {
            loadData();
            return;
        }
        /*if (AppController.getInstance().resSarokhTask.getData() == null) {
            loadData();
            return;
        }*/
        initUi();
    }

    private void loadData() {
        try {
            fragmentPointTransactionGiveShipmentBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(FragmentPointTransactionGiveShipment.this);
            JSONObject jsonObject = new JSONObject();

            networkLoader.loadJsonRequest(AppConstants.END_POST_POINT_SUMMARY + "" + AppController.getInstance().resSarokhTask.getData().getTaskId(), 1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentPointTransactionGiveShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

        if(type == 1){

            Gson gson = new Gson();

            AppController.getInstance().resSarokhTask = gson.fromJson(jsonObject.toString(), ResSarokhTask.class);
            Log.d("JSONDATA",jsonObject.toString());
            Log.d("CASTEDDATA",AppController.getInstance().resSarokhTask.toString());

            if (AppController.getInstance().resSarokhTask.getData() == null) {
                try {
                    fragmentPointTransactionGiveShipmentBinding.btnConfirm.setEnabled(false);
                    fragmentPointTransactionGiveShipmentBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                    AppConstants.showDialogWithoutTitle(getActivity(), jsonObject.getString("message"));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            else {
                if (AppController.getInstance().resSarokhTask.getData().getGiveShipments() == 0) {
                    fragmentPointTransactionGiveShipmentBinding.btnConfirm.setEnabled(false);
                    fragmentPointTransactionGiveShipmentBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                }


                initUi();
            }

        }
        else{

            try {

                AppConstants.showDialogWithoutTitle(getContext(), (String) jsonObject.get("message"));

                getActivity().onBackPressed();

            } catch (JSONException e) {

                e.printStackTrace();

            }
        }

    }

    @Override
    public void onResponse(String response, int type) {
        fragmentPointTransactionGiveShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentPointTransactionGiveShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentPointTransactionGiveShipmentBinding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);

    }


    private void initUi() {

        if(AppController.getInstance().resSarokhTask.getData().getPointName()!=null){
            fragmentPointTransactionGiveShipmentBinding.textviewPointName.setText(AppController.getInstance().resSarokhTask.getData().getPointName());
        }

        if(AppController.getInstance().resSarokhTask.getData().getTaskId()!=null){
            fragmentPointTransactionGiveShipmentBinding.textviewTaskId.setText(AppController.getInstance().resSarokhTask.getData().getTaskId());
        }

        if(AppController.getInstance().resSarokhTask.getData().getGiveShipmentList()!=null){
            ll = (TableLayout)  fragmentPointTransactionGiveShipmentBinding.receiveShipmentsTable;

            ll.removeAllViews();
            TableRow row1= new TableRow(getActivity());
            TableRow.LayoutParams lp1 = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
            row1.setLayoutParams(lp1);
            TextView serialNumber1 = new TextView(getActivity());
            serialNumber1.setText("SR#");
            TextView trackingNumber1 = new TextView(getActivity());
            trackingNumber1.setText("Tracking No");
            TextView status1 = new TextView(getActivity());
            status1.setText("Status");
            row1.addView(serialNumber1);
            row1.addView(trackingNumber1);
            row1.addView(status1);
            row1.setBackgroundResource(R.drawable.table_row_border);
            ll.addView(row1,0);

            for (int i = 0; i <AppController.getInstance().resSarokhTask.getData().getGiveShipmentsList().size(); i++) {
                TableRow row= new TableRow(getActivity());
                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                row.setLayoutParams(lp);
                TextView serialNumber = new TextView(getActivity());
                serialNumber.setText(String.valueOf(i+1));
                TextView trackingNumber = new TextView(getActivity());
                trackingNumber.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipmentsList().get(i).getTrackingNumber().toString());
                TextView status = new TextView(getActivity());
                
                status.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipmentsList().get(i).getStatus());

                if(AppController.getInstance().resSarokhTask.getData().getGiveShipmentsList().get(i).getStatus().equals("Assigned")){

                    status.setText("Waiting");
                }

                row.addView(serialNumber);
                row.addView(trackingNumber);
                row.addView(status);
                row.setBackgroundResource(R.drawable.table_row_border);
                ll.addView(row,i+1);
            }
        }


        /*if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgReceiveShipmentConfirm.setVisibility(View.GONE);
        }

        if (AppController.getInstance().resSarokhTask.getData().isAllShipmentsReceived()) {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.VISIBLE);
        } else {
            sarokhTaskBinding.imgGiveShipmentDetailConfirm.setVisibility(View.GONE);
        }
        sarokhTaskBinding.txtReceiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getReceiveShipments() + "");
        sarokhTaskBinding.txtGiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");
        sarokhTaskBinding.txtPayCodVal.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD() + "");
         */
    }
}