package com.sarokh.driver.callback;

/**
 * Created by becody.com on 01,03,2020
 */
public interface CallbackDateAndTime {
    void onDataAndTimeSelected(boolean isDate, String val);
}
