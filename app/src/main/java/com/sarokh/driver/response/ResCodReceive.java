package com.sarokh.driver.response;

/**
 * Created by becody.com on 15,04,2020
 */
public class ResCodReceive {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        private int dealerId;
        private String dealerName;
        private String businessName;
        private float billedCOD;

        public int getDealerId() {
            return dealerId;
        }

        public void setDealerId(int dealerId) {
            this.dealerId = dealerId;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public String getBusinessName() {
            return businessName;
        }

        public void setBusinessName(String businessName) {
            this.businessName = businessName;
        }

        public float getBilledCOD() {
            return billedCOD;
        }

        public void setBilledCOD(float billedCOD) {
            this.billedCOD = billedCOD;
        }
    }
}