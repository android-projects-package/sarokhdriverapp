package com.sarokh.driver.utils;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.sarokh.driver.R;

/**
 * Created by becody.com on 22,02,2020
 */
public class CustomToast {
    public static void showToast(Context _cntx, String msg) {
        Toast toast = Toast.makeText(_cntx, msg, Toast.LENGTH_LONG);
        View view = toast.getView();
        view.setBackgroundResource(R.drawable.custom_toast);
        TextView text = (TextView) view.findViewById(android.R.id.message);
        text.setPadding(20,0,20,0);

        //Shadow of the Of the Text Color
        text.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
        text.setTextColor(Color.WHITE);
        toast.show();
    }
}
