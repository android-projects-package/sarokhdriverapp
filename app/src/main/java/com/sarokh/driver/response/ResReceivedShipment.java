package com.sarokh.driver.response;

/**
 * Created by becody.com on 23,02,2020
 */
public class ResReceivedShipment {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        private int id;
        private String trackingNo;
        private String mobile;
        private String shipmentClassification;
        private String shipmentPrice;
        private String pickupDatetime;
        private String pickupBy;
        private String driverArrivalTime;
        private String receiverName;
        private String orderType;
        private float deliveryCharges;
        private String shipmentNo;
        private float createdDatetime;
        private boolean shipmentReceived;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(String trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getShipmentClassification() {
            return shipmentClassification;
        }

        public void setShipmentClassification(String shipmentClassification) {
            this.shipmentClassification = shipmentClassification;
        }

        public String getShipmentPrice() {
            return shipmentPrice;
        }

        public void setShipmentPrice(String shipmentPrice) {
            this.shipmentPrice = shipmentPrice;
        }

        public String getPickupDatetime() {
            return pickupDatetime;
        }

        public void setPickupDatetime(String pickupDatetime) {
            this.pickupDatetime = pickupDatetime;
        }

        public String getPickupBy() {
            return pickupBy;
        }

        public void setPickupBy(String pickupBy) {
            this.pickupBy = pickupBy;
        }

        public String getDriverArrivalTime() {
            return driverArrivalTime;
        }

        public void setDriverArrivalTime(String driverArrivalTime) {
            this.driverArrivalTime = driverArrivalTime;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public float getDeliveryCharges() {
            return deliveryCharges;
        }

        public void setDeliveryCharges(float deliveryCharges) {
            this.deliveryCharges = deliveryCharges;
        }

        public String getShipmentNo() {
            return shipmentNo;
        }

        public void setShipmentNo(String shipmentNo) {
            this.shipmentNo = shipmentNo;
        }

        public float getCreatedDatetime() {
            return createdDatetime;
        }

        public void setCreatedDatetime(float createdDatetime) {
            this.createdDatetime = createdDatetime;
        }

        public boolean isShipmentReceived() {
            return shipmentReceived;
        }

        public void setShipmentReceived(boolean shipmentReceived) {
            this.shipmentReceived = shipmentReceived;
        }
    }
}