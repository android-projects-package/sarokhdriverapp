package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.sarokh.driver.R;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterTracking extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<String> dataList;
    private Context _context;

    public AdapterTracking(List<String> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTrackingNo;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtTrackingNo = itemView.findViewById(R.id.txtTrackingNo);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_shipper_tracking_list, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                String str = dataList.get(position);
                cellViewHolder.txtTrackingNo.setText(str);
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
