package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackRecyclerViewClick;
import com.sarokh.driver.callback.CallbackRecyclerViewClickType;
import com.sarokh.driver.response.ResCodDetail;
import com.sarokh.driver.response.ResSarokhTaskDriver;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterSarokhTaskDriver extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResSarokhTaskDriver.Data> dataList;
    private Context _context;
    private CallbackRecyclerViewClickType<ResSarokhTaskDriver.Data> callbackRecyclerViewClick;

    public AdapterSarokhTaskDriver(List<ResSarokhTaskDriver.Data> dataList, CallbackRecyclerViewClickType<ResSarokhTaskDriver.Data> callbackRecyclerViewClick) {
        this.dataList = dataList;
        this.callbackRecyclerViewClick = callbackRecyclerViewClick;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtShopName;
        private TextView txt1;
        private ImageView imgDone;
        private ImageView imgMapDirection;
        private TextView txtType;
        private TextView txtReceiverName;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtShopName = itemView.findViewById(R.id.txtShopName);
            txt1 = itemView.findViewById(R.id.txt1);
            imgDone = itemView.findViewById(R.id.imgDone);
            imgMapDirection = itemView.findViewById(R.id.imgMapDirection);
            txtType = itemView.findViewById(R.id.txtType);
            txtReceiverName = itemView.findViewById(R.id.txtReceiverName);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callbackRecyclerViewClick.onClickRow(dataList.get(getAdapterPosition()), 0);
                }
            });
            imgDone.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callbackRecyclerViewClick.onClickRow(dataList.get(getAdapterPosition()), 1);
                }
            });

            imgMapDirection.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callbackRecyclerViewClick.onClickRow(dataList.get(getAdapterPosition()), 2);
                }
            });
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_sarokh_task_driver, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        CellViewHolder cellViewHolder = (CellViewHolder) holder;
        ResSarokhTaskDriver.Data data = dataList.get(position);
               /* if (data.getStopType().equals("Last Mile")) {
                    cellViewHolder.imgDone.setVisibility(View.VISIBLE);
                } else {*/
        cellViewHolder.imgDone.setVisibility(View.GONE);

        if (data.getLocationName() == null) {
            cellViewHolder.txtShopName.setText(data.getStopType());
        } else if (data.getLocationName().trim().isEmpty()) {
            cellViewHolder.txtShopName.setText(data.getStopType());
        } else {
            cellViewHolder.txtShopName.setText(data.getLocationName());
        }
        cellViewHolder.txtType.setText(data.getReceiverType());
        cellViewHolder.txtReceiverName.setText(data.getReceiverName());
        if (data.getStopType().equals("Dealer Point") || data.getStopType().equals("Sarokh Point")) {
            cellViewHolder.txt1.setText("Point Name");
        } else if (data.getStopType().equals("Shipper Warehouse")) {
            cellViewHolder.txt1.setText("Location Name");
        } else if (data.getStopType().equals("Last Mile")) {
            cellViewHolder.txt1.setText("Location");
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
