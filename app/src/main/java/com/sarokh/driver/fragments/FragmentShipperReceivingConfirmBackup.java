package com.sarokh.driver.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Request;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.adapters.AdapterTracking;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentShipperReceivingConfirmBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by becody.com on 13,04,2020
 */
public class FragmentShipperReceivingConfirmBackup extends Fragment implements CallbackResponse, SignaturePad.OnSignedListener {
    private FragmentShipperReceivingConfirmBinding binding;
    private Bitmap bitmapSignature;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shipper_receiving_confirm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (AppController.getInstance().resShipperTrackingModal == null) {
            return;
        }
    //    binding.txtShipper.setText(AppController.getInstance().resShipperTrackingModal.getShipperName());
        binding.signaturePad.setOnSignedListener(this);
        binding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.signaturePad.clear();
            }
        });
        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmapSignature == null) {
                    CustomToast.showToast(getActivity(), "Enter Signature");
                    return;
                }
                submitDeliverShipment();
            }
        });
        setUpRecyclerView();
    }

    private void submitDeliverShipment() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            JSONArray jsonArray = new JSONArray();
            for (String list : AppController.getInstance().resShipperTrackingModal.getTrackingList()) {
                jsonArray.put(list);
            }
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("shipmentList", jsonArray);
            jsonObject.put("shipperId", AppController.getInstance().resShipperTrackingModal.getShipperId());
            jsonObject.put("shipperName", AppController.getInstance().resShipperTrackingModal.getShipperName());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            jsonObject.put("signature", toBase64(bitmapSignature));
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_POST_SHIPPER_RECEIVE_SHIPMENTS, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void setUpRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        binding.listing.setLayoutManager(linearLayoutManager);
        AdapterTracking adapterTracking = new AdapterTracking(AppController.getInstance().resShipperTrackingModal.getTrackingList());
        binding.listing.setAdapter(adapterTracking);

    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            CustomToast.showToast(getContext(), jsonObject.getString("message"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        bitmapSignature = binding.signaturePad.getSignatureBitmap();
    }

    @Override
    public void onClear() {
        bitmapSignature = null;
    }
}
