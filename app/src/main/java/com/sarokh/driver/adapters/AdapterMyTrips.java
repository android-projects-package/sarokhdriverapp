package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResCodDetail;
import com.sarokh.driver.response.ResMyTrips;
import com.sarokh.driver.utils.AppConstants;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterMyTrips extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResMyTrips.Data> dataList;
    private Context _context;

    public AdapterMyTrips(List<ResMyTrips.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtDate;
        private TextView txtPosition;
        private TextView txtTripId;
        private TextView txtTotalPickUp;
        private TextView txtTotalDeliveries;
        private TextView txtStartTime;
        private TextView txtEndTime;
        private TextView txtTotalStop;
        private TextView txtCodRecover;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtPosition = itemView.findViewById(R.id.txtPosition);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtTripId = itemView.findViewById(R.id.txtTripId);
            txtTotalPickUp = itemView.findViewById(R.id.txtTotalPickUp);
            txtTotalDeliveries = itemView.findViewById(R.id.txtTotalDeliveries);
            txtStartTime = itemView.findViewById(R.id.txtStartTime);
            txtEndTime = itemView.findViewById(R.id.txtEndTime);
            txtTotalStop = itemView.findViewById(R.id.txtTotalStop);
            txtCodRecover = itemView.findViewById(R.id.txtCodRecover);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_my_trips, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResMyTrips.Data data = dataList.get(position);
                cellViewHolder.txtPosition.setText((position+1)+"");
                cellViewHolder.txtDate.setText(data.getDate() == null ? "" : data.getDate());
                cellViewHolder.txtStartTime.setText(data.getStartTime() == null ? "" : data.getStartTime());
                cellViewHolder.txtEndTime.setText(data.getEndTime() == null ? "" : data.getEndTime());
                cellViewHolder.txtTripId.setText(data.getTripId() + "");
                cellViewHolder.txtTotalPickUp.setText(data.getTotalPickups() + "");
                cellViewHolder.txtTotalDeliveries.setText(data.getTotalDeliveries() + "");
                cellViewHolder.txtTotalStop.setText(data.getTotalStops() + "");
                cellViewHolder.txtCodRecover.setText(AppConstants.priceFormat(_context, data.getPaymentCollection(), false));
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
