package com.sarokh.driver.utils;

import android.content.Context;

import com.sarokh.driver.R;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

/**
 * Created by becody.com on 22,02,2020
 */
public class AppConstants {

    public final static String END_USER_CHANGED_PWD = "/user/change-password";
    public final static String END_RECEIVE_SHIPMENT = "/shipment-mobile/search-receive-shipment/";
    public final static String END_DELIVER_ORDER_STATUS = "/shipment-mobile/get-order-status/";
    public final static String END_CASH_HAND_OVER = "/shipment-mobile/get-agents-drivers/";
    public final static String END_CASH_HAND_OVER_SUBMISSION = "/shipment-mobile/cash-handover-dealer/";
    public final static String END_SEARCH_BY_TRACKING_MOBILE = "/shipment-mobile/searchby-tracking-mobile/";
    public final static String END_SHIPMENT_RECEIVED = "/shipment-mobile/handover-recieved-shipment/";
 //   public final static String END_DEALER_SHIPMENT = "/shipment-mobile/get-dealer-shipments/";
    public final static String KEY_FOR_SIGNATURE = "key_for_signature_activity";

    // new APp changes
    //public final static String BASE_URL = "http://vps789305.ovh.net:8443";
    public final static String BASE_URL = "http://vps789305.ovh.net:8080";
    public final static String END_POINT_LOGIN = "/mobile-driver/login-driver";
    public final static String END_DASHBOARD = "/mobile-driver/get-driver-dashboard/";
    public final static String END_GET_SAROKH_TASK = "/mobile-driver/get-sarokh-task/";
    public final static String END_GET_SAROKH_TASKV2 = "/mobile-driver/get-sarokh-driver-task";
    public final static String END_POST_POINT_SUMMARY = "/mobile-driver/get-point-summary/";
    public final static String END_GET_RECEIVE_SHIPMENT_DETAIL = "/mobile-driver/get-recieve-shipments-detail/";
    public final static String END_GET_RECEIVE_SHIPMENT_DETAIL_V2 = "/mobile-driver/get-recieve-shipments-detail-v2/";
    public final static String END_GET_GIVE_SHIPMENT_DETAIL = "/mobile-driver/get-give-shipments-detail/";
    public final static String END_GET_PAY_COD_DETAIL = "/dealer-mobile/get-pay-cod-detail/";
    public final static String END_GET_SAROKH_TASK_CONFIRMATION = "/mobile-driver/sarokh-task-confirm/";
    public final static String END_POST_SUBMIT_SAROKH_TASK = "/mobile-driver/submit-driver-task-confirm/";
    public final static String END_SEARCH_SHIPMENT_TRACKING_NO = "/dealer-mobile/search-shipment-trackingNo/";
    public final static String END_HAND_OVER_RECEIVED_SHIPMENT = "/dealer-mobile/handover-recieved-shipment/";
    public final static String END_REPORT_ISSUE_SHIPMENT = "/dealer-mobile/report-issue-shipment/";
    public final static String END_COD_LEDGER = "/dealer-mobile/get-cod-ledger/";
    public final static String END_MY_TRIPS = "/mobile-driver/get-driver-trips/";
    public final static String END_GET_SHIPPER_LIST = "/dealer-mobile/get-shipper-list/";
    public final static String END_GET_SHIPPER_RECEIVING = "/dealer-mobile/get-shipper-receive/";
    public final static String END_POST_SHIPPER_RECEIVE_SHIPMENTS = "/dealer-mobile/confirm-shipper-receive-shipments/";
    public final static String END_GET_COD_RECEIVE = "/mobile-driver/get-receive-cod/";
    public final static String END_GET_COD_SUBMIT = "/mobile-driver/receive-amount";
    public final static String END_POST_ADD_EXPENSE = "/driver-expense/add/";
    public final static String END_POST_ADD_MY_VEHICLE= "/mobile-driver/add-my-vehicle";
    public final static String END_DRIVER_WALLET = "/mobile-driver/get-driver-wallet/";
    public final static String END_END_WARE_HOUSE = "/mobile-driver/get-shipper-warehouse-shipments/";
    public final static String END_VERIFY_TRACKING_NUMBER = "/dealer-mobile/verify-shipment-trackingNo/";
    public final static String END_FORGET_PASSWORD = "/user/forgot-password";
    public final static String END_UPLOAD_FILE= "/upload-file";

    public static final String DATE_FORMAT_12 = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_TIME = "HH:mm:ss";
    public static final String DRIVER_RECEIVE_AMOUNT = "/mobile-driver-v2/receive-amount";
    public static final String DRIVER_RECEIVE_SHIPMENT = "/mobile-driver-v2/driver-receive-shipment/";
    public static final String SHIPPER_WAREHOUSE_TASK_COMPLETION = "/mobile-driver/shipper-warehouse-task-completion/";
    public static final String GET_DEALER_WALLET = "/driver-transaction/get-dealer-wallet/";
    public static final String GET_DEALER_DETAILS = "/dealer-point/get-details/";
    public static final String GET_LAST_MILE_TASK = "/mobile-driver/get-last-mile-task/";
    public static final String DELIVER_LAST_MILE_SHIPMENT = "/mobile-driver/last-mile-deliver-shipment";

    public static String getCurDateAndTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_12);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }

    public static String getCurTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_TIME);
        dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date today = Calendar.getInstance().getTime();
        return dateFormat.format(today);
    }


    public static void showErrorDialog(Context context, String msg) {
        final PrettyDialog prettyDialog = new PrettyDialog(context);
        prettyDialog.setTitle("Error")
                .setMessage(msg)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .addButton(
                        "OK",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                            }
                        }
                ).show();
    }

    public static void showConfirmationDialog(Context context,String message){
        final PrettyDialog prettyDialog = new PrettyDialog(context);
        prettyDialog.setTitle("Alert")
                .setMessage(message)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .addButton(
                        "OK",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                            }
                        }
                ).
                addButton("Cancel",
                        R.color.pdlg_color_white,
                        R.color.colorPrimary,
                        new PrettyDialogCallback() {
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                            }
                        })
                .show();
    }





    public static void showDialogWithoutTitle(Context context, String msg) {
        final PrettyDialog prettyDialog = new PrettyDialog(context);
        prettyDialog.setMessage(msg)
                .setIcon(R.drawable.pdlg_icon_info)
                .setIconTint(R.color.green)
                .addButton(
                        "OK",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.colorPrimary,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                prettyDialog.dismiss();
                            }
                        }
                ).show();
    }

    public static String priceFormat(Context _context, float price, boolean needEndValue) {
        DecimalFormat formatter = new DecimalFormat("##.##");
        if (needEndValue) {
            return _context.getResources().getString(R.string.currency) + " " + formatter.format(price) + "/-";
        } else {
            return _context.getResources().getString(R.string.currency) + " " + formatter.format(price);
        }
    }

}