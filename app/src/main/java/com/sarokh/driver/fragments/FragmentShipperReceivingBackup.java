package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.ShipmentTrackingListM;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentShipperReceivingBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResShipperReceiving;
import com.sarokh.driver.response.ResShipperTrackingList;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentShipperReceivingBackup extends Fragment implements CallbackResponse {
    private FragmentShipperReceivingBinding binding;
    private CodeScanner mCodeScanner;
    private ShipmentTrackingListM shipmentTrackingListM;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_shipper_receiving, container, false);
        return binding.getRoot();
    }

    private void updateRecord() {
        if (shipmentTrackingListM == null) {
            binding.txtShipmentReceived.setText("0");
        }

        binding.txtShipmentReceived.setText(shipmentTrackingListM.getTrackingList().size() + "");
    }

    private void addTrackingIntoList(String tracking) {
        List<String> trackingList = shipmentTrackingListM.getTrackingList();
        trackingList.add(tracking);
        shipmentTrackingListM.setTrackingList(trackingList);
    }

    private boolean isTrackingAddedIntoList(String tracking) {
        for (String trackingListM : shipmentTrackingListM.getTrackingList()) {
            return trackingListM.equals(tracking);
        }

        return false;
    }

    private void setUpSpinnerData(List<ResShipperReceiving.Data> dataList) {
        String[] shipperName = new String[dataList.size()];
        int counter = 0;
        for (ResShipperReceiving.Data data : dataList) {
            shipperName[counter] = data.getShipperName();
            counter++;
        }
        binding.spinner.setItems(shipperName);
        if (dataList.size() > 0) {
            getShipmentList(0);
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        shipmentTrackingListM = new ShipmentTrackingListM();
        mCodeScanner = new CodeScanner(getActivity(), binding.scannerView);
        mCodeScanner.setScanMode(ScanMode.CONTINUOUS);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.edtTrackingNumber.setText(result.getText());
                        binding.edtTrackingNumber.setSelection(result.getText().length());
                    }
                });
            }
        });
     /*   binding.btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.inputTrackingNo.setErrorEnabled(true);
                    binding.inputTrackingNo.setError("Enter Or Scan QR Code");
                    return;
                }

                binding.inputTrackingNo.setErrorEnabled(false);
                doSearch(binding.edtTrackingNumber.getText().toString());
            }
        });*/
        binding.spinner.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                shipmentTrackingListM.setTrackingList(new ArrayList<String>());
                binding.txtShipmentReceived.setText("0");
                getShipmentList(position);
            }
        });
        binding.btnProceedToNxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (shipmentTrackingListM == null) {
                    CustomToast.showToast(getContext(), "Please receive shipment first");
                    return;
                }
                if (shipmentTrackingListM.getTrackingList().size() == 0) {
                    CustomToast.showToast(getContext(), "Please receive shipment first");
                    return;
                }
                AppController.getInstance().resShipperTrackingModal = shipmentTrackingListM;
                Navigation.findNavController(binding.getRoot()).navigate(R.id.actionShipperReceivingConfirm);
            }
        });
    }

    private void getShipmentList(int position) {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        ResShipperReceiving.Data resShipperReceiving = AppController.getInstance().resShipperReceiving.getData().get(position);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_SHIPPER_RECEIVING + resShipperReceiving.getShipperId(), 2);
    }

    private void doSearch(String trackingNo) {
        ResShipperTrackingList resReceiveShipmentDetail = AppController.getInstance().resShipperTrackingList;
        if (resReceiveShipmentDetail == null) {
            CustomToast.showToast(getContext(), "No tracking numbers list found");
            return;
        }
        if (resReceiveShipmentDetail.getData().getShipmentList() == null) {
            CustomToast.showToast(getContext(), "No tracking numbers list found");
            return;
        }

        if (resReceiveShipmentDetail.getData().getShipmentList().size() > 0) {
            boolean isTrackingFoundFromList = false;
            for (String tracking : resReceiveShipmentDetail.getData().getShipmentList()) {
                if (tracking.equals(trackingNo)) {
                    isTrackingFoundFromList = true;
                    break;
                }
            }

            if (isTrackingFoundFromList) {
                if (isTrackingAddedIntoList(trackingNo)) {
                    CustomToast.showToast(getActivity(), "Tracking Number Already Received");
                } else {
                    addTrackingIntoList(trackingNo);
                }
            } else {
                CustomToast.showToast(getActivity(), "Tracking Number Not Found");
            }
            binding.edtTrackingNumber.setText("");
            updateRecord();
        }
    }

    private void askForPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHavePermission()) {
                showPermissionDialog();
            } else {
                mCodeScanner.startPreview();
            }

        }
    }

    private void showPermissionDialog() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        CustomToast.showToast(getActivity(), "You cannot scan without permission");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppController.getInstance().resShipperReceiving == null) {
            loadData();
        } else {
            setUpSpinnerData(AppController.getInstance().resShipperReceiving.getData());
            updateRecord();
        }
        askForPermission();

    }

    private void loadData() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_SHIPPER_LIST, 1);
    }

    @Override
    public void onPause() {
        super.onPause();
        mCodeScanner.releaseResources();
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        if (type == 1) {
            AppController.getInstance().resShipperReceiving = new Gson().fromJson(jsonObject.toString(), ResShipperReceiving.class);
            setUpSpinnerData(AppController.getInstance().resShipperReceiving.getData());
        } else if (type == 2) {
            AppController.getInstance().resShipperTrackingList = new Gson().fromJson(jsonObject.toString(), ResShipperTrackingList.class);
            shipmentTrackingListM.setShipperId(AppController.getInstance().resShipperTrackingList.getData().getShipperId());
            shipmentTrackingListM.setShipperName(AppController.getInstance().resShipperTrackingList.getData().getShipperName());
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}