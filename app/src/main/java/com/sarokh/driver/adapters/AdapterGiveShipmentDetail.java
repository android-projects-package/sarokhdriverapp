package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResGiveShipmentDetail;

import java.util.List;

public class AdapterGiveShipmentDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResGiveShipmentDetail.Data> dataList;
    private Context _context;

    public AdapterGiveShipmentDetail(List<ResGiveShipmentDetail.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTrackingNo;
        private TextView txtType;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtTrackingNo = itemView.findViewById(R.id.txtTrackingNo);
            txtType = itemView.findViewById(R.id.txtType);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_give_shipment_detail, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResGiveShipmentDetail.Data data = dataList.get(position);
                cellViewHolder.txtTrackingNo.setText(data.getTrackingNo());
                cellViewHolder.txtType.setText(data.getType());
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
