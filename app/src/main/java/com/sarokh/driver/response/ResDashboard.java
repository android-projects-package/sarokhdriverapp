package com.sarokh.driver.response;

/**
 * Created by becody.com on 22,02,2020
 */
public class ResDashboard {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private int droppedOff;
        private int pickedUp;
        private int availableShipments;
        private int completedStops;
        private int remainingStops;
        private int trips;
        private String cargoCapacity;
        private String vehicle;
        private int nextStopDistance;
        private float collectionWalletBalance;
        private float compensationWalletBalance;

        public int getDroppedOff() {
            return droppedOff;
        }

        public void setDroppedOff(int droppedOff) {
            this.droppedOff = droppedOff;
        }

        public int getPickedUp() {
            return pickedUp;
        }

        public void setPickedUp(int pickedUp) {
            this.pickedUp = pickedUp;
        }

        public int getAvailableShipments() {
            return availableShipments;
        }

        public void setAvailableShipments(int availableShipments) {
            this.availableShipments = availableShipments;
        }

        public int getCompletedStops() {
            return completedStops;
        }

        public void setCompletedStops(int completedStops) {
            this.completedStops = completedStops;
        }

        public int getRemainingStops() {
            return remainingStops;
        }

        public void setRemainingStops(int remainingStops) {
            this.remainingStops = remainingStops;
        }

        public int getTrips() {
            return trips;
        }

        public void setTrips(int trips) {
            this.trips = trips;
        }

        public String getCargoCapacity() {
            return cargoCapacity;
        }

        public void setCargoCapacity(String cargoCapacity) {
            this.cargoCapacity = cargoCapacity;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

        public int getNextStopDistance() {
            return nextStopDistance;
        }

        public void setNextStopDistance(int nextStopDistance) {
            this.nextStopDistance = nextStopDistance;
        }

        public float getCollectionWalletBalance() {
            return collectionWalletBalance;
        }

        public void setCollectionWalletBalance(float collectionWalletBalance) {
            this.collectionWalletBalance = collectionWalletBalance;
        }

        public float getCompensationWalletBalance() {
            return compensationWalletBalance;
        }

        public void setCompensationWalletBalance(float compensationWalletBalance) {
            this.compensationWalletBalance = compensationWalletBalance;
        }
    }
}