package com.sarokh.driver.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.Navigation;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackForDialog;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSarokhTaskConfirmBinding;
import com.sarokh.driver.modals.SuccessM;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTaskConfirmation;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by app.com on 12,04,2020
 */
public class FragmentSarokhTaskConfirm extends BaseFragment implements CallbackResponse, SignaturePad.OnSignedListener, CallbackForDialog {
    private FragmentSarokhTaskConfirmBinding fragmentSarokhTaskConfirmBinding;
    private ResSarokhTaskConfirmation resSarokhTaskConfirmation;
    private Bitmap bitmapSignature = null;
    private int received = 0;
    private int receivedRemaining = 0;
    private Animation animBlink;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSarokhTaskConfirmBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sarokh_task_confirm, container, false);
        return fragmentSarokhTaskConfirmBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        fragmentSarokhTaskConfirmBinding.signaturePad.setOnSignedListener(this);
        // load the animation
        animBlink = AnimationUtils.loadAnimation(getContext(),
                R.anim.blink);
        fragmentSarokhTaskConfirmBinding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentSarokhTaskConfirmBinding.signaturePad.clear();
            }
        });
        fragmentSarokhTaskConfirmBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmapSignature == null) {
                    AppConstants.showDialogWithoutTitle(getActivity(), "Signature is required!");
                    return;
                }
                submitConfirmationTask();
            }
        });
        fragmentSarokhTaskConfirmBinding.container.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadRequest(true);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        loadRequest(false);
    }

    private void loadRequest(boolean isRefresh) {
        if (isRefresh) {
            fragmentSarokhTaskConfirmBinding.container.setRefreshing(true);
            fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        } else {
            fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            fragmentSarokhTaskConfirmBinding.container.setRefreshing(false);
        }
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_GET_SAROKH_TASK_CONFIRMATION + AppController.getInstance().resLoginObj.getData().getId(), 1);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        fragmentSarokhTaskConfirmBinding.container.setRefreshing(false);
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        if (type == 1) {
            resSarokhTaskConfirmation = new Gson().fromJson(jsonObject.toString(), ResSarokhTaskConfirmation.class);
            if (resSarokhTaskConfirmation == null) {
                fragmentSarokhTaskConfirmBinding.btnConfirm.setEnabled(false);
                fragmentSarokhTaskConfirmBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                fragmentSarokhTaskConfirmBinding.requestTxt.setVisibility(View.VISIBLE);
                fragmentSarokhTaskConfirmBinding.requestTxt.startAnimation(animBlink);
            } else if (resSarokhTaskConfirmation.getData() == null) {
                fragmentSarokhTaskConfirmBinding.btnConfirm.setEnabled(false);
                fragmentSarokhTaskConfirmBinding.btnConfirm.animate().alpha(0.5f).setDuration(100);
                fragmentSarokhTaskConfirmBinding.requestTxt.setVisibility(View.VISIBLE);
                fragmentSarokhTaskConfirmBinding.requestTxt.startAnimation(animBlink);
            } else {
                fragmentSarokhTaskConfirmBinding.requestTxt.setVisibility(View.GONE);
                fragmentSarokhTaskConfirmBinding.requestTxt.clearAnimation();
                fragmentSarokhTaskConfirmBinding.btnConfirm.setEnabled(true);
                fragmentSarokhTaskConfirmBinding.btnConfirm.animate().alpha(1f).setDuration(100);
                loadBasicUi();
                setUpConfirmationDialog();
            }
        } else if (type == 2) {
            try {
                printMessage(jsonObject.getString("message"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadBasicUi() {
        setScannedData();
        String value = String.format(getResources().getString(R.string.task_confirmation_str), AppController.getInstance().resLoginObj.getData().getUser().getFullName(),
                Integer.toString(AppController.getInstance().resLoginObj.getData().getId()),
                AppController.getInstance().resSarokhTask.getData().getDriverName(), AppController.getInstance().resSarokhTask.getData().getDriverId(), AppController.getInstance().resSarokhTask.getData().getTripId() + "", AppConstants.getCurDate(), AppConstants.getCurTime());
        fragmentSarokhTaskConfirmBinding.txtDescription.setText(Html.fromHtml(value));
        fragmentSarokhTaskConfirmBinding.txtReceiveShipmentVal.setText(received + "");
        fragmentSarokhTaskConfirmBinding.txtReceiveShipmentRemainingVal.setText(receivedRemaining + "");
        fragmentSarokhTaskConfirmBinding.txtDescription.setText(Html.fromHtml(value));
        fragmentSarokhTaskConfirmBinding.txtGiveShipmentVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");
        fragmentSarokhTaskConfirmBinding.txtGiveShipmentRemainingVal.setText(AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "");

        /* fragmentSarokhTaskConfirmBinding.txtCodPaid.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD());
        fragmentSarokhTaskConfirmBinding.txtPendingCod.setText(getString(R.string.currency) + "" + AppController.getInstance().resSarokhTask.getData().getPayCOD());
        */
        fragmentSarokhTaskConfirmBinding.txtDriverName.setText(AppController.getInstance().resSarokhTask.getData().getDriverName());
    }

    private void setScannedData() {
        receivedRemaining = AppController.getInstance().resSarokhTask.getData().getRemainingShipments();
        received = AppController.getInstance().resSarokhTask.getData().getReceiveShipments();
    }

    private void printMessage(String msg) {
        SuccessM successM = new SuccessM();
        successM.setTitle(msg);
        successM.setType(1);
        String description = "Received Shipment\n" + received + "\nGive Shipments\n" + AppController.getInstance().resSarokhTask.getData().getGiveShipments() + "\nAmount Paid \nSAR 2222/-";
        successM.setDescription(description);
        showSuccessDialog(successM, this);
    }

    private void submitConfirmationTask() {
        try {
            fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("confirmationId", resSarokhTaskConfirmation.getData().getConfirmationId());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            jsonObject.put("driverName", AppController.getInstance().resLoginObj.getData().getUser().getFullName());
            jsonObject.put("giveShipments", true);
            jsonObject.put("payCOD", true);
            jsonObject.put("pendingCOD", true);
            jsonObject.put("receiveShipments", true);
            jsonObject.put("reportIssues", true);
            jsonObject.put("signature", toBase64(bitmapSignature));
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_POST_SUBMIT_SAROKH_TASK, 2, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void setUpConfirmationDialog() {
        if (resSarokhTaskConfirmation.getData().isGiveShipments()) {
            fragmentSarokhTaskConfirmBinding.ImgGiveShipmentVal.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.ImgGiveShipmentVal.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isReceiveShipments()) {
            fragmentSarokhTaskConfirmBinding.imgReceiveShipmentVal.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgReceiveShipmentVal.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isPayCOD()) {
            fragmentSarokhTaskConfirmBinding.imgCodPaid.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgCodPaid.setVisibility(View.INVISIBLE);
        }

        if (resSarokhTaskConfirmation.getData().isPendingCOD()) {
            fragmentSarokhTaskConfirmBinding.imgPendingCod.setVisibility(View.VISIBLE);
        } else {
            fragmentSarokhTaskConfirmBinding.imgPendingCod.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onResponse(String response, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        fragmentSarokhTaskConfirmBinding.container.setRefreshing(false);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        fragmentSarokhTaskConfirmBinding.container.setRefreshing(false);
    }

    @Override
    public void onAppError(String msg, int type) {
        fragmentSarokhTaskConfirmBinding.progressBar.progressBar.setVisibility(View.GONE);
        fragmentSarokhTaskConfirmBinding.container.setRefreshing(false);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        bitmapSignature = fragmentSarokhTaskConfirmBinding.signaturePad.getSignatureBitmap();
    }

    @Override
    public void onClear() {
        bitmapSignature = null;
    }

    @Override
    public void onSuccessDialogDismiss(int type) {
        AppController.getInstance().resSarokhTask = null;
        AppController.getInstance().receiveShipmentDetail = null;
        Navigation.findNavController(fragmentSarokhTaskConfirmBinding.getRoot()).navigate(R.id.actionOpenDashboard);
    }

    @Override
    public void onErrorDialogDismiss(int type) {


    }
}
