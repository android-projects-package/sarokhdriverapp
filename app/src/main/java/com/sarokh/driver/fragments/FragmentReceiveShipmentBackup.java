package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.android.volley.Request;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentReceiveShipmentBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResReceiveShipmentDetail;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentReceiveShipmentBackup extends Fragment implements CallbackResponse {
    private FragmentReceiveShipmentBinding binding;
    private CodeScanner mCodeScanner;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_receive_shipment, container, false);
        return binding.getRoot();
    }

    private void updateRecord(List<ResReceiveShipmentDetail.Data> dataList) {
        int shipment = 0;
        int remainingShipment = dataList.size();
        for (ResReceiveShipmentDetail.Data obj : dataList) {
            if (obj.isConfirmed()) {
                shipment++;
                remainingShipment--;
            }
        }
        binding.txtConfirmShipment.setText(shipment + "");
        binding.txtRemaining.setText(remainingShipment + "");

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCodeScanner = new CodeScanner(getActivity(), binding.scannerView);
        mCodeScanner.setScanMode(ScanMode.CONTINUOUS);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        binding.edtTrackingNumber.setText(result.getText());
                        CustomToast.showToast(getActivity(), "Tracking Number Scanned");
                    }
                });
            }
        });
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (binding.edtTrackingNumber.getText().toString().isEmpty()) {
                    binding.inputTrackingNo.setErrorEnabled(true);
                    binding.inputTrackingNo.setError("Enter Or Scan QR Code");
                    return;
                }

                binding.inputTrackingNo.setErrorEnabled(false);
                doSearch(binding.edtTrackingNumber.getText().toString());
            }
        });
    }

    private void doSearch(String trackingNo) {
        ResReceiveShipmentDetail resReceiveShipmentDetail = AppController.getInstance().receiveShipmentDetail;
        if (resReceiveShipmentDetail.getData().size() > 0) {
            int defaultStatus = 0;
            int counter = 0;
            ResReceiveShipmentDetail.Data defaultObj = null;
            for (ResReceiveShipmentDetail.Data obj : resReceiveShipmentDetail.getData()) {
                if (obj.getTrackingNo().equals(trackingNo)) {
                    if (obj.isConfirmed()) {
                        defaultStatus = 2;
                    } else {
                        obj.setConfirmed(true);
                        defaultStatus = 1;
                    }
                    defaultObj = obj;

                    break;
                }
                counter++;
            }
            if (defaultStatus == 0) {
                CustomToast.showToast(getActivity(), "Tracking Number Not Found");
            } else if (defaultStatus == 2) {
                CustomToast.showToast(getActivity(), "Tracking Number Already shipped");
            } else {
                resReceiveShipmentDetail.getData().set(counter, defaultObj);
            }
            AppController.getInstance().receiveShipmentDetail = resReceiveShipmentDetail;
            updateRecord(resReceiveShipmentDetail.getData());
            binding.edtTrackingNumber.setText("");

        }
    }

    private void askForPermission() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            if (!checkIfAlreadyHavePermission()) {
                showPermissionDialog();
            } else {
                mCodeScanner.startPreview();
            }

        }
    }

    private void showPermissionDialog() {
        Dexter.withActivity(getActivity())
                .withPermission(Manifest.permission.CAMERA)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) {
                        mCodeScanner.startPreview();
                    }

                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) {
                        CustomToast.showToast(getActivity(), "You cannot scan without permission");
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).check();
    }

    private boolean checkIfAlreadyHavePermission() {
        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppController.getInstance().receiveShipmentDetail == null) {
            loadData();
        } else {
            updateRecord(AppController.getInstance().receiveShipmentDetail.getData());
        }
        askForPermission();

    }

    private void loadData() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("dealerId", AppController.getInstance().resSarokhTask.getData().getDealerId());
            jsonObject.put("driverId", AppController.getInstance().resLoginObj.getData().getId());
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_GET_RECEIVE_SHIPMENT_DETAIL, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mCodeScanner.releaseResources();
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppController.getInstance().receiveShipmentDetail = new Gson().fromJson(jsonObject.toString(), ResReceiveShipmentDetail.class);
        updateRecord(AppController.getInstance().receiveShipmentDetail.getData());
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }
}
