package com.sarokh.driver.network;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.utils.AppConstants;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by becody.com on 5/6/18.
 */

public class NetworkLoader {
    private CallbackResponse callbackResponse;

    public NetworkLoader(CallbackResponse callbackResponse) {
        this.callbackResponse = callbackResponse;

    }

    public void loadJsonRequest(String api, final int type) {
        String url = AppConstants.BASE_URL + "" + api;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        callbackResponse.onResponse(response, type);
                    } else {
                        callbackResponse.onAppError(response.getString("message"), type);
                    }
                } catch (Exception e) {
                    callbackResponse.onAppError("Server Error", type);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof ServerError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_server), type);
                } else if (error instanceof AuthFailureError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_server), type);
                } else if (error instanceof ParseError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof NoConnectionError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_timeout), type);
                } else if (error instanceof TimeoutError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_timeout), type);
                }

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void loadJsonRequestWithPost(String api, final int type) {
        String url = AppConstants.BASE_URL + "" + api;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        callbackResponse.onResponse(response, type);
                    } else {
                        callbackResponse.onAppError(response.getString("message"), type);
                    }
                } catch (Exception e) {
                    callbackResponse.onAppError("Server Error", type);
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof ServerError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof AuthFailureError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof ParseError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_server), type);
                } else if (error instanceof NoConnectionError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof TimeoutError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_timeout), type);
                }

            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void sendRequest(int method, final String api, final int type, JSONObject jsonObject) {
        String url = AppConstants.BASE_URL + "" + api;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getInt("status") == 200) {
                        callbackResponse.onResponse(response, type);
                    } else {
                        callbackResponse.onAppError(response.getString("message"), type);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                if (error instanceof NetworkError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), 100);
                } else if (error instanceof ServerError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), 100);
                } else if (error instanceof AuthFailureError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), 100);
                } else if (error instanceof ParseError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_server), 100);
                } else if (error instanceof NoConnectionError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), 100);
                } else if (error instanceof TimeoutError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_timeout), 100);
                }

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }

    public void sendRequest(String api, final int type, final HashMap<String, String> params) {
        String url = AppConstants.BASE_URL + "" + api;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    callbackResponse.onResponse(response, type);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error instanceof NetworkError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof ServerError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof AuthFailureError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof ParseError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_server), type);
                } else if (error instanceof NoConnectionError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_network), type);
                } else if (error instanceof TimeoutError) {
                    callbackResponse.onAppError(AppController.getInstance().getBaseContext().getString(R.string.error_timeout), type);
                }

            }

        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/x-www-form-urlencoded");
                return headers;
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return params;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        AppController.getInstance().addToRequestQueue(jsonObjectRequest);
    }
    public void uploadFileToServer(String url, File file, final int type) {
        AndroidNetworking.upload(AppConstants.BASE_URL + "" + url)
                .addMultipartFile("file", file)
                .setTag("uploadTest")
                .setPriority(Priority.HIGH)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        // do anything with progress
                        Log.d("Progress", "progress");
                    }
                })
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("Data", response.toString());
                        callbackResponse.onResponse(response, type);
                    }

                    @Override
                    public void onError(ANError error) {
                        // handle error
                        Log.d("error", error.getErrorDetail());
                        callbackResponse.onAppError(error.getErrorDetail(), type);

                    }
                });
    }
}
