package com.sarokh.driver.response;

/**
 * Created by becody.com on 12,04,2020
 */
public class ResSarokhTaskConfirmation {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private boolean receiveShipments;
        private boolean giveShipments;
        private boolean payCOD;
        private boolean pendingCOD;
        private boolean reportIssues;
        private String driverId;
        private String driverName;
        private String confirmationId;

        public boolean isReceiveShipments() {
            return receiveShipments;
        }

        public void setReceiveShipments(boolean receiveShipments) {
            this.receiveShipments = receiveShipments;
        }

        public boolean isGiveShipments() {
            return giveShipments;
        }

        public void setGiveShipments(boolean giveShipments) {
            this.giveShipments = giveShipments;
        }

        public boolean isPayCOD() {
            return payCOD;
        }

        public void setPayCOD(boolean payCOD) {
            this.payCOD = payCOD;
        }

        public boolean isPendingCOD() {
            return pendingCOD;
        }

        public void setPendingCOD(boolean pendingCOD) {
            this.pendingCOD = pendingCOD;
        }

        public boolean isReportIssues() {
            return reportIssues;
        }

        public void setReportIssues(boolean reportIssues) {
            this.reportIssues = reportIssues;
        }

        public String getDriverId() {
            return driverId;
        }

        public void setDriverId(String driverId) {
            this.driverId = driverId;
        }

        public String getDriverName() {
            return driverName;
        }

        public void setDriverName(String driverName) {
            this.driverName = driverName;
        }

        public String getConfirmationId() {
            return confirmationId;
        }

        public void setConfirmationId(String confirmationId) {
            this.confirmationId = confirmationId;
        }
    }
}