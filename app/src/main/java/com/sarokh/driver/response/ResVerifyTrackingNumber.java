package com.sarokh.driver.response;

/**
 * Created by becody.com on 15,08,2020
 */
public class ResVerifyTrackingNumber {
    private int status;
    private String message;
    private Data data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private int shipperId;
        private String name;

        /////////////////////
        private String pickUpLocation;
        private String shipFromCity;
        private String dealerCity;
        private String dealerName;
        private String assignToDetails;
///////////////////////////////////////


        public String getPickUpLocation() {
            return pickUpLocation;
        }

        public void setPickUpLocation(String pickUpLocation) {
            this.pickUpLocation = pickUpLocation;
        }

        public String getShipFromCity() {
            return shipFromCity;
        }

        public void setShipFromCity(String shipFromCity) {
            this.shipFromCity = shipFromCity;
        }

        public String getDealerCity() {
            return dealerCity;
        }

        public void setDealerCity(String dealerCity) {
            this.dealerCity = dealerCity;
        }

        public String getDealerName() {
            return dealerName;
        }

        public void setDealerName(String dealerName) {
            this.dealerName = dealerName;
        }

        public String getAssignToDetails() {
            return assignToDetails;
        }

        public void setAssignToDetails(String assignToDetails) {
            this.assignToDetails = assignToDetails;
        }

        public int getShipperId() {
            return shipperId;
        }

        public void setShipperId(int shipperId) {
            this.shipperId = shipperId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
