package com.sarokh.driver.ui.deliver;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResDeliverOrderStatus;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterChildDeliver extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResDeliverOrderStatus.Data> dataList;

    public AdapterChildDeliver(List<ResDeliverOrderStatus.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtReceiverName;
        private TextView txtShipmentNumber;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtReceiverName = itemView.findViewById(R.id.txtReceiverName);
            txtShipmentNumber = itemView.findViewById(R.id.txtShipmentNumber);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_child_deliver, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResDeliverOrderStatus.Data data = dataList.get(position);
                cellViewHolder.txtReceiverName.setText(data.getReceiverName());
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
