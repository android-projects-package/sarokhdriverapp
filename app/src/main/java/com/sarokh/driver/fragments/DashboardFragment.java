package com.sarokh.driver.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.activities.EdtProfileActivity;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDashboardBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResDashboard;
import com.sarokh.driver.utils.LocalFile;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.MPPointF;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by becody.com on 16,02,2020
 */
public class DashboardFragment extends Fragment implements OnChartValueSelectedListener, CallbackResponse {
    private FragmentDashboardBinding dashboardBinding;
    protected final String[] parties = new String[]{
            "Order Delivered", "Order Pending", "Order Unreached", "Order Return"};

    private NetworkLoader networkLoader;
    private LocalFile localFile;
    private ResDashboard resDashboard;
    private Context _context;
    private ArrayList<PieEntry> entries;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dashboardBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        _context = getContext();
        return dashboardBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        dashboardBinding.chartPie.setUsePercentValues(true);
        dashboardBinding.chartPie.getDescription().setEnabled(false);
        dashboardBinding.chartPie.setExtraOffsets(5, 10, 5, 5);

        dashboardBinding.chartPie.setDragDecelerationFrictionCoef(0.95f);

        //    dashboardBinding.chartPie.setCenterTextTypeface(tfLight);

        dashboardBinding.chartPie.setDrawHoleEnabled(true);
        dashboardBinding.chartPie.setHoleColor(Color.WHITE);

        dashboardBinding.chartPie.setTransparentCircleColor(Color.WHITE);
        dashboardBinding.chartPie.setTransparentCircleAlpha(110);

        dashboardBinding.chartPie.setHoleRadius(58f);
        dashboardBinding.chartPie.setTransparentCircleRadius(61f);

        dashboardBinding.chartPie.setDrawCenterText(true);

        dashboardBinding.chartPie.setRotationAngle(0);
        // enable rotation of the chart by touch
        dashboardBinding.chartPie.setRotationEnabled(true);
        dashboardBinding.chartPie.setHighlightPerTapEnabled(true);

        // chart.setUnit(" €");
        // chart.setDrawUnitsInChart(true);

        // add a selection listener
        dashboardBinding.chartPie.setOnChartValueSelectedListener(this);


        dashboardBinding.chartPie.animateY(1400, Easing.EaseInOutQuad);
        // chart.spin(2000, 0, 360);

       /* Legend l = dashboardBinding.chartPie.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.CENTER);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(false);
        l.setXEntrySpace(7f);
        l.setYEntrySpace(0f);
        l.setYOffset(0f);
*/
        // entry label styling
        dashboardBinding.chartPie.getLegend().setEnabled(false);
        dashboardBinding.chartPie.setEntryLabelColor(Color.WHITE);
        //     dashboardBinding.chartPie.setEntryLabelTypeface(tfRegular);
        dashboardBinding.chartPie.setEntryLabelTextSize(12f);

        dashboardBinding.btnEdtProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(resDashboard==null)
                    return;
                Intent intent = new Intent(getActivity(), EdtProfileActivity.class);
                AppController.getInstance().resDashboard = resDashboard;
                startActivity(intent);
            }
        });
        networkLoader = new NetworkLoader(this);
        localFile = new LocalFile(_context);
    }

    private void setData() {
        entries = new ArrayList<>();

        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
       /* for (int i = 0; i < count; i++) {
            entries.add(new PieEntry((float) ((Math.random() * range) + range / 5),
                    parties[i % parties.length],
                    getResources().getDrawable(R.drawable.star)));
        }*/
    //    entries.add(new PieEntry(resDashboard.getData().getTotalOrders()));

        PieDataSet dataSet = new PieDataSet(entries, "Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 40));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.VORDIPLOM_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.JOYFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.COLORFUL_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.LIBERTY_COLORS)
            colors.add(c);

        for (int c : ColorTemplate.PASTEL_COLORS)
            colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter(dashboardBinding.chartPie));
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        //   data.setValueTypeface(tfLight);
        dashboardBinding.chartPie.setData(data);

        // undo all highlights
        dashboardBinding.chartPie.highlightValues(null);

        dashboardBinding.chartPie.invalidate();
    }

    private SpannableString generateCenterSpannableText(String txt) {

        SpannableString s = new SpannableString(txt);
       /* s.setSpan(new RelativeSizeSpan(1.7f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.8f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);*/
        return s;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        dashboardBinding.chartPie.setCenterText(generateCenterSpannableText(getIndex((int)h.getX())));
        Log.i("VAL SELECTED",
                "Value: " + e.getY() + ", index: " + h.getX()
                        + ", DataSet index: " + h.getDataSetIndex());
    }

    private String getIndex(int index) {
        switch (index) {
            case 0:
                return "Delivered Order";
            case 1:
                return "Pending Order";
            case 2:
                return "UnReceived Orders";
            case 3:
                return "Returned Orders";
            default:
                return "Total Orders";

        }
    }

    @Override
    public void onNothingSelected() {
        dashboardBinding.chartPie.setCenterText(generateCenterSpannableText(""));
    }

    @Override
    public void onResume() {
        super.onResume();
        loadData();
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        dashboardBinding.progressBar.progressBar.setVisibility(View.GONE);
        Gson gson = new Gson();
        resDashboard = gson.fromJson(jsonObject.toString(), ResDashboard.class);
        if (resDashboard != null) {
            setUpUI();
        }

    }

    private void setUpUI() {

    }

    @Override
    public void onResponse(String response, int type) {

    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {

    }

    @Override
    public void onAppError(String msg, int type) {
        dashboardBinding.progressBar.progressBar.setVisibility(View.GONE);
    }

    private void loadData() {
        dashboardBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
    }

}
