package com.sarokh.driver.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.sarokh.driver.response.ResLogin;
import com.google.gson.Gson;

/**
 * Created by becody.com on 22,02,2020
 */
public class LocalFile {
    private SharedPreferences sharedPref;

    private final String key_user_json = "key_user_json";
    private final String key_is_logged_in = "key_is_logged_in";

    public LocalFile(Context _context) {
        sharedPref = _context.getSharedPreferences("my_sarokh_driver", Context.MODE_PRIVATE);
    }

    public void doLogin(String userJson) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key_user_json, userJson);
        editor.putBoolean(key_is_logged_in, true);
        editor.apply();
    }

    public boolean isUserLoggedIn() {
        return sharedPref.getBoolean(key_is_logged_in, false);
    }


    public ResLogin getLoginObj() {
        Gson gson = new Gson();
        String json = sharedPref.getString(key_user_json, "");
        return gson.fromJson(json, ResLogin.class);
    }

    public void setLogout() {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key_is_logged_in, false);
        editor.apply();
    }
}
