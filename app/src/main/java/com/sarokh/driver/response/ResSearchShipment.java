package com.sarokh.driver.response;

/**
 * Created by becody.com on 29,02,2020
 */
public class ResSearchShipment {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {
        private int id;
        private String trackingNo;
        private String mobile;
        private String shipmentClassification;
        private float shipmentPrice;
        private String pickupDatetime;
        private String pickupBy;
        private String driverArrivalTime;
        private String transitTime;
        private String receiverName;
        private String receiverType;
        private String status;
        private String orderType;
        private String deliveryCharges;
        private String shipmentNo;
        private String createdDatetime;
        private String updatedDatetime;
        private int shipperWarehouseId;
        private int shipperId;
        private boolean shipmentReceived = false;
        private int orderPickupTypeId;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(String trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getShipmentClassification() {
            return shipmentClassification;
        }

        public void setShipmentClassification(String shipmentClassification) {
            this.shipmentClassification = shipmentClassification;
        }

        public float getShipmentPrice() {
            return shipmentPrice;
        }

        public void setShipmentPrice(float shipmentPrice) {
            this.shipmentPrice = shipmentPrice;
        }

        public String getPickupDatetime() {
            return pickupDatetime;
        }

        public void setPickupDatetime(String pickupDatetime) {
            this.pickupDatetime = pickupDatetime;
        }

        public String getPickupBy() {
            return pickupBy;
        }

        public void setPickupBy(String pickupBy) {
            this.pickupBy = pickupBy;
        }

        public String getDriverArrivalTime() {
            return driverArrivalTime;
        }

        public void setDriverArrivalTime(String driverArrivalTime) {
            this.driverArrivalTime = driverArrivalTime;
        }

        public String getTransitTime() {
            return transitTime;
        }

        public void setTransitTime(String transitTime) {
            this.transitTime = transitTime;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getReceiverType() {
            return receiverType;
        }

        public void setReceiverType(String receiverType) {
            this.receiverType = receiverType;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public String getDeliveryCharges() {
            return deliveryCharges;
        }

        public void setDeliveryCharges(String deliveryCharges) {
            this.deliveryCharges = deliveryCharges;
        }

        public String getShipmentNo() {
            return shipmentNo;
        }

        public void setShipmentNo(String shipmentNo) {
            this.shipmentNo = shipmentNo;
        }

        public String getCreatedDatetime() {
            return createdDatetime;
        }

        public void setCreatedDatetime(String createdDatetime) {
            this.createdDatetime = createdDatetime;
        }

        public String getUpdatedDatetime() {
            return updatedDatetime;
        }

        public void setUpdatedDatetime(String updatedDatetime) {
            this.updatedDatetime = updatedDatetime;
        }

        public int getShipperWarehouseId() {
            return shipperWarehouseId;
        }

        public void setShipperWarehouseId(int shipperWarehouseId) {
            this.shipperWarehouseId = shipperWarehouseId;
        }

        public int getShipperId() {
            return shipperId;
        }

        public void setShipperId(int shipperId) {
            this.shipperId = shipperId;
        }

        public boolean getShipmentReceived() {
            return shipmentReceived;
        }

        public void setShipmentReceived(boolean shipmentReceived) {
            this.shipmentReceived = shipmentReceived;
        }

        public int getOrderPickupTypeId() {
            return orderPickupTypeId;
        }

        public void setOrderPickupTypeId(int orderPickupTypeId) {
            this.orderPickupTypeId = orderPickupTypeId;
        }
    }
}