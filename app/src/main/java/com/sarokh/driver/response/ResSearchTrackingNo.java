package com.sarokh.driver.response;

/**
 * Created by becody.com on 12,04,2020
 */
public class ResSearchTrackingNo {
    private Data data;

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        private int id;
        private String trackingNo;
        private String mobile;
        private float billedAmount;
        private String pickupDatetime;
        private String receiverName;
        private String orderType;
        private float deliveryCharges;
        private String shipmentNo;
        private int shipperId;
        private String OTP;
        private String paidAmount;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTrackingNo() {
            return trackingNo;
        }

        public void setTrackingNo(String trackingNo) {
            this.trackingNo = trackingNo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public float getBilledAmount() {
            return billedAmount;
        }

        public void setBilledAmount(float billedAmount) {
            this.billedAmount = billedAmount;
        }

        public String getPickupDatetime() {
            return pickupDatetime;
        }

        public void setPickupDatetime(String pickupDatetime) {
            this.pickupDatetime = pickupDatetime;
        }

        public String getReceiverName() {
            return receiverName;
        }

        public void setReceiverName(String receiverName) {
            this.receiverName = receiverName;
        }

        public String getOrderType() {
            return orderType;
        }

        public void setOrderType(String orderType) {
            this.orderType = orderType;
        }

        public float getDeliveryCharges() {
            return deliveryCharges;
        }

        public void setDeliveryCharges(float deliveryCharges) {
            this.deliveryCharges = deliveryCharges;
        }

        public String getShipmentNo() {
            return shipmentNo;
        }

        public void setShipmentNo(String shipmentNo) {
            this.shipmentNo = shipmentNo;
        }

        public int getShipperId() {
            return shipperId;
        }

        public void setShipperId(int shipperId) {
            this.shipperId = shipperId;
        }

        public String getOTP() {
            return OTP;
        }

        public void setOTP(String OTP) {
            this.OTP = OTP;
        }

        public String getPaidAmount() {
            return paidAmount;
        }

        public void setPaidAmount(String paidAmount) {
            this.paidAmount = paidAmount;
        }
    }
}