package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResReceiveShipmentDetail;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterReceiveShipmentDetail extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResReceiveShipmentDetail.Data> dataList;
    private Context _context;

    public AdapterReceiveShipmentDetail(List<ResReceiveShipmentDetail.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTrackingNo;
        private TextView txtReceiverName;
        private TextView txtBillAmount;
        private TextView txtReceivedStatus;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtTrackingNo = itemView.findViewById(R.id.txtTrackingNo);
            txtReceiverName = itemView.findViewById(R.id.txtReceiverName);
            txtBillAmount = itemView.findViewById(R.id.txtBillAmount);
            txtReceivedStatus = itemView.findViewById(R.id.txtReceivedStatus);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_receive_shipment_detail, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResReceiveShipmentDetail.Data data = dataList.get(position);
                cellViewHolder.txtTrackingNo.setText(data.getTrackingNo());
                cellViewHolder.txtReceiverName.setText(data.getReceiverName());
                cellViewHolder.txtBillAmount.setText(_context.getResources().getString(R.string.currency) + "" + data.getBilledAmount() + "");
                cellViewHolder.txtReceivedStatus.setText(data.getReceivedStatus());
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
