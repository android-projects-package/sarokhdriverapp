package com.sarokh.driver.fragments;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.android.volley.Request;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.DexterBuilder;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;
import com.sarokh.driver.AppController;
import com.sarokh.driver.MapsRouteActivity;
import com.sarokh.driver.R;
import com.sarokh.driver.adapters.AdapterSarokhTaskDriver;
import com.sarokh.driver.callback.CallbackRecyclerViewClick;
import com.sarokh.driver.callback.CallbackRecyclerViewClickType;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentSarokhTaskDriverBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResSarokhTaskDriver;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Created by becody.com on 14,04,2020
 */
public class FragmentSarokhTaskDriver extends Fragment implements CallbackResponse, CallbackRecyclerViewClickType<ResSarokhTaskDriver.Data> {
    private FragmentSarokhTaskDriverBinding sarokhTaskDriverBinding;
    private JSONObject routeData;
    private double lat, lng;
    private double[] totalDistanceList, sortDistance, checkIndex;
    String tripId = "0";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sarokhTaskDriverBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_sarokh_task_driver, container, false);
        return sarokhTaskDriverBinding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getLocation();
        sarokhTaskDriverBinding.swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData(true);
            }
        });
        sarokhTaskDriverBinding.masterRouteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculateDistance();
            }
        });
    }

    private void getLocation() {
        FusedLocationProviderClient fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getContext());
        if (ActivityCompat.checkSelfPermission(getContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            fusedLocationProviderClient.getLastLocation().addOnCompleteListener(new OnCompleteListener<Location>() {
                @Override
                public void onComplete(@NonNull Task<Location> task) {
                    Location location = task.getResult();
                    if (location != null) {
                        try {
                            Geocoder geocoder = new Geocoder(getContext(), Locale.getDefault());
                            List<Address> addresses = geocoder.getFromLocation(
                                    location.getLatitude(), location.getLongitude(), 1);
                            lat = addresses.get(0).getLatitude();
                            lng = addresses.get(0).getLongitude();

                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    private void calculateDistance() {

        if (AppController.getInstance().resSarokhTaskDriver == null) {
//            no data
            return;
        }
        if (AppController.getInstance().resSarokhTaskDriver.getData() == null) {
//            no data
            return;
        }

        List<ResSarokhTaskDriver.Data> rstd = AppController.getInstance().resSarokhTaskDriver.getData();

        if (rstd.size() != 0) {
            totalDistanceList = new double[rstd.size()];
        }

        for (ResSarokhTaskDriver.Data ab : rstd) {
            Location startPoint = new Location("Current");
            startPoint.setLatitude(lat);
            startPoint.setLongitude(lng);

            Location endPoint = new Location("Nearest");
            endPoint.setLatitude(Double.parseDouble(ab.getLatitude()));
            endPoint.setLongitude(Double.parseDouble(ab.getLongitude()));
            totalDistanceList[rstd.indexOf(ab)] = startPoint.distanceTo(endPoint);  //meter
        }

        double min = totalDistanceList[0];
        int position = 0;
        for (int j = 0; j < totalDistanceList.length; j++) {
            if (min > totalDistanceList[j]) {
                min = totalDistanceList[j];
                position = j;
            }
        }

        ResSarokhTaskDriver.Data demo = null;
        AppController.totalDistance = min;
        int k = 0;
        for (ResSarokhTaskDriver.Data ab : rstd) {
            if (k == position) {
                AppController.lat = Double.parseDouble(ab.getLatitude());
                AppController.lng = Double.parseDouble(ab.getLongitude());
                demo = ab;
            }
            k++;
        }

        sortList();
        final ResSarokhTaskDriver.Data finalDemo = demo;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Nearest Point: " + finalDemo.getLocationName());
        builder.setMessage("Do you want to Navigate?");

        builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", Float.parseFloat(finalDemo.getLatitude()), Float.parseFloat(finalDemo.getLongitude()));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        });
        builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //do nothing
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

//        startActivity(new Intent(getContext(), MapsRouteActivity.class));
    }

    private void loadData(final boolean refresh) {

        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle("Alert");
        alert.setIcon(R.drawable.alert);
        alert.setMessage("Please Enter Trip ID ");
        final EditText input = new EditText(getActivity());
        input.setInputType(InputType.TYPE_CLASS_NUMBER);
        alert.setView(input);
        alert.setCancelable(false);
        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();
                tripId = value;
                if(! value.equals("")){
                   // AppController.getInstance().resSarokhTask.getData().setTripId(value.toString());

                    if (refresh) {
                        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.GONE);
                        sarokhTaskDriverBinding.swipeContainer.setRefreshing(true);
                    } else {
                        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.VISIBLE);
                        sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
                    }

                    NetworkLoader loader = new NetworkLoader(FragmentSarokhTaskDriver.this);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("driverId",AppController.getInstance().resLoginObj.getData().getId());
                        jsonObject.put("tripId",value);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    loader.sendRequest(Request.Method.POST, AppConstants.END_GET_SAROKH_TASKV2, 1, jsonObject);

                    //AppController.getInstance().resSarokhTask.getData().setTripId(value);
                    //loader.loadJsonRequest(AppConstants.END_GET_SAROKH_TASK + AppController.getInstance().resLoginObj.getData().getId(), 1);

                }
                else{
                    sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.INVISIBLE);
                    sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
                    Toast.makeText(getContext(), "Trip ID not provided ", Toast.LENGTH_SHORT).show();
                }
            }
        });
        /*alert.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub
                        loadData(true);
                    }
                });*/
        alert.show();


    }

    @Override
    public void onResume() {
        super.onResume();
        if (AppController.getInstance().resSarokhTaskDriver == null) {
            loadData(false);
        } else if (AppController.getInstance().resSarokhTaskDriver.getData() == null) {
            loadData(false);
        } else if (AppController.getInstance().resSarokhTaskDriver.getData().size() == 0) {
            loadData(false);
        } else {
            setUpAdapter();
        }
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        this.routeData = jsonObject;
        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.GONE);
        sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
        AppController.getInstance().resSarokhTaskDriver = new Gson().fromJson(jsonObject.toString(), ResSarokhTaskDriver.class);
        setUpAdapter();
    }

    private void sortList() {
        List<ResSarokhTaskDriver.Data> rstd = AppController.getInstance().resSarokhTaskDriver.getData();

        Location startPoint = new Location("Current");
        startPoint.setLatitude(lat);
        startPoint.setLongitude(lng);

        for(ResSarokhTaskDriver.Data item : rstd){

            Location endPoint = new Location("Nearest");
            endPoint.setLatitude(Double.parseDouble(item.getLatitude()));
            endPoint.setLongitude(Double.parseDouble(item.getLongitude()));
            item.distance = startPoint.distanceTo(endPoint);
        }

        if (rstd.size() != 0) {
            sortDistance = new double[rstd.size()];
        }

        Collections.sort(rstd, new Comparator<ResSarokhTaskDriver.Data>() {
            @Override
            public int compare(ResSarokhTaskDriver.Data o1, ResSarokhTaskDriver.Data o2) {
                if (o1.distance < o2.distance) return -1;
                else if (o1.distance > o2.distance) return 1;
                else return 0;
            }

        });
        setUpAdapter(rstd);
    }

    private void setUpAdapter(List<ResSarokhTaskDriver.Data> datalist){

        AdapterSarokhTaskDriver sarokhTaskDriver = new AdapterSarokhTaskDriver(datalist, this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        sarokhTaskDriverBinding.listing.setLayoutManager(linearLayoutManager);
        sarokhTaskDriverBinding.listing.setAdapter(sarokhTaskDriver);       // set null if problem exist
    }

    private void setUpAdapter() {

        AdapterSarokhTaskDriver sarokhTaskDriver = new AdapterSarokhTaskDriver(AppController.getInstance().resSarokhTaskDriver.getData(), this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setOrientation(RecyclerView.VERTICAL);
        sarokhTaskDriverBinding.listing.setLayoutManager(linearLayoutManager);
        sarokhTaskDriverBinding.listing.setAdapter(sarokhTaskDriver);
    }

    @Override
    public void onResponse(String response, int type) {
        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.GONE);
        sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.GONE);
        sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
    }

    @Override
    public void onAppError(String msg, int type) {
        sarokhTaskDriverBinding.progressBar.progressBar.setVisibility(View.GONE);
        sarokhTaskDriverBinding.swipeContainer.setRefreshing(false);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }

    @Override
    public void onClickRow(ResSarokhTaskDriver.Data modal, int type) {

        if (type == 2) {
            /*String uri = String.format(Locale.ENGLISH, "geo:%f,%f", Float.parseFloat(modal.getLatitude()), Float.parseFloat(modal.getLongitude()));
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
            getActivity().startActivity(intent);*/
           /* Uri gmmIntentUri = Uri.parse("geo:" + modal.getLatitude() + "," + modal.getLongitude());
            Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            mapIntent.setPackage("com.google.android.apps.maps");
            if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                startActivity(mapIntent);
            }*/
            if (modal.getLatitude() == null || modal.getLongitude() == null) {
                AppConstants.showErrorDialog(getActivity(), "No latitude or longitude available");
            } else {
                String uri = String.format(Locale.ENGLISH, "http://maps.google.com/maps?q=loc:%f,%f", Float.parseFloat(modal.getLatitude()), Float.parseFloat(modal.getLongitude()));
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        } else if (type == 0) {

            if (modal.getStopType().equals("Dealer Point") || modal.getStopType().equals("Sarokh Point")) {
//                Toast.makeText(getActivity(), "Dealer Point Scenario", Toast.LENGTH_SHORT).show();
                AppController.getInstance().resSarokhTaskDriverData = modal;
                AppController.getInstance().resSarokhTask = null;
                AppController.getInstance().receiveShipmentDetail = null;

                // TODO NULL CHECK
                Navigation.findNavController(sarokhTaskDriverBinding.getRoot()).navigate(R.id.actionMySarokh);

            } else if (modal.getStopType().equals("Shipper Warehouse")) {

//              Toast.makeText(getActivity(), "Shipper Warehouse Scenario", Toast.LENGTH_SHORT).show();

                Log.d("Modal",modal.toString());

                AppController.getInstance().resSarokhTaskDriverData = modal;

                //Navigation.findNavController(sarokhTaskDriverBinding.getRoot()).navigate(R.id.actionWareHouse);

                Navigation.findNavController(sarokhTaskDriverBinding.getRoot()).navigate(R.id.fragmentReceiveShipmentFromShipper);

            }

            else if (modal.getStopType().equals("Last Mile")) {
//                Toast.makeText(getActivity(), "Last Mile Scenario", Toast.LENGTH_SHORT).show();


                Bundle bundle = new Bundle();
                bundle.putInt("taskId", modal.getId());
                bundle.putString("tripId",tripId);
                Navigation.findNavController(sarokhTaskDriverBinding.getRoot()).navigate(R.id.actionDeliverShipment,bundle);

            }

        }

        else if (type == 1) {

            Navigation.findNavController(sarokhTaskDriverBinding.getRoot()).navigate(R.id.actionDeliverShipment);
        }

    }
}
