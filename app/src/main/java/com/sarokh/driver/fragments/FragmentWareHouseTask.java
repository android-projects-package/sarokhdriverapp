package com.sarokh.driver.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentWareHouseTaskBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.response.ResWareHouse;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by becody.com on 15,06,2020
 */
public class FragmentWareHouseTask extends Fragment implements CallbackResponse {
    private FragmentWareHouseTaskBinding binding;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_ware_house_task, container, false);
        return binding.getRoot();
    }

    private void load() {
        binding.progressBar.progressBar.setVisibility(View.VISIBLE);
        NetworkLoader networkLoader = new NetworkLoader(this);
        networkLoader.loadJsonRequest(AppConstants.END_END_WARE_HOUSE + AppController.getInstance().resSarokhTaskDriverData.getId(), 1);
    }

    @Override
    public void onResume() {
        super.onResume();
        load();
    }

    private void init() {
        binding.txtDriverName.setText(AppController.getInstance().resWareHouse.getData().getDriverName());
        binding.txtShipperId.setText(AppController.getInstance().resWareHouse.getData().getDriverId() + "");
        binding.txtReceivedShipment.setText(AppController.getInstance().resWareHouse.getData().getReceiveShipments() + "");
        binding.txtReturnShipment.setText(AppController.getInstance().resWareHouse.getData().getReturnShipments() + "");
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnReceived.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (AppController.getInstance().resWareHouse == null) {
                    return;
                }
                if (AppController.getInstance().resWareHouse.getData() == null)
                    return;

                Bundle bundle = new Bundle();
                String[] shipmentList = AppController.getInstance().resWareHouse.getData().getReceiveShipmentList();
                bundle.putStringArray("receiveShipmentList",shipmentList);
                Navigation.findNavController(binding.getRoot()).navigate(R.id.actionShipperReceiving,bundle);
            }
        });
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppController.getInstance().resWareHouse = new Gson().fromJson(jsonObject.toString(), ResWareHouse.class);




        if (AppController.getInstance().resWareHouse.getData() == null) {
            AppConstants.showDialogWithoutTitle(getActivity(), AppController.getInstance().resWareHouse.getMessage());
            binding.btnReceived.setEnabled(false);
        } else {
            binding.btnReceived.setEnabled(true);
            init();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);

    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        AppConstants.showDialogWithoutTitle(getActivity(), msg);
    }
}
