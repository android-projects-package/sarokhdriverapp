package com.sarokh.driver.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.sarokh.driver.R;
import com.sarokh.driver.response.ResCodLedger;

import java.util.List;

/**
 * Created by becody.com on 24,02,2020
 */
public class AdapterCodLedger extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ResCodLedger.Data> dataList;
    private Context _context;

    public AdapterCodLedger(List<ResCodLedger.Data> dataList) {
        this.dataList = dataList;
    }

    private class CellViewHolder extends RecyclerView.ViewHolder {
        private TextView txtLedgerId;
        private TextView txtDeliverDate;
        private TextView txtBillAmount;
        private TextView txtPendingAmount;
        private TextView txtDriverId;
        private TextView txtDriverName;

        public CellViewHolder(View itemView) {
            super(itemView);
            txtLedgerId = itemView.findViewById(R.id.txtLedgerId);
            txtDeliverDate = itemView.findViewById(R.id.txtDeliverDate);
            txtBillAmount = itemView.findViewById(R.id.txtBillAmount);
            txtPendingAmount = itemView.findViewById(R.id.txtPendingAmount);
            txtDriverId = itemView.findViewById(R.id.txtDriverId);
            txtDriverName = itemView.findViewById(R.id.txtDriverName);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        _context = parent.getContext();
        switch (viewType) {
            default: {
                View v1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_cod_ledger, parent, false);
                return new CellViewHolder(v1);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            default: {
                CellViewHolder cellViewHolder = (CellViewHolder) holder;
                ResCodLedger.Data data = dataList.get(position);
                cellViewHolder.txtLedgerId.setText(data.getLedgerId() + "");
                cellViewHolder.txtDriverId.setText(data.getDriverId() + "");
                cellViewHolder.txtDeliverDate.setText(data.getDeliveryDate());
                cellViewHolder.txtDriverName.setText(data.getDriverName());
                cellViewHolder.txtBillAmount.setText(_context.getResources().getString(R.string.currency) + "" + data.getAmountPaid() + "");
                cellViewHolder.txtPendingAmount.setText(_context.getResources().getString(R.string.currency) + "" + data.getPendingAmount() + "");
                break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }
}
