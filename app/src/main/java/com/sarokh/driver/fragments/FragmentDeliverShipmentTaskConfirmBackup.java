package com.sarokh.driver.fragments;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import com.android.volley.Request;
import com.github.gcacace.signaturepad.views.SignaturePad;
import com.sarokh.driver.AppController;
import com.sarokh.driver.R;
import com.sarokh.driver.callback.CallbackResponse;
import com.sarokh.driver.databinding.FragmentDeliverShipmentTaskConfirmBinding;
import com.sarokh.driver.network.NetworkLoader;
import com.sarokh.driver.utils.AppConstants;
import com.sarokh.driver.utils.CustomToast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

/**
 * Created by becody.com on 12,04,2020
 */
public class FragmentDeliverShipmentTaskConfirmBackup extends Fragment implements SignaturePad.OnSignedListener, CallbackResponse {
    private FragmentDeliverShipmentTaskConfirmBinding binding;
    private Bitmap bitmapSignature;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_deliver_shipment_task_confirm, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.signaturePad.setOnSignedListener(this);
        binding.txtTrackingNo.setText(AppController.getInstance().resSearchTrackingNo.getData().getTrackingNo());
        binding.txtReceiverName.setText(AppController.getInstance().resSearchTrackingNo.getData().getReceiverName());
        binding.txtBilledAmount.setText(getString(R.string.currency) + "" + AppController.getInstance().resSearchTrackingNo.getData().getBilledAmount());
        binding.txtPaidAmount.setText(getString(R.string.currency) + "" + AppController.getInstance().resSearchTrackingNo.getData().getPaidAmount());
        //binding.txtOtp.setText(AppController.getInstance().resSearchTrackingNo.getData().getOTP().replace());
        binding.txtOtp.setText("****");
        binding.btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                binding.signaturePad.clear();
            }
        });
        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmapSignature == null) {
                    CustomToast.showToast(getActivity(), "Enter Signature");
                    return;
                }
                submitDeliverShipment();
            }
        });
    }

    private void submitDeliverShipment() {
        try {
            binding.progressBar.progressBar.setVisibility(View.VISIBLE);
            NetworkLoader networkLoader = new NetworkLoader(this);
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("codcharges", AppController.getInstance().resSearchTrackingNo.getData().getDeliveryCharges());
            jsonObject.put("delivererName", AppController.getInstance().resLoginObj.getData().getUser().getFullName());
            jsonObject.put("receivedDateTime", AppConstants.getCurDateAndTime());
            jsonObject.put("receiverName", AppController.getInstance().resSearchTrackingNo.getData().getReceiverName());
            jsonObject.put("shipmentNo", AppController.getInstance().resSearchTrackingNo.getData().getShipmentNo());
            jsonObject.put("signature", toBase64(bitmapSignature));
            networkLoader.sendRequest(Request.Method.POST, AppConstants.END_HAND_OVER_RECEIVED_SHIPMENT, 1, jsonObject);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String toBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public void onResponse(JSONObject jsonObject, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        try {
            CustomToast.showToast(getActivity(), jsonObject.getString("message"));
            Navigation.findNavController(binding.getRoot()).popBackStack();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResponse(String response, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onResponse(JSONArray jsonArray, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onAppError(String msg, int type) {
        binding.progressBar.progressBar.setVisibility(View.GONE);
        CustomToast.showToast(getActivity(), msg);
    }

    @Override
    public void onStartSigning() {

    }

    @Override
    public void onSigned() {
        bitmapSignature = binding.signaturePad.getSignatureBitmap();
    }

    @Override
    public void onClear() {
        bitmapSignature = null;
    }
}
